# SM64 Advanced Text Egnine

Advanced Text Engine for displaying dialog in cool  ways for sm64 romhacks.
To download the python tool, download the folder called "dist"

**TEXT FEATURES**
Displays dialog on the screen in unique ways. See video for a small showcase
https://www.youtube.com/watch?v=A-YfaQpm5-A

If you want to use a cmd manually, use this format:

[comment: CMD - parameters]
you can use this format to encode any raw hex data you want. You can also use it to store
any comments you want inside the string.


See video for installation:
https://www.youtube.com/watch?v=c15UlUdh5pE

**DECOMP INSTALL**
Install via patch for refresh 11. If the patch does not work, I have included every single file that I have changed alongside the patch file.

To install this in decomp manually, you have to add the folder called "TextEngine" inside the decomp folder to /src/ in your build directory.
From there you need to define a new segment and add it to the make file.
For now, you need to put the segment at 0x80040000.
Then you add the data to the linker in the same order I have put in in my linker.
From there you need to use the extra function to dma the segment in memory.c and then you add the call to run the text engine from rendering graph node.c and load the segment from main.c
After all of that, setup an interaction type so that setup text engine is run at the appropriate time.