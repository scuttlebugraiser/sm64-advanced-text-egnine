import TextDef
import re
from numpy import genfromtxt
Ascii=TextDef.Ascii #string encoding
Cmds=TextDef.Cmds #special cmds
ent=TextDef.ent#functions to parse entries
revent=TextDef.revent#how to parse stuff when editing cmds
LabelNotes = TextDef.notes#notes on cmds
cdef = TextDef.colors #colors for each cmd
from tkinter import *
from tkinter.filedialog import *
from tkinter.messagebox import *
from tkinter.font import Font
from tkinter.scrolledtext import *
from tkinter.ttk import *
from tkinter.colorchooser import askcolor
import file_menu
import edit_menu
import format_menu
import help_menu
import functools
import binascii

root = Tk()
root.geometry("1280x760")
root.minsize(width=1280, height=760)

Cbase = """#define """

#Make output
def COutput():
	hLen = Output()
	f = open(name.filename + '.c','w')
	f.write(Cbase)
	Cname = name.filename.split("/")
	Cname = Cname[-1].split(".")
	Cname = Cname[0].replace(" ","_")
	f.write(Cname+" ")
	for i, b in enumerate(range(0,len(hLen),2)):
		if i + 1 == len(hLen):
			comma = ""
		else:
			comma = ","
		f.write('0x' + hLen[b:b+2] + comma)

def HexOutput():
	hLen = Output()
	f = open(name.filename + '.bin','wb')
	f.write(binascii.unhexlify(hLen))

def Output():
	hext.delete(1.0,END)
	tinp = text.get(1.0,"end-1c")
	i=0
	while i>=0:
		if i==len(tinp):
			break
		try:
			chr = str(Ascii[tinp[i:i+1]])
			chr=chr.replace("0x","").upper()
			while len(chr)<2:
				chr="0"+chr
			chr=chr+" "
			hext.insert(END,chr)
			i+=1
		except:
			i+=1
			chr = tinp[i:i+1]
			tmp = ""
			flag=0
			while 1:
				if chr is not ']':
					if flag==1 and chr == '(':
						[tinp,tmp,i]=MapToAddr(tinp,tmp,i)
					elif flag==1 and chr is not "-":
						tmp = tmp + chr
					if chr == ":":
						flag=1
					#add everything after colon to string, don't add dash
					i+=1
					chr = tinp[i:i+1]
				else:
					i+=1
					tmp = tmp.replace(" ","") + " "
					hext.insert(END,tmp)
					q = hext.index("end - %d chars" % (len(tmp)+1))
					hext.tag_add("test", q, "end-2c")
					hext.tag_config("test", foreground="white", background="grey")
					break
	#now I will give the length of the string into the length label
	hLen=hext.get(1.0,"end-1c")
	hLen=hLen.replace(" ","")
	lenh.config(text="Length= %s" %hex(int(len(hLen)/2)))
	return hLen

def MapToAddr(text,out,iter):
	if hasattr(map,'test'):
		Pval=text[iter:].split(')')
		Pval=Pval[0][1:]
		ind = name.mapf.find(Pval)
		if ind is -1:
			raise Exception('symbol not found in map')
		else:
			addr = name.mapf[ind-54:ind].split('\n')[-1] #there are 54 characters before identifier
			addr = addr.split('0x')
			addr = addr[1][8:]
			out = out+addr
			iter = iter + len(Pval)+1
			return [text,out,iter]
	else:
		raise Exception('You need to load a map')

#Make Entry Fields for selected cmd
colp = ent[hex(0x42)][0]
def entries():
	global currententry
	global currentkey
	global currententries
	k = slcm.get()
	#change notes inner text
	try:
		notes.config(text=LabelNotes[k])
	except:
		pass
	try:
		if currententry:
			currententry.destroy()
	except:
		pass
	tempframe=Frame(entryframe)
	lb = {}
	nb = {}
	#slcm.get() returns the dict key of our cmd
	for i in enumerate(Cmds[k][1]):
		o = i[0]
		ff = int(o/2)
		if i[0]%2==1:
			lb[o] = Label(tempframe,text=i[1],font=('Times', '12'))
			lb[o].grid(row=o,column=0)
		else:
			nb[o]=Entry(tempframe)
			nb[o].grid(row=o+1,column=1)
			try:
				if(ent[k][ff]==colp):
					nb[o].bind("<Button-1>",functools.partial(getcolor,nb[o]))
			except:
				pass
	tempframe.pack(side=RIGHT,expand=0)
	currententry = tempframe
	currentkey = k
	currententries=nb

cmdmarks=[]
def getLineLen(event):
	Kern = TextDef.Kernings
	line = int(text.index(INSERT).split(".")[0])
	L = text.get("%d.0"%line,"%d.end"%line)
	len = 0
	skip = False
	Check = False
	for i,c in enumerate(L):
		if skip:
			if c == "]":
				skip=False
				continue
			else:
				if c == ":":
					Check=True
					continue
				elif Check == True:
					cmd = L[i:].lstrip()
					cmd = cmd[:2]
					len += Kern[int("0x%s"%cmd, 16)]
					Check = False
					continue
				continue
		if c == "[":
			skip = True
			continue
		H = Ascii.get(c)
		if H:
			len+=Kern[int(H,16)]
	lenl.config(text="Line Len = %s"%len)

def getcolor(field,event):
	color = askcolor()
	field.delete(0,END)
	field.insert(0,color[1])

def setcmd(wid,kOG,nbT):
	k=kOG[0:4]
	try:
		params = Cmds[k][1]
		p=""
		for i in nbT:
			L = int(params[i])*2
			#nbT is each entry field
			if nbT[i].get():
				if (k=='0xa0' or k=='0xa1') and i==6:
					if nbT[4].get():
						if int(nbT[4].get())>=0:
							break
					else:
						break
				try:
					qqq = ent[k]
					f=qqq[int(i/2)]
					num=f(nbT[i].get(),L,map)
				except:
					num=TextDef.numput(nbT[i].get(),L,map)
					f=0
				if (f==getattr(TextDef,'textput')) and hasattr(map,'test'):
					pass #num= "("+num+")"
				else:
					num=str(num)
					num = num.replace("0x","")
					num = num.replace("-","")
					while len(num) < L:
						num = "0" + num
				p=p+num
			else:
				if (k=='0xa0' or k=='0xa1') and i==6:
					if nbT[4].get():
						if int(nbT[4].get())>=0:
							break
					else:
						break
				num="00"
				while len(num) < L:
					num = "0" + num
				p=p+num
		p = "- " + p + "]"
		R = text.tag_ranges(kOG)
		fullcmd = text.get(R[0],R[1])
		dash = fullcmd.rindex("-")
		sttt = (str(R[0])+"+ %d char" %dash)
		text.delete(sttt,R[1])
		text.insert(sttt,p)
		text.tag_delete(kOG)
		text.tag_add(kOG,R[0],R[1])
		cod = cdef[k]
		fc = cod[1]
		bc = cod[0]
		text.tag_config(kOG, foreground=fc, background=bc)
		tinpp = text.get(R[0],R[1])
		text.tag_bind(kOG, '<Button-1>', functools.partial(editcmd, kOG, text=tinpp))
	except:
		pass
	wid.destroy()


def delcmd(wid,k):
	R = text.tag_ranges(k)
	text.delete(R[0],R[1])
	text.tag_delete(k)
	wid.destroy()
	pass

def editcmd(qq,event,text):
	k=qq[0:4]#key of cmd
	if Cmds[k][1] == ():
		return
	editTL = Toplevel()
	lbT = {}
	nbT = {}
	ps = text.find('-')
	text = text[ps+1:len(text)-1]
	text=text.strip()
	#text is now just the arg bytes
	B1 = Button(editTL,text="Edit CMD",command=functools.partial(setcmd,editTL,qq,nbT))
	B2 = Button(editTL,text="Delete CMD",command=functools.partial(delcmd,editTL,qq))
	for i in enumerate(Cmds[k][1]):
		o = i[0] #iterator number
		ff = int(o / 2) #col number
		symb=0#additional characters added because of symbol
		if i[0] % 2 == 1:
			lbT[o] = Label(editTL, text=i[1])
			lbT[o].grid(row=o, column=0)
		else:
			nbT[o] = Entry(editTL)
			nbT[o].grid(row=o + 1, column=1)

			try:
				f = revent[k]
				f = f[int(i[0]/2)]
				if (f == getattr(TextDef, 'textout')):
					rp = text.find(')')
					if rp==-1:
						value = text[0:i[1] * 2]
					else:
						value=text[0:rp+1]
						symb=symb+rp+1-(i[1] * 2)
				else:
					value = text[0:i[1] * 2 + symb]
				try:
					text = text[symb+i[1]*2:len(text)]
					symb=0
				except:
					if k=='0xa0' or k=='0xa1':
						break
				value = f(value,i[1]*8,map)
			except:
				value = text[0:i[1] * 2]
				text = text[i[1]*2:len(text)]
				value = TextDef.numout(value,i[1],map)
			nbT[o].insert(0,value)
			try:
				if (ent[k][ff] == colp):
					nbT[o].bind("<Button-1>", functools.partial(getcolor, nbT[o]))
			except:
				pass
	B1.grid(row=8,column=0)
	B2.grid(row=8,column=1)


def addcmd():
	try:
		params = Cmds[currentkey][1]
		p=""
		for i in currententries:
			#current entries is a dictionary of every entry widget enumerated.
			#current key is the cmd being added
			L = int(params[i])*2
			if currententries[i].get():
				#use function dictionary to convert entries input to proper entry output
				#function will take L as the number of bytes of the input
				#currentetnries[i].get() as the actual entry
				#i/2 as the param number, if the cmd has 4 args, i will be 0,1,2,3
				#key in ent dictionary will be same key, values will be list of functions
				if (currentkey=='0xa0' or currentkey=='0xa1') and i==6:
					if currententries[4].get():
						if int(currententries[4].get())>=0:
							break
					else:
						break
				try:
					k = ent[currentkey]
					f=k[int(i/2)]
					num=f(currententries[i].get(),L,map)
					num=str(num)
				except:
					num=TextDef.numput(currententries[i].get(),L,map)
					f=0
				if (f==getattr(TextDef,'textput')) and hasattr(map,'test'):
					if num.find('(') ==-1:
						while len(num) < L:
							num = "0" + num
						print(num)
				else:
					num = num.replace("0x","")
					num = num.replace("-","")
					while len(num) < L:
						num = "0" + num
				p=p+num
			else:
				if (currentkey=='0xa0' or currentkey=='0xa1') and i==6:
					if currententries[4].get():
						if int(currententries[4].get())>=0:
							break
					else:
						break
				num="00"
				while len(num) < L:
					num = "0" + num
				p=p+num

		cmd = currentkey.replace("0x","").upper()
		cmd = "[" + Cmds[currentkey][0] + ": " + cmd +" - "+ p + "]"
		text.insert(INSERT,cmd)
		q = text.index("insert - %d chars" % len(cmd))
		cod = cdef[currentkey]
		fc = cod[1]
		bc = cod[0]
		text.tag_add(currentkey+q,q,INSERT)
		text.tag_config(currentkey+q,foreground=fc,background=bc)
		tinpp=text.get(q,INSERT)
		text.tag_bind(currentkey+q,'<Button-1>',functools.partial(editcmd,currentkey+q,text=tinpp))
	except:
		pass

def addgeneric(self):
	text.insert(INSERT,self['text'])

def addhotbar(string):
	text.insert(INSERT,string)

def synctags():
	for x in text.tag_names():
		text.tag_delete(x)
	tinp = text.get(1.0,'end-1c')#entire input as a string
	i = 0
	while i >= 0:
		if i >= len(tinp):
			break
		try:
			chr = str(Ascii[tinp[i:i + 1]])
			#if its in the dict then it needs no tag
			i += 1
		except:
			i += 1
			chr = tinp[i:i + 1]
			flag = 0
			st = i
			cmd=""
			while 1:
				if chr is not ']':
					if chr is "-":
						flag=0
					if flag == 1:
						cmd = cmd + chr
					if chr is ":":
						flag=1
					i += 1
					chr = tinp[i:i + 1]
				else:
					i += 1
					q = text.index("1.0 + %d chars" % int(st-1))
					qe = text.index("1.0 + %d chars" % i)
					cmd="0x" + cmd.strip()
					cmd=cmd.lower()
					if cmd in cdef:
						cod = cdef[cmd]
						fc = cod[1]
						bc = cod[0]
						text.tag_add(cmd + q, q, qe)
						text.tag_config(cmd + q, foreground=fc, background=bc)
						text.tag_bind(cmd + q, '<Button-1>', functools.partial(editcmd, cmd + q,text=text.get(q,qe)))
					break

cmdsf=Frame(root)
paramframe = Frame(cmdsf)
paramframe.pack(side=RIGHT,expand=0)

#special characters buttons
end = Button(paramframe,text="[end:FF]")
end.config(command=functools.partial(addgeneric,end))
end.grid(row=3,column=0)
star = Button(paramframe,text="[Solid Star:FA]")
star.config(command=functools.partial(addgeneric,star))
star.grid(row=3,column=1)
star1 = Button(paramframe,text="[Hollow Star:FD]")
star1.config(command=functools.partial(addgeneric,star1))
star1.grid(row=4,column=0)
quoR = Button(paramframe,text="[Right Quote:F5]")
quoR.config(command=functools.partial(addgeneric,quoR))
quoR.grid(row=4,column=1)
quoL = Button(paramframe,text="[Left Quote:F6]")
quoL.config(command=functools.partial(addgeneric,quoL))
quoL.grid(row=5,column=0)
you = Button(paramframe,text="[you:D2]")
you.config(command=functools.partial(addgeneric,you))
you.grid(row=5,column=1)
the = Button(paramframe,text="[the:D1]")
the.config(command=functools.partial(addgeneric,the))
the.grid(row=6,column=0)
UP = Button(paramframe,text="[UP:50]")
UP.config(command=functools.partial(addgeneric,UP))
UP.grid(row=6,column=1)
DOWN = Button(paramframe,text="[DOWN:51]")
DOWN.config(command=functools.partial(addgeneric,DOWN))
DOWN.grid(row=7,column=0)
LL = Button(paramframe,text="[LEFT:52]")
LL.config(command=functools.partial(addgeneric,LL))
LL.grid(row=7,column=1)
RR = Button(paramframe,text="[RIGHT:53]")
RR.config(command=functools.partial(addgeneric,RR))
RR.grid(row=8,column=0)
Abut = Button(paramframe,text="[A:54]")
Abut.config(command=functools.partial(addgeneric,Abut))
Abut.grid(row=8,column=1)
Bbut = Button(paramframe,text="[B:55]")
Bbut.config(command=functools.partial(addgeneric,Bbut))
Bbut.grid(row=9,column=0)
Cbut = Button(paramframe,text="[C:56]")
Cbut.config(command=functools.partial(addgeneric,Cbut))
Cbut.grid(row=9,column=1)
Zbut = Button(paramframe,text="[Z:57]")
Zbut.config(command=functools.partial(addgeneric,Zbut))
Zbut.grid(row=10,column=0)
Rbut = Button(paramframe,text="[R:58]")
Rbut.config(command=functools.partial(addgeneric,Rbut))
Rbut.grid(row=10,column=1)
Pad = Button(paramframe,text="[NB Pad:83]")
Pad.config(command=functools.partial(addgeneric,Pad))
Pad.grid(row=10,column=1)


entryframe = Frame(paramframe)
entryframe.grid(row=0,columnspan=2)
notes = Label(paramframe,text="extra info on cmds will be displayed here",wraplength="150",relief=SUNKEN,font=('Times', '12', 'bold'))
notes.grid(row=2,columnspan=2)
adcmd = Button(paramframe,text="Add Cmd",command=addcmd)
adcmd.grid(row=1,columnspan=2)


note2 = Label(paramframe,text="If using a map, put parenthesis to use a symbol, otherwise it will be hex. Make sure to load a map before editing or adding symbols.",wraplength="150",relief=SUNKEN,font=('Times', '12'))
note2.grid(row=11,columnspan=2)

#list of buttons
btnctn=Frame(cmdsf)
slcm = StringVar()
cbut={}
for i,x in enumerate(Cmds):
	try:
		l=(len(Cmds)+1)//2
		txt=Cmds[x][0]
		cbut[x]=Radiobutton(btnctn,text=txt,width=40,variable=slcm,value=x,command=entries)
		cbut[x].grid(row=i%l,column=i//l)
	except:
		pass
btnctn.pack(side=LEFT,expand=1,fill=X,anchor=E)

cmdsf.pack(side=RIGHT,expand=0,fill=X)


strf=Frame(root)
tpbtnframe=Frame(strf)
encodeC=Button(tpbtnframe,command=COutput,text="Export to C")
encodeC.pack(side=LEFT,padx=5,pady=5)
encode=Button(tpbtnframe,command=HexOutput,text="Export to Bin")
encode.pack(side=LEFT,padx=5,pady=5)
synctag = Button(tpbtnframe,text="Sync Cmds",command=synctags)
synctag.pack(side=RIGHT,padx=5,pady=5)

tpbtnframe.grid(row=1,column=0)
lblframe=Frame(strf)
map=Label(lblframe,text="No Map",relief=SUNKEN)
lenl=Label(lblframe,text="Line Len = ",relief=SUNKEN)
lenl.pack(side=RIGHT,padx=5,pady=5)
lenh=Label(lblframe,text="Encode to find length",relief=SUNKEN)
lenh.pack(side=RIGHT,padx=5,pady=5)
map.pack(side=RIGHT,padx=5,pady=5)
lblframe.grid(row=0,column=0)
txtf = Frame(strf)
text = ScrolledText(txtf,width=56)
inp = Label(txtf,text="String Input")
inp.pack()
text.pack(expand=1)

txtf.grid(row=2,column=0)

hxf = Frame(strf)
hext = ScrolledText(hxf,width=5,height=5)
outp = Label(hxf,text="Hex Output")

outp.pack()
hext.pack()

#config file format just be a tuple
#0 will be plain text user sees on button
#1 will be the entire hex string
HB = genfromtxt(r'config.txt', delimiter=',',dtype=str)
hbf = Frame(strf)
hotbar=Label(hbf,text="COMMONLY USED CMDS",relief=RAISED)
HBD={}
for i,x in enumerate(HB):
	HBD[x[0]]=x[1]
	hbb={}
	hbb[x[0]] = Button(hbf, text=x[0])
	hbb[x[0]].config(command=functools.partial(addhotbar, x[1]))
	hbb[x[0]].grid(row=i%10+1,column=i//10)

hotbar.grid(row=0,column=0)
hbf.grid(row=3,column=0)
strf.pack(side=BOTTOM,expand=1,fill=X,padx=30)



menubar = Menu(root)
name = file_menu.main(root, text, menubar,map,COutput,HexOutput)
edit_menu.main(root, text, menubar)
format_menu.main(root, text, menubar)
help_menu.main(root, text, menubar)
root.title("Text Engine Encoder - %s" %name.filename)
root.bind_all('<KeyPress>',getLineLen)
root.mainloop()