@@T_offset equ 0x000000-@@filestart

;---------------------------------------------------

;put the file names of your strings here.
;to expand, just change the number

@@string0 equ "File Select.txt.bin"
@@string1 equ "CG directions.txt.bin"
@@string2 equ "guild shoutouts.txt.bin"
@@string3 equ "cg astolfo.txt.bin"
@@string4 equ "CG level hints.txt.bin"
@@string5 equ "CG star guild motto.txt.bin"
@@string6 equ "cg star protect guide.txt.bin"
@@string7 equ "CG guild secret.txt.bin"
@@string8 equ "cg dorrie diary.txt.bin"

;---------------------------------------------------

@@filestart:

;copy&paste and change number to add strings

.dw @@T_offset+@@string0start ;string0
.dw @@T_offset+@@string1start ;string1
.dw @@T_offset+@@string2start ;string2
.dw @@T_offset+@@string3start ;string3
.dw @@T_offset+@@string4start ;string4
.dw @@T_offset+@@string5start ;string5
.dw @@T_offset+@@string6start ;string6
.dw @@T_offset+@@string7start ;string7
.dw @@T_offset+@@string8start ;string8

;---------------------------------------------------

;copy paste these and change number to match
;number of strings
@@string0start:
.incbin @@string0

@@string1start:
.incbin @@string1

@@string2start:
.incbin @@string2

@@string3start:
.incbin @@string3

@@string4start:
.incbin @@string4

@@string5start:
.incbin @@string5

@@string6start:
.incbin @@string6

@@string7start:
.incbin @@string7

@@string8start:
.incbin @@string8

;---------------------------------------------------

CG_STRING_END: