;These are npcs that you can talk to.
;It includes:
;Dorrie
;Scuttlebugs
;Bob ombs
;Koopas
;You can use a sign on wall by setting model ID=0

NPC_asm_start equ 0x1206800
NPC_asm_start_Virtual equ 0x80406800
NPC_bhv_start equ 0x1206Ff8
NPC_bhv_start_Segment equ 0x00406Ff8

;You need to calculate virtual and segmented addr
;This is so you can put this data in any segment.
;If you put dont know where, put it in 120xxxx region
;From there the segment is 0x00400000+xxxx
;Virtual is 0x80400000+xxxx

;All npcs work the same, you need to face them,
;be equal or higher in Y pos than them, stand
;still and then press B. These NPCs stop time.

;There is also one object that displays a string
;on level start. This object does not stop time.
;There is another object that displays a string
;when mario is in a range decided by bparam 2.

;see bhv notes file for details on each object.

;---------------------------------------------------

;The following is the startpoint for the text engine
;It is very long. Please reserve around 0x2500 bytes
;in the rom for it. If you dont know, leave it at
;the default.

Text_Engine_Start equ 0x1203800
Text_Engine_Start_Virtual equ 0x80403800

;Calculate the virtual the same way as above section

;---------------------------------------------------

;The following are some useful functions for text
;engine usage. I used them to create menus in SSA.
;These are used with a0, and a1 cmds in text engine.
;Some will have return values for branching, others
;will do an operation. When using them make sure you
;use the right index so you can branch when necessary.

helpful_asm_functions equ 0x1206b00
helpful_asm_functions_Virtual equ 0x80406b00

;When applied this should generate a config file.
;Copy that data and add it to the config to use
;the functions inside the python tool.

;---------------------------------------------------

;When using the text engine, you will need to manage
;the rom space for your strings yourself. I recommend
;making your own bank for this. I have setup all the
;custom objects to work of that idea, and take an index
;into a table as input.

;This will edit the master levelscript. If you
;want to do it with your own method or dont
;want to edit the master level script then
;ignore this section and comment out string
;tables at the bottom.

;You can have a string bank loaded globally,
;or have one in each stage, or both.
;To use them type in "1" in the
;appropriate line. Leave it at "0" to
;not use it.

;The global table uses bank 0x1A, and the per level
;table uses bank 0x1B. To denote which one you
;are using, put a 0 in bparam 1 for global, and a
;1 in bparam 1 to use the per level. Bparam 4 will
;control the string index (0 for the first, 1 for
;second etc.) Bparam 3 decided if the object gets
;stored to the object index. The index is 1 less
;than the bparam value (e.g. 1 gets index 0)
;Bparam 2 decides the range at which the objects
;activate, (bparam*8)

USE_GLOBAL_STRING_BANK equ 1

USE_BOB_BANK equ 1
USE_WF_BANK equ 1
USE_JRB_BANK equ 1
USE_CCM_BANK equ 0
USE_BBH_BANK equ 0
USE_HMC_BANK equ 0
USE_LLL_BANK equ 0
USE_SSL_BANK equ 0
USE_DDD_BANK equ 0
USE_SL_BANK equ 0
USE_WDW_BANK equ 0
USE_TTM_BANK equ 0
USE_THI_BANK equ 0
USE_TTC_BANK equ 0
USE_RR_BANK equ 0

USE_IC_BANK equ 1
USE_CG_BANK equ 1
USE_CC_BANK equ 0 ;CASTLE COURTYARD
USE_PSS_BANK equ 0
USE_SA_BANK equ 0
USE_ECS_BANK equ 0 ;END CAKE
USE_WC_BANK equ 0
USE_MC_BANK equ 0
USE_VC_BANK equ 0
USE_RC_BANK equ 0

USE_BITDW_BANK equ 0
USE_BFS_BANK equ 0
USE_BITS_BANK equ 0

;these are the rom locations, just type the
;starting spot. You can overwrite one bank
;with another if you aren't careful so check
;after you import.

Global_String_Table equ 0x2000000

BoB_String_Table equ 0x2100000
WF_String_Table equ 0x2130000
JRB_String_Table equ 0x2140000
CCM_String_Table equ 0x2000000
BBH_String_Table equ 0x2000000
HMC_String_Table equ 0x2000000
LLL_String_Table equ 0x2000000
SSL_String_Table equ 0x2000000
DDD_String_Table equ 0x2000000
SL_String_Table equ 0x2000000
WDW_String_Table equ 0x2000000
TTM_String_Table equ 0x2000000
THI_String_Table equ 0x2000000
TTC_String_Table equ 0x2000000
RR_String_Table equ 0x2000000

IC_String_Table equ 0x2120000
CG_String_Table equ 0x2110000
CC_String_Table equ 0x2000000
PSS_String_Table equ 0x2000000
SA_String_Table equ 0x2000000
ECS_String_Table equ 0x2000000
RC_String_Table equ 0x2000000
WC_String_Table equ 0x2000000
MC_String_Table equ 0x2000000
VC_String_Table equ 0x2000000

BITDW_String_Table equ 0x2000000
BFS_String_Table equ 0x2000000
BITS_String_Table equ 0x2000000

;---------------------------------------------------

;To make sure your string tables import, you need
;to make a list of the strings. When you finish
;and want to test, hit export to bin on the
;python tool. it should save it as the file name
;of your string with a .bin file type

;Now you should have a file for each course
;and one for the global. They should be called
;"LEVEL_STRINGS.asm" with Level being replaced by
;the name of the course. Inside that file, just
;follow the instructions shown in that file to
;add your strings to the level.

;---------------------------------------------------

;These are the files being compiled. Dont edit
;these unless you know asm. You can comment it
;out if you dont want a section if needed.

.include "print string notes.txt"
.include "NPC_text_engine.asm"
.include "string_tables.asm"
.include "helper_asm.asm"