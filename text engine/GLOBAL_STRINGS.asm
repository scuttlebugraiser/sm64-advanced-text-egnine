@@T_offset equ 0x000000-@@filestart

;---------------------------------------------------

;put the file names of your strings here.
;to expand, just change the number

@@string0 equ "File Select.txt.bin"
@@string1 equ "intro cutscene.txt.bin"
@@string2 equ "global anim test.txt.bin"

;---------------------------------------------------

@@filestart:

;copy&paste and change number to add strings

.dw @@T_offset+@@string0start ;string0
.dw @@T_offset+@@string1start ;string1
.dw @@T_offset+@@string2start ;string2

;---------------------------------------------------

;copy paste these and change number to match
;number of strings
@@string0start:
.incbin @@string0

@@string1start:
.incbin @@string1

@@string2start:
.incbin @@string2

;---------------------------------------------------
GLOBAL_STRING_END: