@@T_offset equ 0x000000-@@filestart

;---------------------------------------------------

;put the file names of your strings here.
;to expand, just change the number

@@string0 equ "bob.txt.bin"
;@@string1 equ "your 2nd string here.bin"

;---------------------------------------------------

@@filestart:

;copy&paste and change number to add strings

.dw @@T_offset+@@string0start ;string0
;.dw @@T_offset+@@string1start ;string1

;---------------------------------------------------

;copy paste these and change number to match
;number of strings
@@string0start:
.incbin @@string0

;@@string1start:
;.incbin @@string1

;---------------------------------------------------

BFS_STRING_END: