;Load bank 12 routine.
.orga 0x2ABCDC
.dw 0x170C001C
.dw 0x002AB6B0
.dw Bank_12_End
.dw 0x1D040000
.dw 0x10040000
.dd 0x2208000117002DD4

.orga 0x2abf1c
.dd 0x050800001C000000
.dw 0x10040000


.orga 0x2AB6B0
.dw 0x1B040000

.if USE_GLOBAL_STRING_BANK
.dw 0x170c001A
.dw GLOBAL_STRING_TABLE
.dw GLOBAL_STRING_END
.endif

HDSZ equ 0x1C000000-0x2ab6b0


.if USE_BOB_BANK
.dw 0x0C0C0200
.dw 0x09
.dw @@BOB_Load+HDSZ
.endif

.if USE_WF_BANK
.dw 0x0C0C0200
.dw 0x18
.dw @@WF_Load+HDSZ
.endif

.if USE_JRB_BANK
.dw 0x0C0C0200
.dw 0x0C
.dw @@JRB_Load+HDSZ
.endif

.if USE_CCM_BANK
.dw 0x0C0C0200
.dw 0x05
.dw @@CCM_Load+HDSZ
.endif

.if USE_BBH_BANK
.dw 0x0C0C0200
.dw 0x04
.dw @@BBH_Load+HDSZ
.endif

.if USE_HMC_BANK
.dw 0x0C0C0200
.dw 0x07
.dw @@HMC_Load+HDSZ
.endif

.if USE_LLL_BANK
.dw 0x0C0C0200
.dw 0x16
.dw @@LLL_Load+HDSZ
.endif

.if USE_SSL_BANK
.dw 0x0C0C0200
.dw 0x08
.dw @@SSL_Load+HDSZ
.endif

.if USE_DDD_BANK
.dw 0x0C0C0200
.dw 0x17
.dw @@DDD_Load+HDSZ
.endif

.if USE_SL_BANK
.dw 0x0C0C0200
.dw 0x0A
.dw @@SL_Load+HDSZ
.endif

.if USE_WDW_BANK
.dw 0x0C0C0200
.dw 0x0B
.dw @@WDW_Load+HDSZ
.endif

.if USE_TTM_BANK
.dw 0x0C0C0200
.dw 0x24
.dw @@TTM_Load+HDSZ
.endif

.if USE_THI_BANK
.dw 0x0C0C0200
.dw 0x0D
.dw @@THI_Load+HDSZ
.endif

.if USE_TTC_BANK
.dw 0x0C0C0200
.dw 0x0E
.dw @@TTC_Load+HDSZ
.endif

.if USE_RR_BANK
.dw 0x0C0C0200
.dw 0x0F
.dw @@RR_Load+HDSZ
.endif

.if USE_IC_BANK
.dw 0x0C0C0200
.dw 0x06
.dw @@IC_Load+HDSZ
.endif

.if USE_CC_BANK
.dw 0x0C0C0200
.dw 0x1A
.dw @@CC_Load+HDSZ
.endif

.if USE_CG_BANK
.dw 0x0C0C0200
.dw 0x10
.dw @@CG_Load+HDSZ
.endif

.if USE_PSS_BANK
.dw 0x0C0C0200
.dw 0x1B
.dw @@PSS_Load+HDSZ
.endif

.if USE_SA_BANK
.dw 0x0C0C0200
.dw 0x14
.dw @@SA_Load+HDSZ
.endif

.if USE_RC_BANK
.dw 0x0C0C0200
.dw 0x1E
.dw @@RC_Load+HDSZ
.endif

.if USE_ECS_BANK
.dw 0x0C0C0200
.dw 0x19
.dw @@ECS_Load+HDSZ
.endif

.if USE_WC_BANK
.dw 0x0C0C0200
.dw 0x1D
.dw @@WC_Load+HDSZ
.endif

.if USE_VC_BANK
.dw 0x0C0C0200
.dw 0x12
.dw @@VC_Load+HDSZ
.endif

.if USE_MC_BANK
.dw 0x0C0C0200
.dw 0x1C
.dw @@MC_Load+HDSZ
.endif

.if USE_BITDW_BANK
.dw 0x0C0C0200
.dw 0x11
.dw @@BITDW_Load+HDSZ
.endif

.if USE_BFS_BANK
.dw 0x0C0C0200
.dw 0x13
.dw @@BFS_Load+HDSZ
.endif

.if USE_BITS_BANK
.dw 0x0C0C0200
.dw 0x15
.dw @@BITS_Load+HDSZ
.endif

@@Bank_12_Return:
.dd 0x2208009617000038 
.dd 0x22080095170001BC

;overwritten models from above
.dw 0x0C0C0200
.dw 0x00000004
.dw 0x150003F4
.dw 0x05080000
.dw 0x15000288

.headersize 0


@@BOB_Load:
.if USE_BOB_BANK

.dw 0x170c001B
.dw BOB_STRING_TABLE
.dw BOB_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@WF_Load:
.if USE_WF_BANK

.dw 0x170c001B
.dw WF_STRING_TABLE
.dw WF_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@JRB_Load:
.if USE_JRB_BANK

.dw 0x170c001B
.dw JRB_STRING_TABLE
.dw JRB_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@CCM_Load:
.if USE_CCM_BANK

.dw 0x170c001A
.dw CCM_STRING_TABLE
.dw CCM_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@BBH_Load:
.if USE_BBH_BANK

.dw 0x170c001B
.dw BBH_STRING_TABLE
.dw BBH_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@HMC_Load:
.if USE_HMC_BANK

.dw 0x170c001B
.dw HMC_STRING_TABLE
.dw HMC_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@LLL_Load:
.if USE_LLL_BANK

.dw 0x170c001B
.dw LLL_STRING_TABLE
.dw LLL_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@SSL_Load:
.if USE_SSL_BANK

.dw 0x170c001B
.dw SSL_STRING_TABLE
.dw SSL_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@DDD_Load:
.if USE_DDD_BANK

.dw 0x170c001B
.dw DDD_STRING_TABLE
.dw DDD_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@SL_Load:
.if USE_SL_BANK

.dw 0x170c001B
.dw SL_STRING_TABLE
.dw SL_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@WDW_Load:
.if USE_WDW_BANK

.dw 0x170c001B
.dw WDW_STRING_TABLE
.dw WDW_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@TTM_Load:
.if USE_TTM_BANK

.dw 0x170c001B
.dw TTM_STRING_TABLE
.dw TTM_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@THI_Load:
.if USE_THI_BANK

.dw 0x170c001B
.dw THI_STRING_TABLE
.dw THI_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@TTC_Load:
.if USE_TTC_BANK

.dw 0x170c001B
.dw TTC_STRING_TABLE
.dw TTC_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@RR_Load:
.if USE_RR_BANK

.dw 0x170c001B
.dw RR_STRING_TABLE
.dw RR_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@IC_Load:
.if USE_IC_BANK

.dw 0x170c001B
.dw IC_STRING_TABLE
.dw IC_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@CC_Load:
.if USE_CC_BANK

.dw 0x170c001B
.dw CC_STRING_TABLE
.dw CC_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@CG_Load:
.if USE_CG_BANK

.dw 0x170c001B
.dw CG_STRING_TABLE
.dw CG_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@PSS_Load:
.if USE_PSS_BANK

.dw 0x170c001B
.dw PSS_STRING_TABLE
.dw PSS_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@SA_Load:
.if USE_SA_BANK

.dw 0x170c001B
.dw SA_STRING_TABLE
.dw SA_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@RC_Load:
.if USE_RC_BANK

.dw 0x170c001B
.dw RC_STRING_TABLE
.dw RC_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@ECS_Load:
.if USE_ECS_BANK

.dw 0x170c001B
.dw ECS_STRING_TABLE
.dw ECS_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@WC_Load:
.if USE_WC_BANK

.dw 0x170c001B
.dw WC_STRING_TABLE
.dw WC_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@MC_Load:
.if USE_MC_BANK

.dw 0x170c001B
.dw MC_STRING_TABLE
.dw MC_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@VC_Load:
.if USE_VC_BANK

.dw 0x170c001B
.dw VC_STRING_TABLE
.dw VC_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@BITDW_Load:
.if USE_BITDW_BANK

.dw 0x170c001B
.dw BITDW_STRING_TABLE
.dw BITDW_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@BFS_Load:
.if USE_BFS_BANK

.dw 0x170c001B
.dw BFS_STRING_TABLE
.dw BFS_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
@@BITS_Load:
.if USE_BITS_BANK

.dw 0x170c001B
.dw BITS_STRING_TABLE
.dw BITS_STRING_END

.dw 0x05080000

.dw @@Bank_12_Return+HDSZ
.endif
Bank_12_End:

.if USE_GLOBAL_STRING_BANK
.orga Global_String_Table
.include "GLOBAL_STRINGS.asm"
.endif

.if USE_BOB_BANK
.orga BoB_String_Table
.include "BOB_STRINGS.asm"
.endif

.if USE_WF_BANK
.orga WF_String_Table
.include "WF_STRINGS.asm"
.endif

.if USE_JRB_BANK
.orga JRB_String_Table
.include "JRB_STRINGS.asm"
.endif

.if USE_CCM_BANK
.orga CCM_String_Table
.include "CCM_STRINGS.asm"
.endif

.if USE_BBH_BANK
.orga BBH_String_Table
.include "BBH_STRINGS.asm"
.endif

.if USE_HMC_BANK
.orga HMC_String_Table
.include "HMC_STRINGS.asm"
.endif

.if USE_LLL_BANK
.orga LLL_String_Table
.include "LLL_STRINGS.asm"
.endif

.if USE_SSL_BANK
.orga SSL_String_Table
.include "SSL_STRINGS.asm"
.endif

.if USE_DDD_BANK
.orga DDD_String_Table
.include "DDD_STRINGS.asm"
.endif

.if USE_SL_BANK
.orga SL_String_Table
.include "SL_STRINGS.asm"
.endif

.if USE_WDW_BANK
.orga WDW_String_Table
.include "WDW_STRINGS.asm"
.endif

.if USE_TTM_BANK
.orga TTM_String_Table
.include "TTM_STRINGS.asm"
.endif

.if USE_THI_BANK
.orga THI_String_Table
.include "THI_STRINGS.asm"
.endif

.if USE_TTC_BANK
.orga TTC_String_Table
.include "TTC_STRINGS.asm"
.endif

.if USE_RR_BANK
.orga RR_String_Table
.include "RR_STRINGS.asm"
.endif

.if USE_IC_BANK
.orga IC_String_Table
.include "IC_STRINGS.asm"
.endif

.if USE_CG_BANK
.orga CG_String_Table
.include "CG_STRINGS.asm"
.endif

.if USE_CC_BANK
.orga CC_String_Table
.include "CC_STRINGS.asm"
.endif

.if USE_PSS_BANK
.orga PSS_String_Table
.include "PSS_STRINGS.asm"
.endif

.if USE_SA_BANK
.orga SA_String_Table
.include "SA_STRINGS.asm"
.endif

.if USE_ECS_BANK
.orga ECS_String_Table
.include "ECS_STRINGS.asm"
.endif

.if USE_WC_BANK
.orga WC_String_Table
.include "WC_STRINGS.asm"
.endif

.if USE_MC_BANK
.orga MC_String_Table
.include "MC_STRINGS.asm"
.endif

.if USE_VC_BANK
.orga VC_String_Table
.include "VC_STRINGS.asm"
.endif

.if USE_BITDW_BANK
.orga BITDW_String_Table
.include "BITDW_STRINGS.asm"
.endif

.if USE_BFS_BANK
.orga BFS_String_Table
.include "BFS_STRINGS.asm"
.endif

.if USE_BITS_BANK
.orga BITS_String_Table
.include "BITS_STRINGS.asm"
.endif

;changing init level procedure to not execute twice
.orga 0x000FBBC8;8037ee48
addiu sp, sp, -0x18
sw ra, 0x14 (SP)
lui at, 0x8080
lb t0, 0xffff (AT);level init bool of bad code
nop;bnez t0, endinit
or a0, r0, r0
li a1, 0x8038bd88
jal 0x8037b448 ;init graph node start
sb a1, 0xffff (AT)
jal 0x8029d1e8 ;clear objects
nop
jal 0x8027ab04 ;clear areas
nop
jal 0x802783e8 ;main pool push state
nop
endinit:
lui at, 0x8039
lw t6, 0xbe28 (AT) ;curr lvl script cmd
lbu t7, 0x1 (T6) ;len cmd
addu t8, t7, t6
sw t8, 0xbe28 (AT)
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18

;clear level cmd
.orga 0x000FBC28;8037eea8
addiu sp, sp, -0x18
sw ra, 0x14 (SP)
jal 0x8029d1e8 ;clear objects
nop
jal 0x8027ad74 ;clear area graph nodes
nop
jal 0x8027ab04 ;clear areas
nop
jal 0x80278498 ;main pool pop state
nop
sb r0, 0x807fffff ;clear level init bool of bad code
lui at, 0x8039
lw t6, 0xbe28 (AT) ;curr lvl script cmd
lbu t7, 0x1 (T6) ;len cmd
addu t8, t7, t6
sw t8, 0xbe28 (AT)
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18