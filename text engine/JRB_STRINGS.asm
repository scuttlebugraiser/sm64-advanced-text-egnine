@@T_offset equ 0x000000-@@filestart

;---------------------------------------------------

;put the file names of your strings here.
;to expand, just change the number

@@string0 equ "aldis start.txt.bin"
@@string1 equ "aldis GTM.txt.bin"
@@string2 equ "aldis shop clerk.txt.bin"
@@string3 equ "aldis instant proposal.txt.bin"
@@string4 equ "aldis team member.txt.bin"
@@string5 equ "aldis first target.txt.bin"

@@string6 equ "aldis conquest start.txt.bin"
@@string7 equ "aldis second girl.txt.bin"
@@string8 equ "aldis star trader.txt.bin"

@@string9 equ "aldis get star.txt.bin"
;---------------------------------------------------

@@filestart:

;copy&paste and change number to add strings

.dw @@T_offset+@@string0start ;string0
.dw @@T_offset+@@string1start ;string1
.dw @@T_offset+@@string2start ;string2
.dw @@T_offset+@@string3start ;string3
.dw @@T_offset+@@string4start ;string4
.dw @@T_offset+@@string5start ;string5

.dw @@T_offset+@@string6start ;string6
.dw @@T_offset+@@string7start ;string7
.dw @@T_offset+@@string8start ;string8

.dw @@T_offset+@@string9start ;string9
;---------------------------------------------------

;copy paste these and change number to match
;number of strings
@@string0start:
.incbin @@string0

@@string1start:
.incbin @@string1

@@string2start:
.incbin @@string2

@@string3start:
.incbin @@string3

@@string4start:
.incbin @@string4

@@string5start:
.incbin @@string5

@@string6start:
.incbin @@string6

@@string7start:
.incbin @@string7

@@string8start:
.incbin @@string8

@@string9start:
.incbin @@string9

;---------------------------------------------------

JRB_STRING_END: