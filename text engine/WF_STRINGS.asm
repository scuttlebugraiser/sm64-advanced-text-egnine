@@T_offset equ 0x000000-@@filestart

;---------------------------------------------------

;put the file names of your strings here.
;to expand, just change the number

@@string0 equ "creep creek start.txt.bin"
@@string1 equ "creep creek coin 1.txt.bin"
@@string2 equ "creep creek coin 2.txt.bin"
@@string3 equ "creep creek coin 3.txt.bin"
@@string4 equ "creep creek steve intro.txt.bin"
@@string5 equ "creep creek steve trade.txt.bin"

;---------------------------------------------------

@@filestart:

;copy&paste and change number to add strings

.dw @@T_offset+@@string0start ;string0
.dw @@T_offset+@@string1start ;string1
.dw @@T_offset+@@string2start ;string2
.dw @@T_offset+@@string3start ;string3
.dw @@T_offset+@@string4start ;string4
.dw @@T_offset+@@string5start ;string5

;---------------------------------------------------

;copy paste these and change number to match
;number of strings
@@string0start:
.incbin @@string0

@@string1start:
.incbin @@string1

@@string2start:
.incbin @@string2

@@string3start:
.incbin @@string3

@@string4start:
.incbin @@string4

@@string5start:
.incbin @@string5

;---------------------------------------------------

WF_STRING_END: