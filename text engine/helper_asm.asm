.orga helpful_asm_functions

@star_check_branch:
lh at, 0x8033b21a //whatever num stars is
jr ra
sltu v0, at, a0
//v0 is one when you fail star check

@set_sound_option:
addiu sp, sp, -0x1C
sw ra, 0x14 (sp)
lui a0, 0x807f
jal 0x8027a564
lbu a0, 0x6d58 (a0)
lw ra, 0x14 (sp)
jr ra
addiu sp, sp, 0x1C

@mario_spawn_halt:
addiu sp, sp, -0x18
sw ra, 0x14 (sp)
lw t6, 0x8032ddc4
lw t1, 0x80361158
lw t9, 0x0 (T6)
sw t9, 0x14 (T1)
lui t0, 0x8034
sh r0, 0xb26a (T0)
beq a0, r0, @@end
//disable mario
sw r0, 0xb17c (t0)
li t2, 0x0C400201
li at, 2
sw t2, 0xb17c (t0)
sh at, 0x8033B248
lw t9, 0x4 (T6)
sw t9, 0x14 (T1)
li at, 0x3d
sh at, 0xb26a (T0)
@@end:
lw ra, 0x14 (sp)
jr ra
addiu sp, sp, 0x18

@erase_file:
addiu sp, sp, -0x1C
sw ra, 0x14 (SP)
lui a0, 0x807f
jal 0x802798fc //save_file_erase
lbu a0, 0x6d58 (A0)

lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x1C

@display_star_count:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)
sw a1, 0x18 (SP)
li a1,0
jal 0x8027a010 //save_file_get_total_star_count
li a2, 0x18
lw a1, 0x18 (SP)
li at, 0xa
divu v0, at
mflo v1
mfhi v0
sb v0, 0xE (A1)
sb v1, 0xD (A1)
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18

@copy_file:
addiu sp, sp, -0x1C
sw ra, 0x14 (SP)
beq a0, r0, @@setuprestore
lui t0, 0x807f
lw a0, 0x6da8 (T0)
jal 0x80279960
lbu a1, 0x6d58 (T0)
@@setuprestore:
lui a0, 0x807f
lbu v0, 0x6d58 (A0)
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x1C

@select_file:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)

lui a0, 0x807f
lbu a0, 0x6d58 (A0)
lui at, 0x8033
addiu a0, a0, 1
jal 0x80254f44 //init mario from save
sh a0, 0xDDF4 (AT)
jal 0x80254b20 //init mario
nop
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18


@@checkeepflag:
lh t0, 0x8032ddf4
subiu t0, t0, 1
sll t1, t0, 7;times 0x80
sll t2, t0, 4;times 0x10
subu t1, t1, t2 ;times 0x70
lui t0, 0x8020
addu t0, t0, t1
lw t7, 0x7708 (t0)
and t9, a0, t7
beqz t9, noflag
or v0, r0, r0
li v0, 1
noflag:
jr ra
nop

@@RunStringOncePerBoot:
beq a0, r0, @@init
li at, 1
jr ra
li v0, 1

@@init:
sb at, 0x0 (T8)
jr ra
li v0, 0

@@BranchOnObjArray:
sll t1, a0, 2
addu at, t1, s7
lw v0, 0x6e80 (at)
jr ra
sltu v0, v0, 1

@@BranchOnTopObj:
sll t1, a0, 2
addu at, t1, s7
lw t0, 0x6e80 (at)
lw t9, 0x80361158
lw t8, 0x214 (T9)
beq t8, t0, @@onobj
li v0, 0
addiu v0, v0, -1
@@onobj:
jr ra
addiu v0, v0, 1