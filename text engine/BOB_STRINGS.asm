@@T_offset equ 0x000000-@@filestart

;---------------------------------------------------

;put the file names of your strings here.
;to expand, just change the number

@@string0 equ "volcano dorrie text start.txt.bin"
@@string1 equ "volcano pink bob omb.txt.bin"
@@string2 equ "volcano island koopa.txt.bin"
@@string3 equ "volcano island chuckya.txt.bin"

;---------------------------------------------------

@@filestart:

;copy&paste and change number to add strings

.dw @@T_offset+@@string0start ;string0
.dw @@T_offset+@@string1start ;string1
.dw @@T_offset+@@string2start ;string2
.dw @@T_offset+@@string3start ;string3

;---------------------------------------------------

;copy paste these and change number to match
;number of strings
@@string0start:
.incbin @@string0

@@string1start:
.incbin @@string1

@@string2start:
.incbin @@string2

@@string3start:
.incbin @@string3

;---------------------------------------------------

BOB_STRING_END:
