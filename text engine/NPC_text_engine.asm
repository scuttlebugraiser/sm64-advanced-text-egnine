//behavior for talking dorrie

.headersize(NPC_asm_start_Virtual-NPC_asm_start)

;.orga dorrie_bhv_start
.orga NPC_bhv_start
.dw 0x00090000
.dw 0x11012041
.dd 0x272600000600f638
.dw 0x28010000
.dw 0x0e437530
.dw 0x2a000000
.dw 0x0600f644
@@npc_bhv_loop:
.dw 0x08000000
.dw 0x0c000000
.dw NPC_asm_start_Virtual//darrie og asm
.dw 0x0c000000
.dw 0x803839cc
.dw 0x09000000

.dw 0x10050000
.dw 0x08000000
.dw 0x102B0000
.dw 0x0c000000
.dw NPC_asm_start_Virtual//darrie og asm
.dw 0x09000000


;.orga scuttlebug_bhv_start
.dw 0x00040000
.dw 0x11012041
.dd 0x2726000006015064
.dw 0x28000000
.dw 0x2f000000
.dw 0x40000000
.dw 0x23000000
.dw 0x00640064
.dw 0x04000000
.dw NPC_bhv_start_Segment+0x38

;.orga bob_omb_bhv_start
.dw 0x00040000
.dw 0x11012041
.dd 0x272600000802396c
.dw 0x28000000
.dw 0x2f000000
.dw 0x40000000
.dw 0x23000000
.dw 0x00640064
.dw 0x04000000
.dw NPC_bhv_start_Segment+0x38

;.orga koopa_bhv_start
.dw 0x00040000
.dw 0x11012041
.dd 0x2726000006011364
.dw 0x28090000
.dw 0x2f000000
.dw 0x40000000
.dw 0x23000000
.dw 0x00640064
.dw 0x04000000
.dw NPC_bhv_start_Segment+0x38

;string on level start
.dw 0x00040000
.dw 0x0c000000
.dw text_level_start_asm
.dw 0x0a000000

;proximitystring
.dw 0x00040000
.dw 0x11012041
.dw 0x08000000
.dw 0x0c000000
.dw proximitystring
.dw 0x09000000

;chuckya
.dw 0x00040000
.dw 0x11012041
.dd 0x272600000800c070
.dw 0x28050000
.dw 0x2f000000
.dw 0x40000000
.dw 0x23000000
.dw 0x00640064
.dw 0x04000000
.dw NPC_bhv_start_Segment+0x38

;goomba
.dw 0x00040000
.dw 0x11012041
.dd 0x272600000801da4c
.dw 0x28000000
.dw 0x2f000000
.dw 0x40000000
.dw 0x23000000
.dw 0x00640064
.dw 0x04000000
.dw NPC_bhv_start_Segment+0x38


.orga NPC_asm_start

@@dorrie_asm:

addiu sp, sp, -0x18
sw ra, 0x14 (SP)

lw t0, 0x80361160
lb at, 0x188 (T0)
sw at, 0xf0 (T0)
lw t1, 0x14c (T0)
bne t1, r0, @@incutscene
lw t2, 0x154 (T0)
subiu t2, t2, 0x30
blez t2, @@endbob
lwc1 f0, 0x15c (T0)
lbu t9, 0x189 (T0)
sll t9, t9, 3
mtc1 t9, f8
cvt.s.w f8, f8
c.lt.s f0, f8
lw a1, 0x160 (T0)
bc1f @@endbob
lw a0, 0xd4 (T0)
jal 0x8029E530 //rotate towards obj
li a2, 0x0800
lw t0, 0x80361160
lui t8, 0x8034
lwc1 f2, 0xa4 (T0) //dorrie y
lwc1 f4, 0xb1b0 (T8) //mario y
c.lt.s f4, f2
lh t9, 0xb194 (T8)
bc1t @@endbob
subu t3, v0, t9 
sw v0, 0xc8 (T0)
sw v0, 0xd4 (T0)
addiu t3, t3, 0xA000
andi t3, t3, 0xffff
sltiu t3, t3, 0x4001
blez t3, @@endbob
lh t7, 0xafa2 (T8)
andi t7, t7, 0x4000
beq t7, r0, @@endbob
lw t6, 0xb17c (T8)
li at, 0x00800457 //sliding punch
beq at, t6, @@startdiag
andi t6, t6, 0x1c0
beq t6, r0, @@startdiag
li t5, 0x180
bne t5, t6, @@endbob
@@startdiag:
addiu a0, t8, 0xb170
li a2, 0x20001306 //reading sign
jal 0x80252cf4
li a1, 0

lw t0, 0x80361160
lbu a2, 0x18A (T0) //obj array index-1
beq a2, r0, @@noobjinarray
lui at, 0x807f
sll a2, a2, 2
addu at, at, a2
sw t0, 0x6e7C (AT)
@@noobjinarray:
lbu a2, 0x188 (T0)
sll a2, a2, 24
lui a0, 0x1a00
jal 0x80277f50
addu a0, a0, a2
lw t0, 0x80361160
lbu a2, 0x18B (T0)
sll a2, a2, 2
addu a2, a2, v0
lw a2, 0x0 (A2)
addu a2, a2, v0
li a0, 0x1a
jal STARTDIALOGWITHTHISOBJFUNC
li a1, 0xc0
lw t0, 0x80361160
li t9, 1
sw t9, 0x14c (T0)
li a0, 0x8033b170
jal 0x802509b8 //set mario anim
li a1, 0xc2 //anim ID
beq r0, r0, @@endbob


@@incutscene:
lw t8, 0x807f6df8
bne t8, r0, @@endbob
lui t7, 0x8034
li t6, 0x0C400201 //idle
sw t6, 0xb17c (T7)
sw r0, 0x14c (T0)
lui at, 0x8034
sw r0, 0xd480 (AT)
@@endbob:
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18


text_level_start_asm:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)

lw t0, 0x80361160
lbu a2, 0x18A (T0) //obj array index-1
beq a2, r0, @@noobjinarraystart
lui at, 0x807f
sll a2, a2, 2
addu at, at, a2
sw t0, 0x6e7C (AT)
@@noobjinarraystart:
lbu a2, 0x188 (T0)
sll a2, a2, 24
lui a0, 0x1a00
jal 0x80277f50
addu a0, a0, a2
lw t0, 0x80361160
lbu a2, 0x18B (T0)
sll a2, a2, 2
addu a2, a2, v0
lw a2, 0x0 (A2)
addu a2, a2, v0
li a1, 0x50
jal SETUPSTRING
li a0, 0x10

lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18


proximitystring:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)

lw t0, 0x80361160
lwc1 f0, 0x15c (T0)
lbu t9, 0x189 (T0)
sll t9, t9, 3
mtc1 t9, f8
cvt.s.w f8, f8
c.lt.s f0, f8
lw a1, 0x160 (T0)
bc1f @@endproximity
nop
jal text_level_start_asm
nop

lw t0, 0x80361160
sh r0, 0x74 (T0)

@@endproximity:
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18



.headersize(0)