#ifndef _TEST_H
#define _TEST_H

extern unsigned char TextEngine[];
void SetupTextEngine(s16 x, s16 y, const u8 *str);
void RunTextEngine(void);
#endif