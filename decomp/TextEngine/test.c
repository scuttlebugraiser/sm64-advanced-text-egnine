#include <ultra64.h>
#include "test.h"
#include "sm64.h"

//definitions of stuff I need inside the text engine
extern void print_generic_string(s16 x, s16 y, const u8 *str);
extern u8 gDialogCharWidths[256];
extern Gfx *gDisplayListHead;
extern u32 *sNumVblanks;
extern Gfx *dl_ia_text_end;
extern Gfx *dl_ia_text_begin;
extern Gfx *dl_ia_text_tex_settings;
extern Gfx *dl_draw_text_bg_box_TE;
extern void create_dl_translation_matrix(s8 pushOp, f32 x, f32 y, f32 z);
extern void create_dl_scale_matrix(s8 pushOp, f32 x, f32 y, f32 z);
extern void create_dl_ortho_matrix(void);
extern void handle_menu_scrolling(s8 scrollDirection, s8 *currentIndex, s8 minIndex, s8 maxIndex);
extern float sinf(float);
extern struct Controller gControllers;
extern void play_sequence(u8 player, u8 seqId, u16 fadeTimer);
extern void play_sound(s32 soundBits, f32 *pos);
extern f32 gDefaultSoundArgs[3];
extern struct MarioState gMarioStates[1];
extern struct Object cur_obj_nearest_object_with_behavior(const BehaviorScript *);
extern s16 set_mario_animation(struct MarioState *m,s32 targetAnimID);
extern s16 atan2s(f32 y, f32 x);
extern f32 find_floor_height(f32x,f32y,f32z);
extern u32 gTimeStopState;
extern s32 absi(s32 a0);
//stuff will show up in memory in reverse order after the functions.
//Data (aka text engine bin) will show up last, because I put it in the linker last.
//The first to show up will be all this data, so it will appear at the exactly location
//that you define in segments.h
s32 *AbsoluteI=&absi;
u32 *TimeStop=&gTimeStopState;
f32 *FindFloor=&find_floor_height;
s16 *Tan2S = &atan2s;
s16 *SetMarioAnim = &set_mario_animation;
struct Object *FindObj = &cur_obj_nearest_object_with_behavior;
struct MarioState *GMARIO=&gMarioStates;
f32 *SoundArgs=&gDefaultSoundArgs;
void *PlaySfx=&play_sound;
void *PlayTrack=&play_sequence;
float *Sinf=&sinf;
void *printfunc=&print_generic_string;
void *kerningtable=&gDialogCharWidths;
Gfx *MasterDL=&gDisplayListHead;
u32 *currVI=&sNumVblanks;
Gfx *TextEnd=&dl_ia_text_end;
Gfx *TextBegin=&dl_ia_text_begin;
Gfx *TextSettings=&dl_ia_text_tex_settings; //draw char triangle
Gfx *BGbox=&dl_draw_text_bg_box_TE;
void *TMatrix=&create_dl_translation_matrix;
void *SMatrix=&create_dl_scale_matrix;
void *OMatrix=&create_dl_ortho_matrix;
void *MenuScroll=&handle_menu_scrolling;
struct Controller *ControllersInput=&gControllers;


//I'll just do it this way until I bother to write something better
void SetupTextEngine(s16 x, s16 y, const u8 *str){
u16 *TEstart =(u8 *) 0x807f0000;
int *TEstart2 =(int *) 0x807f0000;
*(TEstart2+0x1B40)= sNumVblanks;
*(TEstart2+0x1B5A)=str;
*(TEstart2+0x1B7E)=str;
*(TEstart+0x36FA)=x;
*(TEstart+0x36FB)=y;
}

void RunTextEngine(void){
unsigned char (*p2f)(void);
p2f = &TextEngine;
p2f();
};