#include "sm64.h"
// == TE dialog ==
// (defines en_dialog_table etc.)

struct DialogEntryTE
{
 /*0x00*/ u16 x;
 /*0x02*/ u16 y;
 /*0x04*/ u8 *str;
};

#define DEFINE_DIALOG_TE(id, _1, _2, str) \
    u8 dialog_text_TE_ ## id[] = { str };

#include "dialogs_TE.h"

#undef DEFINE_DIALOG_TE
#define DEFINE_DIALOG_TE(id, x, y, _) \
    struct DialogEntryTE dialog_entry_TE_ ## id = { \
        x, y, dialog_text_TE_ ## id \
    };

#include "dialogs_TE.h"

#undef DEFINE_DIALOG_TE
#define DEFINE_DIALOG_TE(id, _1, _2, _5) &dialog_entry_TE_ ## id,

struct DialogEntryTE * TEdialog_table[] = {
#include "dialogs_TE.h"
    NULL
};