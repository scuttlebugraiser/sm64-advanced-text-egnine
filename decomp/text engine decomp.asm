//list of plaintextcmds
//40 set text spd
//41 play sfx
//48 jump string //can cause logic problems
//4c enable a to increase spd
//4d disable a to increase spd
//4e enable re input on re talk
//72 remove display
//74 pause
//76 play blip each char
//77 disable blip
//78 keep play music
//79 play music
//7A cleanup buffer
//82 enable cutscene
//86 display on correct dialog option //can cause logic problems
//87 display gen text
//88 screen shake
//89 end screen shake
//8A-8E camera cmds
//9b enable transition
//9c disable transition
//a0-a2 asm function cmds//can cause logic problems

//if non plaintext is called during plaintext
//plaintext auto ends.
//calling plaintext end cmd while not in plaintext
//acts as just padding
//this means end plain text before calling end dialog
//plaintext does not include line break!!



//40 to 4F is free
//70 to 9d is free
//use above numbers for special effects such as sounds and colors
//cutscenes/time stop will not enabled for this, that will have to be done seperately


//change 0xb9 value at 0x02011cc8 have CVG_X_ALPHA enabled so it works on pj64 video... why (not sure if will be needed in final)

//special text encoding cmds

//first is cmd byte, then parameter byte if necessary

//done
//40 set VIs per character, 2 byte - VIs per new char

//41 play sfx, 2 bytes - sfx arg

//42 set color of following characters, 4 bytes (white by default) - RGBA

//43 display short user string, 1 byte - user string number (each string is 10 bytes and goes from 6f00 to 6FF0, so inputs are 0 to F)

//44 set scissor 2 bytes x start, 2 bytes x end, 2 bytes y start,  2 bytes y end. 4 bytes env color (coordinates go from the bottom left as the origin and increase. Screen is 360x240)

//45 return from user string

//46 make rainbow text (characters following cmd will rgb glow, call a new env color to disable) 1 byte, alpha value

//47 change text origin, 4 bytes - 2 bytes x pos, 2 bytes y pos

//48 jump string, 4 bytes - ram address (takes segmented and ram) Does not make a new box

//49 translate text offset 4 bytes - 2 bytes x pos offset, 2 bytes y pos offset

//4a translate text hardset 4 bytes - 2 bytes x pos, 2 bytes y pos

//4b translate text moving, 4 bytes VI timer reserved, 1 byte keyframes, 6 bytes* (length, x offset, y offset)

//4C enable A press to increase text speed, 2 bytes - speed while holding A
//4D disable A press to increase text speed

//4E re enable user input on re-talk (by default, a user input only works once, then the cmd acts as if it is not there, ofc this does not stay after resetting the game)

//4F allow user input of short string, 1 byte - short user string ID. (This cmd will remove all other input and bring a keyboard up the player can type on. If you are not in time stop mario will move around while typing and it will suck)

//boxes will reset scaling and position and color. The only thing preserved is the character speed, and the blip flags.
//70 end of box, auto go to next box (put a pause before this so your string stays a while)

//71 end of box, goto next box on a press

//72 remove all text for certain number of VIs, 2 bytes - num of VIs with no text (Text will flash back to its previous spot after it is done, so if you had three sentences, it will auto go back to that point once the cmd is done), 0 has no effect

//73 only display following text if btn matches key, 2 bytes - btn input. Put the following string that you want to display and end it with a 0x75 [END] byte. Regular input will follow after the 0x75

//74 pause - 2 bytes VIs to wait

//75 cmd - ends btn check, does nothing otherwise

//76 play small blip sound per character (think like in megaman or any other rpg with text scrolling) call again to disable
//77 disable blip every character

//78 change music that plays after message to currently playing music (basically use this so if you switch tracks mid message, the track keeps playing, use after play music cmd)

//79 play music track, 1 byte - track number (original music will resume when string is done)

//7A cleanup buffer (Basically certain cmds like the keyboard, camera cmds, pause and remove on VI make use of a buffer so that the cmds don't repeat every frame. What happens is the cmds are replaced by padding, and their location are stored in the buffer which is at 807f6000 and can basically theoretically be 0xE00 bytes long. This cmd cleans the buffer, which re-enables those cmds. This means if the current box has a pause or keyboard and you use this cmd, it will play again. I advise using this before a jump string cmd or for weird mindtricks.)

//7B stop displaying string on A press (basically use this before an FF)

//7C stop displaying string after number of VI's (basically use this before an FF) - 2 bytes - num of VI's to wait until string is gone

//7d define textured mosaic background box, 2 bytes x start, 2 bytes x end, 2 bytes y start,  2 bytes y end. 4 texture offset (RGBA16 32x32 textures only for now. coordinates go from the bottom left as the origin and increase. Screen is 360x240), 1 byte num x tiles, 1 byte num y tiles

//7e define moving textured background box, 2 bytes x start, 2 bytes x end, 2 bytes y start,  2 bytes y end, 4 bytes texture, 4 bytes VI timer reserved for this cmd, 1 byte keyframes, *a bytes (length, xi change ,yi change,xf change ,yf change), (coordinates go from the bottom left as the origin and increase. Screen is 360x240)


//7F define background box, 2 bytes x start, 2 bytes x end, 2 bytes y start,  2 bytes y end. 4 bytes env color (coordinates go from the bottom left as the origin and increase. Screen is 360x240)

//80 define textured background box, 2 bytes x start, 2 bytes x end, 2 bytes y start,  2 bytes y end. 4 texture offset (RGBA16 32x32 textures only for now. coordinates go from the bottom left as the origin and increase. Screen is 360x240)

//81 define moving shaded background box, 2 bytes x start, 2 bytes x end, 2 bytes y start,  2 bytes y end, 4 bytes env color, 4 bytes VI timer reserved for this cmd, 1 byte keyframes, *a bytes (length, xi change ,xf change, yi change ,yf change), (coordinates go from the bottom left as the origin and increase. Screen is 360x240)

//82 enable cutscene, 1 byte - cutscene index (zero to disable all cutscenes, others see my documentation at https://sites.google.com/view/supermario64romhacks/tweaks-and-tutorials?authuser=0)


//83 non buffer padding (basically for asm modifying the string)

//84 scale text, 4 bytes x scale float, 4 bytes y scale float (To compensate for large Y scaling, just do several line breaks the text will go farther up, e.g. y scale of 2 will need an empty line above it to not overlap, x will auto align)

//85 enable dialog options, 1 byte, number of options (they will be displayed vertically), the following strings will be the options, to use an user inputted string as an option do this (ex. 43 FF), returns byte of option selected (zero indexed)

//86 display following only on returned dialog option, 1 byte return, truncate string with FF, if return value does not match it will skip to following string, if there is no following string it will just display nothing (zero indexed)

//87 display general text (basically anything after this will display no matter what dialog option you choose, if you put another 86 cmd after this, general text will end and it will pass all text until another 87 or a matching 86)

//88 enable screen shake
//89 disable screen shake

//8A change camera location, 6 bytes - 2 bytes x, 2 bytes y, 2 bytes z (set once on the would be character it would draw, aka if a string goes like "mari[83 cmd]o" then the camera would be set when the "i" would be drawn to the screen)

//8B change camera focus, 6 bytes - 2 bytes x, 2 bytes y, 2 bytes z (set once on the would be character it would draw, aka if a string goes like "mari[83 cmd]o" then the camera would be set when the "i" would be drawn to the screen)

//8C set camera speed, 8 bytes - 2 bytes VIs, 2 bytes x, 2 bytes y, 2 bytes z (sets the change in camera values. Use to set a speed after defining a location. This means if you set y spd to 10, it will continue to go up until you change the location  all speeds are units/every second VI)

//8D set camera focus speed, 8 bytes - 2 bytes VIs, 2 bytes x, 2 bytes y, 2 bytes z (sets the change in camera values. Use to set a speed after defining a location. This means if you set y spd to 10, it will continue to go up until you change the location  all speeds are units/every second VI)

//8E lock camera on object, 4 bytes - *object

//8f trigger warp, 1 byte - warp delay, 1 byte - warp ID

//90 animate object, 1 byte - ojbect index, 1 byte - anim index

//91 Path object to point, 1 byte - pad, 1 byte - obj index, 8 bytes (x,y,z,spd)

//92 drop object to ground, 1 byte - obj index.

//These cmds are basically like brackets. If you encounter one, it will skip over
//all other cmds until the end skip with matching key is found
//very useful for dialog options, use in a return option so that you
//can skip over stuff
//93 skip until key, 1 bytes - key
//94 end skip until key, 1 bytes - key

//You use these for inside nested dialog so you don't
//get a return on the nested options, also skips general text
//95 mask nested dialog, 1 bytes - key
//96 end nested mask, 1 bytes - key

//these are for jumping backwards. Basically use these for menus
//97 set return pointer, 1 byte - index (0 to 7)
//98 jump to return pointer, 1 byte - index (0 to 7), makes a new box

//only certain cmds work in plaintext mode, these will basically be the cmds
//that don't require me to segment the string. Plaintext will auto end if such a cmd
//is encountered
//will use the currently defined color as foreground, background will always be dark grey
//to have text effects with plain text (e.g. scale/translate dialog options...)
//then simply re-enable it each time.
//This has to occur because the engine is blind (on purpose to deal with
//dynamic or self modifying strings) to cmds past the current one
//99 enable backdrop plaintext, 0 bytes
//9a disable backdrop plaintext, 0 bytes

//9b setup end transition, 1 byte length, 1 byte target alpha, 1 byte direction, 1 byte speed
//9c setup begin transition, 1 byte length, 1 byte target alpha, 1 byte direction, 1 byte speed

//in struct
//1 byte length
//1 byte target alpha
//1 byte direction
//1 byte spd
//length of zero doubles as
//bool check

//9D is padding for buffer, (do not use without associated buffer function)


//!!!!
//9e+9f are characters.
//!!!!


//a0 call function once, V0 is stored to array starting at 0x6d90, current string pos is stored to A1, 9 bytes -  4 bytes A1 arg, 4 bytes function Vaddresss, 1 byte return index (0 to 3)

//a1 call function every frame while in box, V0 is stored to array starting at 0x6dA0, current string pos is stored to A1, 9 bytes - 4 bytes A1 arg, 4 bytes function Vaddresss, 1 byte return index (0 to 3)

//a2 display following only on matching function return, 1 byte - index of combined array (0 to 3 for A0 returns, 4 to 7 for A1 returns)

//a3 set camera pos relative to camera focus, 6 bytes - (x,y,z)

//a4 toggle timestop,1 byte -(1 to enable, 0 to disable)

//a5 set mario animation, 1 byte - animation ID

//a6 set mario action, 4 bytes - action

//a7 add object to array, 4 bytes - behavior ID, 1 byte - obj index

//a8 rotate object towards another object, 1 byte - source obj index, 1 byte - target obj index

//FE is line break
//FF is end of string, this is used for dialog options and such. Use 7B and 7C to end full string


//store temporary text starting from 0x807f7000
//6d00 VI of last character
//6d04 will store temp x origin
//6d06 will store temp y origin
//6d08 VI's per character
//6d0a will store state of keyboard input re input, and End of Box state
//6d0b will store wobbly text height
//6d0c will store temp string ptr
//6d10 will store temp x
//6d12 will store temp y
//6d14 will store temp string end pos
//6d18 will store ptr to sfx
//6d1a will store check if blip is playing

//6d1b IS FREE

//6d1c will store return ptr for user inputted string
//6d20 will store original music track
//6d22 will store switched to music
//6d24 will store VI timer for no display
//6d28 will store VI timer for string end
//6d2C will store VI timer for pause
//6d30 will store x scale
//6d34 will store y scale
//6d38 will store env color
//6d3C will store color for rainbow text
//6d40 will store string ptr for text removal
//6d44 will store flag for user input
//6d45 will store whether shift key is used
//6d46 will store which letter is being moved to
//6d47 will store which letter is selected
//6d48 will store VI timer for keyboard input
//6d4c will store ptr for keyboard removal
//6d50 will store location of user input
//6d54 will store total x pos offset
//6d58 will store returned dialog option
//6d59 will store currently displaying dialog option
//6d5A will store total number of options
//6d5B will store hovered dialog option
//6d5C will store ptr to end of all dialog options
//6d60 will store screen shake byte and set scissor byte
//6d61 will store plaintext byte
//6d62 will store counter for VI display
//6d64 will store ptr to end of text in DL
//6d68 will store original pointer to string for buffer offset
//6d6C will store speed set with 40 if replaced (initialized to -1 if not set)
//6d70 will store camera location sweep start VI
//6d74 will store camera focus sweep start VI
//6d78 will store camera loc sweep flag
//6d7C will store camera foc sweep flag
//6d80 will store camera loc sweep end VI
//6d84 will store camera foc sweep end VI
//6d88 will store origin sweep start VI
//6d8C will store origin sweep end VI
//6d90 will store struct of returned single call function values (i*4+6d90), up to 4 values, zero indexed
//6da0 will store struct of returned every frame call function values (i*4+6db0), up to 4 values, zero indexed.
//6db0 will store struct of return string pointers, up to 8 values (i*4+6db0) zero indexed

//6dd0 will store transition VI timer
//6dd4 will store transition variables
//6dd8 will store length of embedded VI buffer

//6ddc will store pointer to buffer position
//6de0 will store begin transition VI timer
//6de4 will store begin transition vars
//6de8 will store last env
//6df0 will be used as current position in the string
//6df4 will store x coord
//6df6 will store y coord
//6df8 will store the original string
//6dfc will store string end pos


//6000 will store cmd buffer (prob oversized)
//6600 will store VI timer buffer (for cmds that have embedded timers, this is necessary to clear it)
//6e00 will store return values from ext asm calls (6e80-i*4)
//6e80 will store object array, up to 20 object ptrs

//6f00 to 6ff0 - user inputted strings, 15 characters each (4E truncated)

SCALE_MATRIX equ 0X802d7280
TRANSLATION_MATRIX equ 0x802d7070
ORTHO_MATRIX equ 0x802d7384
PRINT_GENERIC equ 0x802d77dc
SINF equ 0x80325480
HANDLE_MENU_SCROLL equ 0x802d862c
sNumVblanks equ 0x8032d580
CharKernings equ 0x80331370
Text_Engine_Start equ 0x1203800
Text_Engine_Start_Virtual equ 0x80040090

SEG_TEST equ 0x80040000 //the magic


KEYBOARD_FILE equ "keyboard.bin"
.headersize Text_Engine_Start_Virtual-Text_Engine_Start


.orga Text_Engine_Start
ADVANCEDTEXTENGFUNC:

addiu sp, sp, -0x58
sw ra, 0x14 (SP)
sw s1, 0x38 (SP)
sw s2, 0x50 (SP)
sw s3, 0x4c (SP)
sw s4, 0x48 (SP)
sw s5, 0x44 (SP)
sw s6, 0x40 (SP)
sw s7, 0x3c (SP)

//check if there is a string, if not do nothing'
li s1, 1
lui s7, 0x807f
lw a2, 0x6df8 (S7) //ptr string
beq a2, r0, end
sw a2, 0x6d0c (S7)

jal GETLABELS
li at, 10//DisplayListHead
lw v0, 0x0 (V1) //current end of master DL
lw t0, 0x6d64 (S7) //end of last text
beq t0, v0, end
lh at, 0x6d62 (S7)
addiu t1, at, 1
beq at, r0, end
sh t1, 0x6d62 (S7)

//if there is a string, set it up
jal INSERTCHARSTARTDLFUNC //insert setup ptr
sw r0, 0x6d54 (S7)
lh a0, 0x6df4 (S7) //start x pos
lh a1, 0x6df6 (S7) //start y pos
sh a0, 0x6d10 (S7) //temp x pos
sh a1, 0x6d12 (S7) //temp y pos
sh a0, 0x6d04 (S7) //temp x origin
sh a1, 0x6d06 (S7) //temp y origin
lw at, 0x6dfc (S7) //string end pos
sw at, 0x6d14 (S7) //temp string end pos
sh r0, 0x6d18 (S7) //sfx ptr
lui at, 0x3f80
sw at, 0x6d30 (S7) //x scale
sw at, 0x6d34 (S7) //y scale
subiu at, r0, 0x1
sw at, 0x6d38 (S7) //env color
sw at, 0x6d6c (S7) //original VIs/char spd
sb r0, 0x6d60 (S7)
li at, 0xff
sb at, 0x7000 (S7)
jal GETLABELS
li at, 9//curr VI
lw s6, 0x0 (V1)


lw at, 0x6d24 (S7)
bne at, r0, nodisplayonVI

lw t1, 0x6d44 (S7)
bne t1, r0, makeuserinput

//next parse string for special cmds and characters
parse:
lw a2, 0x6d0c (S7)
lw t1, 0x6df0 (S7) //current pos in string
addu t2, t1, a2
lbu t2, 0x0 (T2) //part in string to parse
sltiu t3, t2, 0x40 //1 if its less than 40
subiu t4, t2, 0x50
sltiu t4, t4, 0x20 //1 if its between 50 and 6f
subiu t5, t2, 0xd0
sltiu t5, t5, 0x2E //1 if its greater than D0
subiu t7, t2, 0x9e
sltiu t7, t7, 0x2
or t3, t3, t7 //catches space and dash
or t6, t4, t3 //will be 1 if its a normal character
or t5, t6, t5
bne t5, r0, normalchar
nop
//from here I will parse special cmds

lw t9, 0x6df0 (S7) //current pos in string 
lw t8, 0x6d0c (S7)
addu t8, t9, t8
lbu t7, 0x0 (T8) //char in string to read

//should use a jump table for every other special


//plain text cmds
li t3, 0x73
beq t3, t7, BTNbranch
li t2, 0x99
beq t2, t7, enableplaintext
li at, 0x9a
beq at, t7, disableplaintextcmd
li t2, 0x9b
beq t2, t7, enabletransition
li at, 0x9c
beq at, t7, Begintransition
li t3, 0x40
beq t3, t7, setspeed
li t2, 0x48
beq t2, t7, jumptext
li t3, 0x74
beq t3, t7, pauseforVI
li t2, 0x76
beq t2, t7, enableblip
li t3, 0x41
beq t3, t7, setsfx
li t2, 0x77
beq t2, t7, disableblip
li at, 0x94
beq at, t7, advancedbytwo
li t3, 0x79
beq t3, t7, playmusictrack
li t2, 0x96
beq t2, t7, advancedbytwo
li t3, 0x93
beq t3, t7, skipuntilkey
li at, 0x95
beq at, t7, advancedbytwo
li t3, 0x82
beq t3, t7, enablecutscene
li t2, 0x7a
beq t2, t7, clearbuffer
li t3, 0x8A
beq t3, t7, setcameralocation
li t2, 0x8B
beq t2, t7, setcamerafocus
li t3, 0x8C
beq t3, t7, setcamerasweep
li t2, 0x8D
beq t2, t7, setcamerafocussweep
li at, 0x75
beq at, t7, advancedbyone
li t3, 0x83
beq t3, t7, advancedbyone
li t2, 0x8E
beq t2, t7, lockcameraonobj
li at, 0xa0
beq at, t7, callasmonce
li t3, 0xa1
beq t3, t7, callasmonce
li t2, 0xa2
beq t2, t7, displayonmatchingreturn
li at, 0x87
beq at, t7, advancedbyone
li t3, 0x4c
beq t3, t7, speeduptext
li t2, 0x4d
beq t2, t7, disablefasttext
li at, 0x72
beq at, t7, nodisplayonVI
li t3, 0x9D
beq t3, t7, advancedbyone
li t2, 0x78
beq t2, t7, changeoriginalmusic
li at, 0x86
beq at, t7, displayoncorrectdialogreturn
li t3, 0x88
beq t3, t7, shakescreen
li t2, 0x89
beq t2, t7, disablescreenshake
li t3, 0x4e
beq t3, t7, enablereinput
li t2, 0x8f
beq t2, t7, triggerwarp
li t3, 0x90
beq t3, t7, SetAnimation
li t2, 0x91
beq t2, t7, PathToPoint
li t3, 0x92
beq t3, t7, DropToFloor
li t2, 0xa3
beq t2, t7, SetCamRelative
li t3, 0xa4
beq t3, t7, ToggleTimestop
li t2, 0xa5
beq t2, t7, SetMarioAnim
li t3, 0xa6
beq t3, t7, SetMarioAction
li t2, 0xa7
beq t2, t7, AddBhvtoArray
li t3, 0xa8
beq t3, t7, rotatetowardsobj
li t2, 0xAB
beq t2, t7, clearVIbuffer
lbu at, 0x6d61 (S7)
bne at, r0, disableplaintext


//no longer plaintext
li t2, 0xfe
beq t2, t7, linebreak
li t3, 0xff
beq t3, t7, endofstring
li t2, 0x42
beq t2, t7, setenvcolor
li t3, 0x70
beq t3, t7, AutoBoxEnd
li t2, 0x71
beq t2, t7, ABoxEnd
li t3, 0x45
beq t3, t7, returnuserstring
li t2, 0x49
beq t2, t7, translatetext
li at, 0x4a
beq at, t7, temporigintext
li t2, 0x4b
beq t2, t7, translatetextmoving
li t3, 0x7B
beq t3, t7, removestringonA
li t2, 0x7C
beq t2, t7, removestringonVI
li t3, 0x84
beq t3, t7, scaletext
li at, 0x7d
beq at, t7, drawBGbox
li t2, 0x7f
beq t2, t7, drawBGbox
li at, 0x80
beq at, t7, drawBGbox
li t2, 0x81
beq t2, t7, drawmovingBGbox
li at, 0x7e
beq at, t7, drawmovingBGbox
li t3, 0x43
beq t3, t7, shortuserstring
li t2, 0x46
beq t2, t7, enablerainbowtext
li t3, 0x4f
beq t3, t7, userinputshortstring
li t2, 0x44
beq t2, t7, setscissor
li at, 0x85
beq at, t7, startdialogoptions
li t3, 0x47
beq t3, t7, translateorigin
li at, 0x97
beq at, t7, setreturnptr
li t2, 0x98
beq t2, t7, gotoreturnptr
li t3, 0x9a
beq t3, t7, advancedbyone
li t2, 0xa9
beq t2, t7, wobblytext
li t3, 0xaa
beq t3, t7, bgboxtransition

//99 cmd
enableplaintext:
li at, 1
b advancedbyone
sb at, 0x6d61 (S7)




//9a cmd

disableplaintextcmd:
lw at, 0x6d0c
addiu at, at, 1
sw at, 0x6d0c

disableplaintext:
lw at, 0x6d38 (S7)
sw at, 0x18 (SP)
li at, 0x101010FF
jal SETENVCOLORFUNC //set env color
sw at, 0x6d38 (S7)
//backdrop shadow
lh a0, 0x6d10 (S7)
addiu a0, a0, 2
lh a1, 0x6d12 (S7)
subiu a1, a1, 2
jal transitionprint //print string
ori a2, s7, 0x7000

lw at, 0x18 (SP)
jal SETENVCOLORFUNC //set env color
sw at, 0x6d38 (S7)

//pure white text
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000


lb at, 0x6d61 (S7)
beq at, r0, printnone
nop

jal PREPSTRING
sb r0, 0x6d61 (S7)
b parse
nop


//42 cmd
setenvcolor:
jal SETENVCOLORFUNC //set env color
sw t8, 0x18 (SP)
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
jal FLUSHSTRINGFUNC //flushstring
lw t8, 0x18 (SP)
jal U32LOADFUNC //unaligned word load, t8 in, a0 out
nop
jal XPOSRESETFUNC //update x pos of string
sw a0, 0x6d38 (S7)
jal POPMATRIXFUNC //pop matrix
nop
jal SCALEMATRIXFUNC //push scale matrix
nop
jal FIXSTRINGTRANSLATIONFUNC //fix translation after scale
nop

lw t9, 0x6d0c (S7) //temp string
lw t1, 0x6df0 (S7) //current pos in string

lw t2, 0x6d14 (S7)
subu t2, t2, t1
sw t2, 0x6d14 (S7)
sw r0, 0x6df0 (S7)
sw r0, 0x6d54 (S7)
addu t9, t1, t9 //last byte read
addiu t9, t9, 0x5
beq r0, r0, parse
sw t9, 0x6d0c (S7)


//46 cmd
enablerainbowtext:
//print previous string
jal SETENVCOLORFUNC //set env color
sw t8, 0x18 (SP)
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
jal FLUSHSTRINGFUNC //flushstring
nop



//set rainbow color for next string
lw t3, 0x6d3C (S7)
bne t3, r0, oscillate
lw t4, 0x6d38 (S7)
sw t4, 0x6d3C (S7)


oscillate:
or s4, s7, r0
or v0, r0, s6
sin:
sw v0, 0x10 (SP)
mtc1 V0, f12 //VI timer
lui at, 0x3D20
cvt.s.w f12, f12
mtc1 at, f20
jal GETLABELS
li at, 13//sinf
jalr v1 //takes f12 returns f0 sinf
mul.s f12, f12, f20 //VI timer*modulation
lui t9, 0x42ff //127.5
mtc1 t9, f2
mul.s f4, f0, f2 //sin times 128
add.s f4, f2, f4 //sin oscillating 0-255
cvt.w.s f4, f4
mfc1 t1, f4
andi t2, s4, 0xff
sb t1, 0x6d3C (S4)
lw v0, 0x10 (SP)
addiu v0, v0, 0x78
addiu s4, s4, 0x1
li t3, 0x2
bne t3, t2, sin
lw t8, 0x18 (SP)
lbu t7, 0x1 (T8) //alpha value
sb t7, 0x6d3F (S7)
lw t6, 0x6d3C (S7)
sw t6, 0x6d38 (S7)


//set stuff up for next string
endoscillate:
jal XPOSRESETFUNC //update x pos of string
nop
jal POPMATRIXFUNC //pop matrix
nop
jal SCALEMATRIXFUNC //push scale matrix
nop
jal FIXSTRINGTRANSLATIONFUNC //fix translation after scale
nop

lw t9, 0x6d0c (S7) //temp string
lw t1, 0x6df0 (S7) //current pos in string

lw t2, 0x6d14 (S7)
subu t2, t2, t1
sw t2, 0x6d14 (S7)
sw r0, 0x6df0 (S7)
sw r0, 0x6d54 (S7)
addu t9, t1, t9 //last byte read
addiu t9, t9, 0x2
beq r0, r0, parse
sw t9, 0x6d0c (S7)



//47 cmd
translateorigin:
jal U16LOADFUNC //loads unaligned half, t8 in, t4 out
lw at, 0x6d0c (S7)
or t3, r0, t4
jal U16LOADFUNC
addiu t8, t8, 2
sh t3, 0x6d04 (S7)
sh t4, 0x6d06 (S7)
sh t3, 0x6df4 (S7)
sh t4, 0x6df6 (S7)
advancedbyfive:
addiu at, at, 0x5
beq r0, r0, parse
sw at, 0x6d0c (s7)


//48 cmd
jumptext:
jal SETENVCOLORFUNC //set env color
sw t8, 0x18 (SP) //
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
jal PREPSTRING //flushstring
nop
jal U32LOADFUNC//unaligned word load
lw t8, 0x18 (SP)
jal 0x80277f50
sw r0, 0x6df0 (S7)
beq r0, r0, parse
sw v0, 0x6d0c (S7)


//49 cmd
translatetext:
jal SETENVCOLORFUNC //set env color
sw t8, 0x18 (SP) //
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
jal PREPSTRING //flushstring
nop
jal S16LOADFUNC //signed unaligned half load
lw t8, 0x18 (SP)
addiu t8, t8, 2
jal S16LOADFUNC
or t5, r0, a0 //SIGNED x pos

lh a2, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
addu t3, a2, t5
addu t6, a1, a0
sh t3, 0x6d10 (S7)
sh t6, 0x6d12 (S7)
sh t3, 0x6d04 (S7)
sh t6, 0x6d06 (S7)
beq r0, r0, advancedbyfive
lw at, 0x6d0C (S7)


//4A cmd
temporigintext:
jal SETENVCOLORFUNC //set env color
sw t8, 0x18 (SP) //
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
jal PREPSTRING //flushstring
nop
jal S16LOADFUNC //signed unaligned half load
lw t8, 0x18 (SP)
addiu t8, t8, 2
jal S16LOADFUNC
or t5, r0, a0 //SIGNED x pos

sh t5, 0x6d10 (S7)
sh a0, 0x6d12 (S7)
sh t5, 0x6d04 (S7)
sh a0, 0x6d06 (S7)
beq r0, r0, advancedbyfive
lw at, 0x6d0C (S7)

//4b cmd
translatetextmoving:
jal SETENVCOLORFUNC //set env color
sw t8, 0x18 (SP) //
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
jal PREPSTRING //flushstring
sh r0, 0x2e (SP)

jal U32LOADFUNC //a0 is VI timer
lw t8, 0x18 (SP)
sw a0, 0x1c (SP)
lbu t5, 0x5 (T8)//num keyframes (0 indexed)
beqz a0, storeVIstranslate
sh t5, 0x2a (SP)

addiu t8, t8, 0x3
nextkeyframetranslate:
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
addiu t8, t8, 0x2
or t6, r0, t4 //length of current keyframe
lh t4, 0x2E (SP)
sh t6, 0x2c (SP)
addu t6, t6, t4
sh t6, 0x2E (SP)
lw t9, 0x1c (SP) //VI timer

subu at, s6, t9 //num VIs passed since cmd start
subu at, t6, at //remaining VIs in keyframe
bgtz at, correctkeyframetranslate
sh t5, 0x28 (SP)
//add positions flatly to maintain continuous movement
addiu t8, t8, 2
jal S16LOADFUNC //
sh at, 0x20 (SP)//remaining VIs in keyframe
or t5, r0, a0 //x change

jal S16LOADFUNC //
addiu t8, t8, 0x2
or t6, r0, a0 //y change


lh t1, 0x6d10 (S7)
lh t2, 0x6d12 (S7)
addu t1, t1, t5
addu t2, t2, t6
lh t5, 0x28 (SP)
subiu t5, t5, 1
sh t1, 0x6d10 (S7)
sh t2, 0x6d12 (S7)
sh t1, 0x6d04 (S7)
sh t2, 0x6d06 (S7)
bgez t5, nextkeyframetranslate
nop


//end of cmd reached
advancedtranslatemoving:
lh t5, 0x2a (SP)
addiu t5, t5, 1
lw t7, 0x6d0c (S7)
li at, 10
multu t5, at
mflo t5
addu t7, t7, t5
addiu t7, t7, 6
b parse
sw t7, 0x6d0c (S7)


correctkeyframetranslate:
addiu t8, t8, 2
jal S16LOADFUNC //
sh at, 0x20 (SP)
or t5, r0, a0 //x change

jal S16LOADFUNC //
addiu t8, t8, 0x2
or t6, r0, a0 //y change


lh at, 0x20 (SP) //remaining VIs in keyframe
lh a2, 0x2c (SP) //length
subu a3, a2, at //VIs that have passed


lh a1, 0x6d10 (S7)//current value
jal LINEARINTERPOLATE
addu a0, a1, t5 //target value
sh v0, 0x6d10 (S7)
sh v0, 0x6d04 (S7)
lh a1, 0x6d12 (S7)//current value
jal LINEARINTERPOLATE
addu a0, a1, t6 //target value
sh v0, 0x6d12 (S7)
sh v0, 0x6d06 (S7)
b advancedtranslatemoving
nop


storeVIstranslate:
sb s6, 0x4 (T8)
srl at, s6, 8
sb at, 0x3 (T8)
srl at, at, 8
sb at, 0x2 (T8)
srl at, at, 8
sb at, 0x1 (T8)
jal ADDVIBUFFER
addiu a0, t8, 1
b advancedtranslatemoving
nop

//73 cmd
BTNbranch:
jal U16LOADFUNC
nop
jal GETLABELS
li at, 0 //gcontrollers
lh at, 0x12 (V1) //button pressed
and at, t4, at
bne at, r0, advancedbythree
lw t5, 0x6d0c (s7)
addiu t8, t8, 3
addiu t5, t5, 3
li t9, 0x75
btnloop:
lbu t1, 0x0 (t8)
addiu t5, t5, 1
bne t1, t9, btnloop
addiu t8, t8, 1
b parse
sw t5, 0x6d0c (s7)



//4C cmd
speeduptext:
jal GETLABELS
li at, 0 //gcontrollers
lh t9, 0x10 (V1) //button down
andi t7, t9, 0x8000
BEQ t7, r0, nospeedup
lw at, 0x6d0c (S7)
lh t1, 0x6d08 (S7)
jal U16LOADFUNC //loads unaligned half, t8 in, t4 out
sh t1, 0x6d6C (S7)
sh t4, 0x6d08 (S7)
nospeedup:
addiu at, at, 3
beq r0, r0, parse
sw at, 0x6d0c (S7)


//4D cmd
disablefasttext:
lh t1, 0x6d6c (S7)
beq r0, r0, advancedbyone
sh t1, 0x6d08 (S7)


//84 cmd
scaletext:
jal SETENVCOLORFUNC //set env color
sw t8, 0x18 (SP)
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
jal FLUSHSTRINGFUNC //flushstring
nop

//by popping the matrix, all previous scalings are gone
jal POPMATRIXFUNC //pop matrix
nop

//update positions

jal XPOSRESETFUNC //update x pos of string
nop

//load new scales


jal U32LOADFUNC//unaligned word load
lw t8, 0x18 (SP)
sw a0, 0x6d30 (S7)
jal U32LOADFUNC//unaligned word load
addiu t8, t8, 4
sw a0, 0x6d34 (S7)

jal SCALEMATRIXFUNC //push scale matrix
nop
jal FIXSTRINGTRANSLATIONFUNC //fix translation after scale
nop
lw t9, 0x6d0c (S7) //temp string
lw t1, 0x6df0 (S7) //current pos in string
lw t2, 0x6d14 (S7)
subu t2, t2, t1
sw t2, 0x6d14 (S7) //temp string end pos
sw r0, 0x6df0 (S7)
sw r0, 0x6d54 (S7)
addu t9, t1, t9 //last byte read
addiu t9, t9, 0x9
beq r0, r0, parse
sw t9, 0x6d0c (S7)



//81 cmd 7e cmd
drawmovingBGbox:
//draw previous string
sh t7, 0x24 (sp) //cmd number
jal SETENVCOLORFUNC //set env color
sw t8, 0x18 (SP)
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
jal FLUSHSTRINGFUNC //flushstring
sh r0, 0x2E (SP)

//pop mtx to remove any scaling on previous string
jal POPMATRIXFUNC //pop matrix
nop

lw t8, 0x18 (SP)
jal U32LOADFUNC //a0 is VI timer
addiu t8, t8, 0xC

//t1 -xi
//t2 - xf
//t3 - yi
//t0 - yf

jal U16LOADFUNC //load unaligned half, t8 in, t4 out
lw t8, 0x18 (SP)
or t1, r0, t4 //xi
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
addiu t8, t8, 0x2
or t2, r0, t4 //xf
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
addiu t8, t8, 0x2
or t3, r0, t4 //yi
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
addiu t8, t8, 0x2
or t0, r0, t4 //yf

sh t3, 0x22 (SP)
sw a0, 0x1c (SP)
//interpolate points to get new coordinates
lbu t5, 0xb (T8)//num keyframes (0 indexed)
beqz a0, storeVIsBG
sh t5, 0x2a (SP)
addiu t8, t8, 0x9
nextkeyframe:
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
addiu t8, t8, 0x2
or t6, r0, t4 //length of current keyframe
lh t4, 0x2E (SP)
sh t6, 0x2c (SP)
addu t6, t6, t4
sh t6, 0x2E (SP)
lw t9, 0x1c (SP) //VI timer

subu at, s6, t9 //num VIs passed since cmd start
subu at, t6, at //remaining VIs in keyframe
bgtz at, correctkeyframe
sh t5, 0x28 (SP)
//add positions flatly to maintain continuous movement
addiu t8, t8, 2
jal S16LOADFUNC //
sh at, 0x20 (SP)//remaining VIs in keyframe
or t5, r0, a0 //xi change
jal S16LOADFUNC //
addiu t8, t8, 0x2
or t6, r0, a0 //xf change
jal S16LOADFUNC //
addiu t8, t8, 0x2
or t7, r0, a0 //yi change
jal S16LOADFUNC //
addiu t8, t8, 0x2
//yf change - a0

lh t3, 0x22 (SP)
addu t1, t1, t5
addu t2, t2, t6
addu t3, t3, t7
sh t3, 0x22 (SP)
lh t5, 0x28 (SP)
subiu t5, t5, 1
bgez t5, nextkeyframe
addu t0, t0, a0


//end of cmd reached
//suspend box at end
b genericBGbox
or t4, r0, t0

correctkeyframe:
addiu t8, t8, 2
jal S16LOADFUNC //
sh at, 0x20 (SP)
or t5, r0, a0 //xi change
jal S16LOADFUNC //
addiu t8, t8, 0x2
or t6, r0, a0 //xf change
jal S16LOADFUNC //
addiu t8, t8, 0x2
or t7, r0, a0 //yi change
jal S16LOADFUNC //
addiu t8, t8, 0x2
or t8, r0, a0 //yf change


//t1 -xi
//t2 - xf
//t3 - yi
//t0 - yf - change to t4 at end

lh t3, 0x22 (SP)
lh at, 0x20 (SP) //remaining VIs in keyframe
lh a2, 0x2c (SP) //length
subu a3, a2, at //VIs that have passed
or a1, r0, t1 //current value
jal LINEARINTERPOLATE
addu a0, t1, t5 //target value
or t1, r0, v0


or a1, r0, t2 //current value
jal LINEARINTERPOLATE
addu a0, t2, t6 //target value
or t2, r0, v0

or a1, r0, t3 //current value
jal LINEARINTERPOLATE
addu a0, t3, t7 //target value
or t3, r0, v0

or a1, r0, t0 //current value
jal LINEARINTERPOLATE
addu a0, t0, t8 //target value


b genericBGbox
or t4, r0, v0



storeVIsBG:
sb s6, 0xa (T8)
srl at, s6, 8
sb at, 0x9 (T8)
srl at, at, 8
sb at, 0x8 (T8)
srl at, at, 8
sb at, 0x7 (T8)
jal ADDVIBUFFER
addiu a0, t8, 7
b genericBGbox
sh r0, 0x28 (SP)


//AA cmd
bgboxtransition:
sw t8, 0x34 (SP)
addiu t8, t8, 9
lbu t6, 0x0 (T8)
sh t7, 0x28 (SP)
li at, 0x44
beq at, t6, setscissor
sh t6, 0x24 (SP)
b transitionBGbox

Dotransitionlinterpolate:
lw t5, 0x6dd0 (S7) //transition timer
beq t5, r0, CheckBSTrans
lbu t6, 0x6dd4 (S7) //transition length
bne t6, r0, EndBTrans
CheckBSTrans:
nop
lw t5, 0x6de0 (S7) //transition timer
beq t5, r0, genericswitch
lbu t6, 0x6dE4 (S7) //transition length
beq t6, r0, genericswitch
nop
EndBTrans:
subu t7, s6, t5 //VIs since transition start
slt t8, t6, t7
bnezl t8, captrans
or t7, r0, t6
captrans:
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
lw t8, 0x34 (SP)
or t1, r0, t4 //xi
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
addiu t8, t8, 0x2
or t2, r0, t4 //xf
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
addiu t8, t8, 0x2
or t3, r0, t4 //yi
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
addiu t8, t8, 0x2

lh ra, 0x24 (SP)
li at, 0x44
bne ra, at, nonegatevalues
li v1, 240
subu t4, v1, t4
subu t3, v1, t3
nonegatevalues:
//a0 is target value
//a1 is current (original) value
//a2 is max value length
//a3 is VIs passed
//returns v0
or a0, r0, t1
lh a1, 0x20 (SP)
or a2, r0, t6
jal LINEARINTERPOLATE
or a3, r0, t7
or t1, r0, v0

or a0, r0, t2
lh a1, 0x2A (SP)
or a2, r0, t6
jal LINEARINTERPOLATE
or a3, r0, t7
or t2, r0, v0

or a0, r0, t3
lh a1, 0x2C (SP)
or a2, r0, t6
jal LINEARINTERPOLATE
or a3, r0, t7
or t3, r0, v0

or a0, r0, t4
lh a1, 0x2E (SP)
or a2, r0, t6
jal LINEARINTERPOLATE
or a3, r0, t7
lh t6, 0x24 (SP)
li at, 0x44
beq at, t6, FinishScissor
or t4, r0, v0
b genericBGbox
nop

genericswitch:
lh t6, 0x24 (SP)
li at, 0x44
beq at, t6, FinishScissor
nop
b genericBGbox
nop


//7F cmd 80 cmd 7d cmd
drawBGbox:
//make ortho matrix (don't need to call again after first one for text)
//pop mtx (to remove any previous scaling)
//env color
//scale push
//translate mult
//06 00 00 00 02 01 1c 48 (draws triangle
//pop matrix
//rescale+retranslate (so putting a box in the middle of a scaled string doesn't destroy it)

//draw previous string
sh r0, 0x28 (SP)
sh t7, 0x24 (sp) //cmd number
transitionBGbox:
sh r0, 0x26 (SP)
jal SETENVCOLORFUNC //set env color
sw t8, 0x18 (SP)
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
jal FLUSHSTRINGFUNC //flushstring
nop

//pop mtx to remove any scaling on previous string
jal POPMATRIXFUNC //pop matrix
nop

//push scale matrix, push translation matrix



//814D5E - rom location of BG box verts
//coords
//(0,-80)-(130,0)
//UVs should be edited to be 400x400
//814d68 - fc
//814d78 - fc
//814d76 - 4
//814d86 - 4
//EC370 - dialog char widths (80331370)

//W=xf-xi
//H=yf-yi
//Sx=W/130=(xf-xi)/130
//Sy=H/80=(yf-yi)/80

//Tx=xi/Sx=130*xi/(xf-xi)
//Ty=yf/Sy=80*(yf+80)/(yf-yi)



jal U16LOADFUNC //load unaligned half, t8 in, t4 out
lw t8, 0x18 (SP)
or t1, r0, t4 //xi
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
addiu t8, t8, 0x2
or t2, r0, t4 //xf
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
addiu t8, t8, 0x2
or t3, r0, t4 //yi
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
addiu t8, t8, 0x2


lh t7, 0x24 (sp)
li at, 0x7d ;draw mosaic
sh t1, 0x20 (SP)
sh t2, 0x2A (SP)
sh t3, 0x2C (SP)
beq at, t7, mosaicbox
sh t4, 0x2E (SP)
lh t7, 0x28 (SP)
li at, 0xaa
beq at, t7, Dotransitionlinterpolate



//t4 is yf
genericBGbox:
subu t2, t2, t1 //W
mtc1 t2, f2
subu t0, t4, t3 //H
cvt.s.w f2, f2 //Width
mtc1 t0, f4
lui at, 0x4302 //130
cvt.s.w f4, f4 //H
mtc1 at, f10 //130
lui at, 0x42a0 //80
mtc1 at, f20 //80
div.s f22, f2, f10 //Sx
div.s f14, f4, f20 //Sy
mtc1 t1, f12 //xi
mtc1 t4, f16 //yf
cvt.s.w f12, f12
div.s f12, f12, f22 //Tx
cvt.s.w f16, f16
div.s f16, f16, f14 //Ty
swc1 f16, 0x1C (SP) //Ty
mfc1 a1, f22 //Sx
swc1 f12, 0x20 (SP) //Tx
mfc1 a2, f14 //Sy
lui a3, 0x3f80 //1 = scale
jal GETLABELS//uses V1 and AT
li at, 3//scale
jalr v1 //scale matrix
ori a0, r0, 0x1 //push matrix
lw a1, 0x20 (SP)
lw a2, 0x1C (SP)
or a3, r0, r0
jal GETLABELS//uses V1 and AT
li at, 4//translation
jalr v1 //translate matrix
ori a0, r0, 0x2 //push matrix

lh t7, 0x24 (sp)
li at, 0x7d
beq at, t7, mosaictex
andi at, t7, 1
beq at, r0, texturedboxcontents
nop
b shadedboxcontents
nop


endbgcontents:
//setup stuff for the next string

jal INSERTCHARSTARTDLFUNC //init string
nop


jal XPOSRESETFUNC //update x pos of string
nop
jal POPMATRIXFUNC //pop matrix
nop
jal POPMATRIXFUNC //pop matrix
nop
jal SCALEMATRIXFUNC //push scale matrix
nop
jal FIXSTRINGTRANSLATIONFUNC //fix translation after scale
nop


lw t9, 0x6d0c (S7) //temp string
lw t1, 0x6df0 (S7) //current pos in string
lw t2, 0x6d14 (S7)
subu t2, t2, t1
sw t2, 0x6d14 (S7) //temp string end pos
sw r0, 0x6df0 (S7)
sw r0, 0x6d54 (S7)
addu t9, t1, t9 //last byte read
lh t7, 0x24 (sp)
li t1, 0x7e
beq t7, t1, moveincrease
li t1, 0x7d
beql t1, t7, medincrease
addiu t9, t9, 0xf
li at, 0x81
bne at, t7, shortincrease
moveincrease:
lh t5, 0x2a (SP)
addiu t5, t5, 1
li at, 10
multu t5, at
mflo t5
addiu t5, t5, 5
addu t9, t9, t5
shortincrease:
addiu t9, t9, 0xD
medincrease:
lh t7, 0x28 (SP)
li at, 0xaa
bne at, t7, noextraincrease
nop
addiu t9, t9, 9
noextraincrease:
beq r0, r0, parse
sw t9, 0x6d0c (S7)


shadedboxcontents:
//store env color, make new set combine and draw the rectangle

lw t8, 0x18 (SP)
jal U32LOADFUNC//unaligned word load
addiu t8, t8, 0x8
jal GETLABELS
li at, 10//DisplayListHead
lw v0, 0x0 (V1)
li t1, 0xFC62EAC5 
sw t1, 0x0 (V0)
li t2, 0xFFD7FFFF
sw t2, 0x4 (V0)
lui t6, 0xFB00
sw t6, 0x8 (V0)
sw a0, 0xC (V0)
lui t9, 0x0600
sw t9, 0x10 (V0)
jal GETLABELS
li at, 5
sw V1, 0x14 (V0)
lui t1, 0xbd00 //pop mtx
sw t1, 0x18 (V0)
sw r0, 0x1C (V0)
addiu v0, v0, 0x20
jal GETLABELS
li at, 10//DisplayListHead
addiu v0, v0, 0x20
b endbgcontents
sw v0, 0x0 (V1)




texturedboxcontents:
//load texture, then draw the rectangle

//pipe sync
//Render Tile
//tile sync
//Set Tile
//Set Tile Size
//load Timg
//Load Sync
//Load Block

lw t8, 0x18 (SP)
jal U32LOADFUNC//unaligned word load, a0 is texture addr
addiu t8, t8, 0x8
texturedmosaiccontents:
jal GETLABELS
li at, 10 //DisplayListHead
lw v0, 0x0 (V1)
; li t2, 0xBA000C02 ;point filter
; sw t2, 0x0 (V0)
; sw r0, 0x4 (V0)
; addiu v0, v0, 8

lui t1, 0xe800
sw t1, 0x0 (V0)
sw r0, 0x4 (V0)
addiu v0, v0, 8
//pipe sync done

li t2, 0xB900031D 
li t3, 0x005530B0 ;layer 4 preset-AA
sw t2, 0x0 (V0)
sw t3, 0x4 (V0)
addiu v0, v0, 8

lui t1, 0xe700
sw t1, 0x0 (V0)
sw r0, 0x4 (V0)
//pipe sync done

li t6, 0xF5100000
sw t6, 0x08 (V0)
lui t6, 0x0700
sw t6, 0x0c (V0)
//render tile done
lui t1, 0xe800
sw t1, 0x10 (V0)
sw r0, 0x14 (V0)
//tile sync done

li t6, 0xf5101000
li t7, 0x00014050 //rewrite to fit size (S shift -1, T shift -2 by default for 32x32)
sw t6, 0x18 (V0)
sw t7, 0x1c (V0)
//set tile done

li t1, 0xFD100000
sw t1, 0x20 (V0)
sw a0, 0x24 (V0)
//setTimg done

lui t6, 0xf200
li t7, 0x0007c07c //rewrite to fit size
sw t6, 0x28 (V0)
sw t7, 0x2c (V0)
//set tile size done

lui t9, 0xE600
sw t9, 0x30 (V0)
sw r0, 0x34 (V0)
//pipe sync done

lui t6, 0xf300
sw t6, 0x38 (V0)
li t6, 0x073ff100 //rewrite to fit size
sw t6, 0x3C (V0)
//load block done

li t6, 0xFC1FFE3F 
li t7, 0xFFFCF279 ;texel 0 only
sw t6, 0x40 (V0)
sw t7, 0x44 (V0)
//set combine done

lui t6, 0x0600
jal GETLABELS
li at, 5 //BG box triangle
sw t6, 0x48 (V0)
sw v1, 0x4C (V0)
//traingles drawn
li t1, 0xbd380002 //pop mtx
sw t1, 0x50 (V0)
li t2, 1 //pop one mtx
sw t2, 0x54 (V0)

; li t2, 0xBA000C02 
; li t3, 0x00002000 ;bilinear filter
; sw t2, 0x58 (V0)
; sw t3, 0x5c (V0)

addiu v0, v0, 0x58
jal GETLABELS
li at, 10 //DisplayListHead
sw v0, 0x0 (V1)



lh t7, 0x24 (sp)
li at, 0x7d ;draw mosaic
bne at, t7, endbgcontents
lh t0, 0x26 (sp)
addiu t0, t0, 1
b mosaicbox
sh t0, 0x26 (sp)

mosaictex:
b texturedmosaiccontents ;line 1380
lw a0, 0x30 (sp)

mosaicbox:
lw t8, 0x18 (SP)
jal U32LOADFUNC//unaligned
addiu t8, t8, 8
lh t1, 0x20 (SP)
lh t2, 0x2A (SP)
lh t3, 0x2C (SP)
lh t4, 0x2E (SP)
lh t0, 0x26 (sp) ;texture num
lw t8, 0x18 (SP)
lbu t6, 0xD (t8) ;rows
lbu t5, 0xE (t8) ;columns
multu t5, t6
mflo at
subu at, t0, at
bgez at, endbgcontents
divu t0, t6
mflo at ;row number
mfhi t9 ;col number
sll a2, t0, 11 ;*0x800
addu a0, a2, a0
subu a1, t4, t3
divu a1, t6
mflo a1
multu at, a1
mflo at
addu t3, t3, at ;yi + row offset
addu t4, t3, a1 ;yi + row offset + row height
subu a1, t2, t1
divu a1, t5
mflo a1
multu t9, a1
mflo t9
addu t1, t1, t9 ;xi + col offset
addu t2, t1, a1 ;xi + col offset + col width
b genericBGbox
sw a0, 0x30 (sp)



//8f cmd
triggerwarp:
jal U16LOADFUNC
lbu t1, 0x3 (T8) //warp ID
lui t0, 0x8033
ori t0 ,t0, 0xB252 //beginning of paiting struct
ori t7, r0, 0x1
sh t7, 0x0 (T0) //enable warp
sh t4, 0x2 (T0) //warp delay
sh t1, 0x4 (T0) //warp ID
//a0 = location start
//a1 = num of bytes to add to buffer
or a0, r0, t8
jal ADDTOBUFFERFUNC
li a1, 0x4
lw t9, 0x6d0c (S7)
addiu t9, t9, 4
b parse
sw t9, 0x6d0c (S7)

//90 cmd
SetAnimation:
lbu t0, 0x1 (t8)//obj index
lbu t1, 0x2 (t8)//anim id
sll t0, t0, 2
lui at, 0x807f
addu t0, t0, at
lw A0, 0x6E80 (t0) //*obj
lw t0, 0x120 (A0) //*animations
sll t1, t1, 2

//a0 = ptr object
//a1 = animation ptr
jal 0x8037c658 //geo_obj_init_animation
addu a1, t0, t1
b advancedbythree
nop

//92 cmd
DropToFloor:
lbu t0, 0x1 (t8)
sll t0, t0, 2
lui t9, 0x807f
addu t0, t9, t0
lw t0, 0x6E80 (t0) //*obj
sw t0, 0x18 (SP)
lwc1 f12, 0xa0 (t0) //curr x
lwc1 f14, 0xa4 (t0) //curr y
lw a2, 0xa8 (t0) //curr z

li.s f2, 150.0
//f12 - x, f14 - y, a2 - z
jal GETLABELS
li at, 21 //find_floor_height
jalr v1
add.s f14, f14, f2
lw t0, 0x18 (SP)
b advancedbytwo
swc1 f0, 0xa4 (t0) //curr y

//a4 cmd
ToggleTimestop:
jal GETLABELS
li at, 22 //timestopstate
lw t9, 0x0 (V1)
lbu t0, 0x1 (T8)
beqz t0, EndTimeStop
ori t9, t9, 0x2
b advancedbytwo
sw t9, 0x0 (V1)

EndTimeStop:
b advancedbytwo
sw r0, 0x0 (V1)


//91 cmd
PathToPoint:
//move object to position
//(pad,obj index,x,y,z,spd)
sw t8, 0x1C (SP)
lbu at, 0x1 (t8)
addiu t8, t8, 1
lbu t0, 0x1 (t8)
sll t0, t0, 2
lui t9, 0x807f
addu t0, t9, t0
lw t0, 0x6E80 (t0) //*obj
bne at, r0, Noiinitinterpolate
lwc1 f4, 0xa0 (t0) //curr x
lwc1 f6, 0xa4 (t0) //curr y
lwc1 f8, 0xa8 (t0) //curr z
;obj home values
;I do this to avoid keeping track of what frame I'm on and
;and interpolating. Its probably not faster but I just
;decided to do it this way.
swc1 f4, 0x164 (t0) //x
swc1 f6, 0x168 (t0) //y
swc1 f8, 0x16c (t0) //z
Noiinitinterpolate:
addiu at, at, 1
sb at, 0x0 (T8) ;increment timer
sb at, 0x18 (SP)
jal S16LOADFUNC
addiu t8, t8, 1
or t2, a0, r0 //x pos
jal S16LOADFUNC
addiu t8, t8, 2
or a1, a0, r0 //y pos
jal S16LOADFUNC
addiu t8, t8, 2
or a3, a0, r0//a3 is z pos
jal U16LOADFUNC
addiu t8, t8, 2
mtc1 t4, f2
cvt.s.w f2, f2 //spd
li.s f0, 4.0
div.s f2, f2, f0
;get start pos
lwc1 f4, 0x164 (t0) //curr x
lwc1 f6, 0x168 (t0) //curr y
lwc1 f8, 0x16c (t0) //curr z
mtc1 t2, f10
mtc1 a1, f12
mtc1 a0, f14
; get target pos
cvt.s.w f10, f10 //x target
cvt.s.w f12, f12 //y target
cvt.s.w f14, f14 //z target
;get R vector components
sub.s f10, f10, f4
sub.s f12, f12, f6
sub.s f14, f14, f8
abs.s f16, f10
abs.s f18, f12
add.s f16, f16, f18
abs.s f18, f14
add.s f16, f16, f18
;do displacement formula
sqrt.s f16, f16 //R
div.s f0, f2, f16 //percent moved each frame
div.s f2, f16, f2 //amount of frames
lb at, 0x18 (SP)
cvt.w.s f2, f2
mfc1 t9, f2
bgt at, t9, EndObjPathing

;percent per frame times components
mul.s f10, f10, f0 //spd x %
mul.s f12, f12, f0 //spd y %
mul.s f14, f14, f0 //spd z %
;get curr pos
lwc1 f4, 0xa0 (t0) //curr x
lwc1 f6, 0xa4 (t0) //curr y
lwc1 f8, 0xa8 (t0) //curr z
;add and store to object
add.s f4, f4, f10
add.s f6, f6, f12
add.s f8, f8, f14
swc1 f4, 0xa0 (t0) //x
swc1 f6, 0xa4 (t0) //y
swc1 f8, 0xa8 (t0) //z
;store speeds only so that mario moves if on top of obj
swc1 f10, 0xAC (t0) //x
swc1 f14, 0xB4 (t0) //z

//f14 is x
//f12 is z
//returns angle as v0
sw t0, 0x1c (SP)
mov.s f12, f14
jal GETLABELS
li at, 20//atan2s
jalr v1
mov.s f14, f10
lw t0, 0x1c (SP)
sw v0, 0xd4 (T0)
AdvanceByEleven:
lw at, 0x6d0c (S7)
addiu at, at, 11
b parse
sw at, 0x6d0c (S7)

EndObjPathing:
sw r0, 0xAC (t0) //x
sw r0, 0xB4 (t0) //z
//a0 = location start
//a1 = num of bytes to add to buffer
lw a0, 0x1C (SP)
jal ADDTOBUFFERFUNC
li A1, 11
b AdvanceByEleven
nop


//82 cmd
enablecutscene:
lbu t3, 0x1 (T8)
lui t2, 0x8033
lw t1, 0xddcc (T2) //its always been 8033b90c as far as I can tell
lw t1, 0x24 (T1)
sb t3, 0x30 (T1) //cutscene controller
beqz t3, endcutscene
sw t8, 0x18 (SP)

cutscenebuffer:
//a0 = location start
//a1 = num of bytes to add to buffer
or a0, r0, t8
jal ADDTOBUFFERFUNC
li A1, 2
advancedbytwo:
lw t9, 0x6d0c (S7)
addiu t9, t9, 2
beq r0, r0, parse
sw t9, 0x6d0c (S7)

endcutscene:
jal 0x80286f68// reset_camera
or a0, r0, t1
b cutscenebuffer
lw t8, 0x18 (SP)


//not sure exactly how these work but I just store the intended positions in both the location and offset. If they aren't the same value then the camera skews.

//SET 1 8033c698 = object to focus on location

//SET 2 8033c6a4 = camera location

//SET 3 8033c6b0 = offset from focus object

//SET 4 8033c6bc = offset from cam location


//8A cmd
setcameralocation:
lui at, 0x8034
addiu at, at, 0xc
sw at, 0x20 (SP)
genericcameraset:

jal S16LOADFUNC //signed unaligned half load, t8 in, a0 out
nop
addiu t8, t8, 0x2
jal S16LOADFUNC //signed unaligned half load, t8 in, a0 out
or t1, r0, a0 //x pos
addiu t8, t8, 0x2
jal S16LOADFUNC //signed unaligned half load, t8 in, a0 out
or t2, r0, a0 //y pos

mtc1 t1, f0
mtc1 t2, f2
cvt.s.w f0, f0 //x pos
mtc1 a0, f4
cvt.s.w f2, f2 //y pos
cvt.s.w f4, f4 //z pos
lw at, 0x20 (SP)
swc1 f0, 0xc698 (at)
swc1 f0, 0xc6b0 (at)
swc1 f2, 0xc69c (at)
swc1 f2, 0xc6b4 (at)
swc1 f4, 0xc6a0 (at)
swc1 f4, 0xc6b8 (at)
lw t9, 0x6d0c (S7)
addiu t9, t9, 7
beq r0, r0, parse
sw t9, 0x6d0c (S7)


//8B cmd
setcamerafocus:
lui at, 0x8034
beq r0, r0, genericcameraset
sw at, 0x20 (SP)



//8C cmd
setcamerasweep:
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
sw t8, 0x18 (SP)
//t4 is length in VIs
mtc1 t4, f16
lw t9, 0x6d70 (S7)
cvt.s.w f16, f16
bnez t9, enablelocsweep

lw t6, 0x6d78 (S7)
beqz t6, initcamlocsweep
subu t5, t8, t6
bltz t5, endcameralocsweep
lw t6, 0x6d80 (S7)
beq t6, t8, caplocsweep
nop
initcamlocsweep:
sw t8, 0x6d78 (S7) //flag for current sweep
sw s6, 0x6d70 (S7) //start VI

enablelocsweep:
lw t6, 0x6d78 (S7)
subu t5, t8, t6
bgtz t5, endcameralocsweep //if current addr is > start addr then exectute sweep
lw t9, 0x6d70 (S7)
bne t8, t6, caplocsweep
subu t6, s6, t9 //number of VIs that have passed since cmd started
subu t7, t4, t6 //VIs left in cmd
addiu t8, t8, 2
bgtz t7, nocapsweep
lw t8, 0x18 (SP)
sw r0, 0x6d70 (S7)
sw t8, 0x6d80 (S7)
caplocsweep:
or t6, r0, t4
nocapsweep:
lui at, 0x8034
addiu at, at, 0xC
sw at, 0x20 (SP)


genericcamerasweep:
//t6 is num VIs sweep has lasted
addiu t8, t8, 2
beqz t6, endcameralocsweep
mtc1 t6, f20
jal S16LOADFUNC
cvt.s.w f20, f20 //constant
//f16 is total length
or t1, r0, a0 //SIGNED x pos
div.s f20, f20, f16 //percent completion
mtc1 t1, f10
addiu t8, t8, 0x2
jal S16LOADFUNC
cvt.s.w f10, f10

or t2, r0, a0 //SIGNED y pos
mtc1 t2, f12
addiu t8, t8, 0x2
jal S16LOADFUNC
cvt.s.w f12, f12

mtc1 a0, f14 //SIGNED z pos
lw at, 0x20 (SP)
cvt.s.w f14, f14

lwc1 f0, 0xc698 (at) //cam x
lwc1 f2, 0xc69c (at) //cam y
sub.s f10, f10, f0
lwc1 f4, 0xc6a0 (at) //cam z
sub.s f12, f12, f2
sub.s f14, f14, f4

mul.s f10, f10, f20
mul.s f12, f12, f20
mul.s f14, f14, f20

add.s f0, f0, f10
add.s f2, f2, f12
add.s f4, f4, f14


storecameravars:
swc1 f0, 0xc698 (at)
swc1 f0, 0xc6b0 (at)
swc1 f2, 0xc69c (at)
swc1 f2, 0xc6b4 (at)
swc1 f4, 0xc6a0 (at)
swc1 f4, 0xc6b8 (at)

endcameralocsweep:
lw at, 0x6d0c (S7)
addiu at, at, 0x9
beq r0, r0, parse
sw at, 0x6d0c (S7)




//8D cmd
setcamerafocussweep:
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
sw t8, 0x18 (SP)
//t4 is length in VIs
mtc1 t4, f16
lw t9, 0x6d74 (S7)
cvt.s.w f16, f16
bne t9, r0, enablefocsweep

lw t6, 0x6d7C (S7)
beqz t6, initcamfocsweep
subu t5, t8, t6
bltz t5, endcameralocsweep
lw t6, 0x6d84 (S7)
beq t6, t8, capfocsweep
nop
initcamfocsweep:
sw t8, 0x6d7C (S7) //flag for current sweep
sw s6, 0x6d74 (S7) //start VI

enablefocsweep:
lw t6, 0x6d7C (S7)
subu t5, t8, t6
bgtz t5, endcameralocsweep //if current addr is > start addr then exectute sweep
lw t9, 0x6d74 (S7)
bne t8, t6, capfocsweep
subu t6, s6, t9 //number of VIs that have passed since cmd started
subu t7, t4, t6 //VIs left in cmd
addiu t8, t8, 2
bgtz t7, nocapFsweep
lw t8, 0x18 (SP)
sw r0, 0x6d74 (S7)
sw t8, 0x6d84 (S7)
capfocsweep:
or t6, r0, t4
nocapFsweep:
lui at, 0x8034
beq r0, r0, genericcamerasweep
sw at, 0x20 (SP)


//8E cmd
lockcameraonobj:
jal U32LOADFUNC//unaligned word load
lw t5, 0x6d0c (S7)
lw a0, 0x0 (A0)
subiu t5, t5, 4
sw t5, 0x6d0c (S7)
lwc1 f0, 0xa0 (A0)
lwc1 f2, 0xa4 (A0)
lwc1 f4, 0xa8 (A0)
beq r0, r0, storecameravars
lui at, 0x8034



//a3 cmd
SetCamRelative:
jal S16LOADFUNC //signed unaligned half load, t8 in, a0 out
nop
addiu t8, t8, 0x2
jal S16LOADFUNC //signed unaligned half load, t8 in, a0 out
or t1, r0, a0 //x pos
addiu t8, t8, 0x2
jal S16LOADFUNC //signed unaligned half load, t8 in, a0 out
or t2, r0, a0 //y pos
//a0 is z pos
mtc1 t1, f0
mtc1 t2, f2
mtc1 a0, f4
cvt.s.w f0, f0
cvt.s.w f2, f2
cvt.s.w f4, f4
lui at, 0x8034

lwc1 f10, 0xc698 (at)
lwc1 f12, 0xc69c (at)
lwc1 f14, 0xc6a0 (at)
add.s f0, f0, f10
add.s f2, f2, f12
add.s f4, f4, f14



addiu at, at, 0xC
swc1 f0, 0xc698 (at)
swc1 f0, 0xc6b0 (at)
swc1 f2, 0xc69c (at)
swc1 f2, 0xc6b4 (at)
swc1 f4, 0xc6a0 (at)
swc1 f4, 0xc6b8 (at)
lw at, 0x6d0c (S7)
addiu at, at, 7
b parse
sw at, 0x6d0c (S7)


//7A cmd
clearbuffer:
jal CLEANUPBUFFERFUNC //cleanup buffer
lui s7, 0x807f
beq r0, r0, advancedbyone
nop

//0xAB cmd
clearVIbuffer:
jal CLEANUPVIBUFFER
nop
beq r0, r0, advancedbyone
nop


//4F cmd
userinputshortstring:

//set combine for selected text is FC12EA25FFD7FFFF
//2A is the max value for the selector


lw at, 0x6d44 (S7) //flag for user input
BNE at, r0, makeuserinput
li t6, 0x1
sw r0, 0x6d50 (S7)
sh t6, 0x6d46 (S7)
sw t8, 0x6d4C (S7)
lbu t7, 0x1 (T8)
li t9, 0x10
multu t9, t7
mflo t9
addu t9, t9, s7
addiu t9, t9, 0x6f00
li t4, 0x9f9f9f9f
sw t4, 0x0 (T9)
li t3, 0x9f9f9fFF
sw t4, 0x4 (T9)
sw t4, 0x8 (T9)
sw t3, 0xC (T9)
beq r0, r0, printnone
sb T7, 0x6d44 (S7)



makeuserinput:

//draw background boxes for user input and keyboard
lui a1, 0x41F8
lui a2, 0x40E0
lui a3, 0x3f80 //1 = scale
jal GETLABELS//uses V1 and AT
li at, 3//scale
jalr v1 //scale matrix
ori a0, r0, 0x1 //push matrix

lui a1, 0x3f80
or a2, r0, r0
or a3, r0, r0
jal GETLABELS//uses V1 and AT
li at, 4//translation
jalr v1 //translate matrix
ori a0, r0, 0x2 //push matrix

jal GETLABELS
li at, 10 //DisplayListHead
lw v0, 0x0 (V1)
li t1, 0xFC62EAC5 
sw t1, 0x0 (V0)
li t2, 0xFFD7FFFF
sw t2, 0x4 (V0)
lui t6, 0xFB00
sw t6, 0x8 (V0)
li t9, 0x60606060
sw t9, 0xC (V0)
lui t9, 0x0600
sw t9, 0x10 (V0)
jal GETLABELS
li at, 6//BGboxTriangle
sw v1, 0x14 (V0)
addiu v0, v0, 0x18
jal GETLABELS
li at, 10 //DisplayListHead
sw v0, 0x0 (V1)


lui a1, 0x3f80
lui a2, 0x3EC0
lui a3, 0x3f80 //1 = scale
jal GETLABELS//uses V1 and AT
li at, 3//scale
jalr v1 //scale matrix
ori a0, r0, 0x2 //mult matrix


or a1, r0, r0
lui a2, 0x4250
or a3, r0, r0
jal GETLABELS//uses V1 and AT
li at, 4//translation
jalr v1 //translate matrix
ori a0, r0, 0x2 //push matrix

jal GETLABELS
li at, 10 //DisplayListHead
lw v0, 0xb06c (V1)
lui t9, 0x0600
sw t9, 0x0 (V0)
jal GETLABELS
li at, 6 //bgboxtriangle
sw v1, 0x4 (V0)
addiu v0, v0, 0x8
jal GETLABELS
li at, 10 //DisplayListHead
sw v0, 0x0 (V1)

jal POPMATRIXFUNC //pop matrix
nop

jal INSERTCHARSTARTDLFUNC //init string
nop

jal FLUSHSTRINGFUNC //flushstring
nop

lw at, 0x6d48 (S7)
beq at, s6, makekeyboarddisplay

li t0, 6
andi t2, s6, 0x6
bne t2, t0, beginkeyboard


//control selected text with control stick
li a0, 0x2 //horizontal
ori a1, s7, 0x6d47 //*menuvalue
li a2, 0x0 //min
jal GETLABELS
li at, 1//menu scrolls
jalr v1
li a3, 0x2b//max value

li a0, 0x1 //vert
ori a1, s7, 0x6d46 //*menuvalue
li a2, -1 //min
jal GETLABELS
li at, 1//menu scrolls
jalr v1
li a3, 0x1//max value

lb t0, 0x6d46 (S7)
lbu t1, 0x6d47 (S7)
li t2, 0xa
multu t2, t0
mflo t0
addu t0, t0, t1
sltiu t3, t0, 0x2c
bne t3, r0, capcontrolstickvert
nop
li t0, 0x2b
capcontrolstickvert:
sh t0, 0x6d46 (S7)


//control currently selected text with d pad
jal GETLABELS
li at, 0 //gcontrollers
lh T0, 0x10 (V1) //button down
lh at, 0x6d46 (S7) //currently selected text
li t9, 0x0200 // D Left
bne t9, t0, checkDright
subiu t1, at, 0x1
beq r0, r0, beginkeyboard
sh t1, 0x6d46 (S7)
checkDright:
li t9, 0x0100
bne t9, t0, checkDup
addiu t1, at, 0x1
beq r0, r0, beginkeyboard
sh t1, 0x6d46 (S7)

checkDup:
li t9, 0x800 //Dup
bne t9, t0, checkDdown
subiu t1, at, 0x0a
beq r0, r0, beginkeyboard
sh t1, 0x6d46 (S7)
checkDdown:
li t9, 0x0400 //Ddown
bne t9, t0, beginkeyboard
sltiu t0, at, 0x21
beq t0, r0, ddowntospace
addiu t1, at, 0x0a
beq r0, r0, beginkeyboard
sh t1, 0x6d46 (S7)

ddowntospace:
li t1, 0x2a
sh t1, 0x6d46 (S7)

beginkeyboard:
//check for overflow or underflow on selected
lh t9, 0x6d46 (S7) //currently selected text
subiu t0, t9, 0x1
bltz t0, keyboardunderflow
sltiu at, t9, 0x2C
beq at, r0, keyboardoverflow
nop
beq r0, r0, makekeyboarddisplay
keyboardoverflow:
li t8, 0x1
beq r0, r0, makekeyboarddisplay
sh t8, 0x6d46 (S7)
keyboardunderflow:
li t7, 0x2B
sh t7, 0x6d46 (S7)



//80404ffC = lowercase
//8040505C = uppercase

makekeyboarddisplay:

lui a1, 0x4000
lui a2, 0x3f9A
lui a3, 0x3f80 //1 = scale
jal GETLABELS//uses V1 and AT
li at, 3//scale
jalr v1 //scale matrix
ori a0, r0, 0x1 //push matrix

lh t9, 0x6d46 (S7) //currently selected text
lb t7, 0x6d45 (S7) //shift selected or not
li s5, KEYBOARDSTARTLOC//LOWERCASE
beq t7, r0, lowercase
nop
li s5, KEYBOARDSTARTLOC+filesize("keyboard.bin")/2 //uppercaseset
lowercase:
or t5, r0, r0
or t8, r0, s7
or s2, r0, r0//letter spacing counter
or s3, r0, r0 //line break counter
loopkeyboard1:
li t2, 0x2B
beql t2, t9, noaccountforspace
addiu t9, t9, 0x4
noaccountforspace:
lbu t1, 0x0 (S5)
li t2, 0x9e //space
beq t2, t1, nocountforselector1
li t3, 0xfe
bne t3, t1, countforselector1
nop
subiu s2, r0, 0x1
beq r0, r0, nocountforselector1
subiu s3, s3, 0x10

countforselector1:
addiu t5, t5, 0x1

nocountforselector1:
li t3, 0xff
sb t3, 0x7001 (T8)
beq t3, t1, breakkeyboardloop1
addiu t8, t8, 0x1
beq t5, t9, breakkeyboardloop1
nop
addiu s2, s2, 0x1
addiu s5, s5, 0x1
beq r0, r0, loopkeyboard1
sb t1, 0x7000 (t8)
breakkeyboardloop1:



li a0, 0x12
li a1, 0x46
lui a2, 0x807f
jal transitionprint //print string
ori a2, A2, 0x7000

jal FLUSHSTRINGFUNC //flushstring
nop

//change setcombine so that black background goes around current letter
//2A = space
//2B = end


li t1, 0xFC12EA25
li t0, 0xFFD7FFFF
jal GETLABELS
li at, 10 //DisplayListHead
lw v0, 0x0 (V1)
sw t1, 0x0 (V0)
sw t0, 0x4 (V0)
addiu v0, v0, 0x8
sw v0, 0x0 (V1)

//print single letter with black background

li t6, 0
lui t8, 0x807f
lh t9, 0x6d46 (S7)
li at, 0x2b
beq at, t9, endletter
li t0, 0x2a
bne t0, t9, singleletter
nop
addiu s2, s2, 0x1
space:
li t3, 0xff
lbu t1, 0x0 (S5)
addiu S5, S5, 0x1
addiu t6, t6, 0x1
sb t3, 0x7001 (T8)
li t0, 0x5
sb t1, 0x7000 (T8)
bne t0, t6, space
addiu t8, t8, 0x1
beq r0, r0, printhighlightedletter
nop

endletter:
li t3, 0xff
lbu t1, 0x0 (S5)
addiu S5, S5, 0x1
addiu t6, t6, 0x1
sb t3, 0x7001 (T8)
li t0, 0x3
sb t1, 0x7000 (T8)
bne t0, t6, endletter
addiu t8, t8, 0x1
beq r0, r0, printhighlightedletter



singleletter:
li t3, 0xff
lbu t1, 0x0 (S5)
addiu S5, S5, 0x1
sb t3, 0x7001 (S7)
sb t1, 0x7000 (S7)



printhighlightedletter:
mtc1 s2, f2
cvt.s.w f2, f2
lh t9, 0x6d46 (S7)
sltiu t9, t9, 0xb
bne t9, r0, smallerkerning1
lui at, 0x40C8
lb t0, 0x6d45 (S7)
beq t0, r0, smallerkerning1
lui at, 0x40B0
lui at, 0x40C8
smallerkerning1:
mtc1 at, f4
mul.s f2, f2, f4
cvt.w.s f2, f2
mfc1 t0, f2


li a0, 0x12
addu a0, a0, t0
li a1, 0x46
addu a1, a1, s3
lui a2, 0x807f
jal transitionprint //print string
ori a2, A2, 0x7000

jal FLUSHSTRINGFUNC //flushstring
nop

jal INSERTCHARSTARTDLFUNC //init string
addiu s2, s2, 0x1


lui a1, 0x4000
lui a2, 0x3f9A
lui a3, 0x3f80 //1 = scale
jal GETLABELS//uses V1 and AT
li at, 3//scale
jalr v1 //scale matrix
ori a0, r0, 0x1 //push matrix



lui t8, 0x807f
loopkeyboard2:
lbu t1, 0x0 (S5)
li t3, 0xfe
beq t3, t1, breakkeyboardloop2
li t2, 0xff
sb t1, 0x7000 (t8)
beq t2, t1, breakkeyboardloop3
sb t2, 0x7001 (T8)
addiu t8, t8, 0x1
bne t5, t9, loopkeyboard2
addiu s5, s5, 0x1
breakkeyboardloop2:



mtc1 s2, f2
cvt.s.w f2, f2
lh t9, 0x6d46 (S7)
sltiu t9, t9, 0xb
bne t9, r0, smallerkerning2
lui at, 0x40C8
lb t0, 0x6d45 (S7)
beq t0, r0, smallerkerning2
lui at, 0x40B0
lui at, 0x40C8
smallerkerning2:
mtc1 at, f4
mul.s f2, f2, f4
cvt.w.s f2, f2
mfc1 t0, f2



li a0, 0x12
addu a0, a0, t0
li a1, 0x46
addu a1, a1, s3
lui a2, 0x807f
jal transitionprint //print string
ori a2, A2, 0x7000



lui t8, 0x807f
loopkeyboard3:
lbu t1, 0x0 (S5)
sb t1, 0x7000 (t8)
li t3, 0xff
sb t3, 0x7001 (T8)
beq t3, t1, breakkeyboardloop3
addiu t8, t8, 0x1
bne t5, t9, loopkeyboard3
addiu s5, s5, 0x1
breakkeyboardloop3:


lh t9, 0x6d46 (S7)
li t7, 0x29
bne t7, t9, checkspacebarspace
li a0, 0x12
beq r0, r0, printfinalkeyboard
addiu a0, a0, 0x18
checkspacebarspace:
li t8, 0x2a
bne t8, t9, printfinalkeyboard
li a0, 0x12
addiu a0, a0, 0x46

printfinalkeyboard:
li a1, 0x46
addu a1, a1, s3
lui a2, 0x807f
jal transitionprint //print string
ori a2, A2, 0x7000



//keyboard has been properly displayed, now I will print the string I am writing above

lb T7, 0x6d44 (S7)
li t9, 0x10
multu t7, t9
mflo t9
addu t9, t9, s7
addiu t9, t9, 0x6f00 //address of string


//display user string

li a0, 0x18
li a1, 0x76
jal transitionprint //print string
or a2, r0, T9

jal POPMATRIXFUNC //pop matrix
nop

//add to string on A press
lw at, 0x6d48 (S7)
beq at, s6, printnone
nop
sw s6, 0x6d48 (S7)
lbu t7, 0x6d44 (S7)
li t6, 0x10
multu t6, t7
mflo t6
addu t6, t6, s7
addiu t6, t6, 0x6f00
lw t8, 0x6d50 (S7) //spot in user string
addu t6, t8, t6
lh t9, 0x6d46 (S7)
jal GETLABELS
li at, 0 //gcontrollers
lh T0, 0x12 (V1) //button pressed
andi t2, t0, 0x4000
bne t2, r0, backspace
andi t1, t0, 0x8000
beq t1, r0, printnone
li at, 0x2b
beq at, t9, endkeyboard
li t1, 0x29
beq t1, t9, shift
li at, 0x2a
beq at, t9, addspace
li t1, 0x28
beq t1, t9, backspace
li at, 0xF
beq at, t8, printnone




//every other letter
lb t7, 0x6d45 (S7) //shift selected or not
ori t0, r0, 0x1
li s5, KEYBOARDSTARTLOC//LOWERCASE
beq t7, r0, lowercaseinput
nop
li s5, KEYBOARDSTARTLOC+filesize("keyboard.bin")/2 //uppercaseset


lowercaseinput:
lbu t4, 0x0 (S5)
addiu s5, s5, 0x1
li t3, 0x9e
beq t3, t4, lowercaseinput
li t2, 0xfe
beq t2, t4, lowercaseinput
nop
beq t0, t9, addletterinput
nop
beq r0, r0, lowercaseinput
addiu t0, t0, 0x1


addletterinput:
sb t4, 0x0 (T6)
addiu t8, t8, 0x1
beq r0, r0, printnone
sw t8, 0x6d50 (S7)


backspace:
blez t8, printnone
subiu t6, t6, 0x1
li t2, 0x9f
sb t2, 0x0 (T6)
subiu t8, t8, 0x1
beq r0, r0, printnone
sw t8, 0x6d50 (S7)



shift:
lb t2, 0x6d45 (S7)
xori t2, t2, 0x1
beq r0, r0, printnone
sb t2, 0x6d45 (S7)


addspace:
li at, 0xF
beq at, t8, printnone
li t2, 0x9e
sb t2, 0x0 (T6)
addiu t8, t8, 0x1
beq r0, r0, printnone
sw t8, 0x6d50 (S7)



//end keyboard, save input

endkeyboard:
li t2, 0x45
sb t2, 0x0 (T6)
lw t8, 0x6d4C (S7)
sw r0, 0x6d50 (S7)
sw r0, 0x6d4C (S7)
sw r0, 0x6d44 (S7)
sw r0, 0x6d48 (S7)
lw at, 0x6d0c (S7) //temp string
addiu at, at, 0x2
sw at, 0x6d0c (S7) //temp string
sw s6, 0x6d00 (S7)
lbu at, 0x6d0a (S7)
andi at, at, 1
beq at, r0, paddoutkeyboard

//a0 = location start
//a1 = num of bytes to add to buffer
or a0, r0, t8
jal ADDTOBUFFERFUNC
li A1, 2

beq r0, r0, parse

paddoutkeyboard:
li t9, 0x9d
sb t9, 0x0 (T8)
sb t9, 0x1 (T8)
beq r0, r0, parse
sb r0, 0x6d0a (S7)



//72 cmd
nodisplayonVI:
lw at, 0x6d24 (S7) //VI timer for no display
BNE at, r0, checkVItimefordisplay
nop
sw s6, 0x6d24 (S7)
beq r0, r0, printnone
sw t8, 0x6d40 (S7)
checkVItimefordisplay:
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
lw t8, 0x6d40 (S7)
addu at, t4, at //VI no display should end at
subu at, s6, at
sw s6, 0x6d00 (S7)
bltz at, printnone
lw t1, 0x6d0c (S7) //temp string
addiu t4, t1, 0x3
sw r0, 0x6d24 (S7)

//a0 = location start
//a1 = num of bytes to add to buffer
or a0, r0, t8
jal ADDTOBUFFERFUNC
li A1, 3

beq r0, r0, parse
sw t4, 0x6d0c (S7) //temp string


//4E cmd
enablereinput:
lb t9, 0x6d0a (S7)
ori t9, t9, 1
beq r0, r0, advancedbyone
sb t9, 0x6d0a (S7)



//74 cmd
pauseforVI:
lh at, 0x6d08 (S7) //VI spd
beq at, r0, skippause
lw t9, 0x6df0 (S7) //current pos in string
addu at, s7, t9
li t0, 0xFF
sb t0, 0x7001 (AT)
lw t1, 0x6d2C (S7)
sw s6, 0x6d00 (S7)
BEQL t1, r0, nonewchar
sw s6, 0x6d2C (S7)
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
nop
addu t1, t4, t1 //VI that the pause should end at
subu t1, s6, t1
bltz t1, nonewchar
skippause:
lw t2, 0x6d0c (S7) //temp string
addiu t4, t2, 0x3
sw r0, 0x6d2c (S7)
//a0 = location start
//a1 = num of bytes to add to buffer
or a0, r0, t8
jal ADDTOBUFFERFUNC
li A1, 3

beq r0, r0, parse
sw t4, 0x6d0c (S7) //temp string



//76 cmd
enableblip:
lb t9, 0x6d1A (S7)
ori t9, t9, 0x1
beq r0, r0, advancedbyone
sb t9, 0x6d1A (S7)



//77 cmd
disableblip:
beq r0, r0, advancedbyone
sb r0, 0x6d1A (S7)



//78 cmd
changeoriginalmusic:
lh t2, 0x6d22 (S7) //switched to music
beq r0, r0, advancedbyone
sh t2, 0x6d20 (S7) //original music


//79 cmd
playmusictrack:
lui at, 0x8034
lh t1, 0xb944 (AT) //currently playing music
lh t2, 0x6d20 (S7) //original music
bltzl t2, skipsetogmusic
sh t1, 0x6d20 (S7)
skipsetogmusic:
lbu a1, 0x1 (T8) //music we want to play
lh t3, 0x6d22 (S7) //switched to music
beq t3, a1, skipenablemusic
or a0, r0, r0
sh a1, 0xb944 (AT) //currently playing
jal GETLABELS
li at, 14//play sequence
jalr v1 //set music
sh a1, 0x6d22 (S7) //switched to music
skipenablemusic:
lw at, 0x6d0c (S7) //temp string
addiu at, at, 0x2
beq r0, r0, parse
sw at, 0x6d0c (S7) //temp string



//7C cmd
removestringonVI:
lw at, 0x6d28 (S7)
BEQL at, r0, nocheckVIforstringend
sw s6, 0x6d28 (S7)
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
nop
addu at, t4, at //VI string should end at
subu at, s6, at
blez at, nocheckVIforstringend
nop


lbu t2, 0x6dd4 (S7) //length
beqz t2, notransendauto
lw t1, 0x6dd0 (S7)
beqzl t1, nonewchar
sw s6, 0x6dd0 (S7)
subu t3, s6, t1 //VIs that have passed
subu t3, t2, t3 //VIs left
bgtz t3, nonewchar
nop
notransendauto:

jal COMPLETESTRINGRESETFUNC //resetstring
sw r0, 0x6d2c (S7)
beq r0, r0, end
sw r0, 0x6d24 (S7)
nocheckVIforstringend:
li t7, 0xff
lw t9, 0x6df0 (S7) //current pos in string
addu t9, t9, s7
beq r0, r0, nonewchar
sb t7, 0x7000 (T9)



//7B cmd
removestringonA:
lb at, 0x6d0a (S7)
andi t0, at, 0x80
sw s6, 0x6d00 (S7)
ori at, at, 0x80
beq t0, r0, nonewchar
sb at, 0x6d0a (S7)
jal GETLABELS
li at, 0 //gcontrollers
lh t9, 0x12 (V1) //button pressed
andi t0, t9, 0x8000 //a press
//flash down arrow
andi t1, s6, 0x20
beq r0, t1, noarrowforend7c
li t7, 0xff
li t7, 0x51
noarrowforend7c:
li t6, 0xff
lw t9, 0x6df0 (S7) //current pos in string
addu t9, t9, s7
lw t1, 0x6dd0 (S7)
bnez t1, transitionend
sb t6, 0x7001 (T9)
beq t0, r0, nonewchar
sb t7, 0x7000 (T9)

transitionend:
lbu t2, 0x6dd4 (S7) //length
beqz t2, notransend
nop
beqzl t1, nonewchar
sw s6, 0x6dd0 (S7)
subu t3, s6, t1 //VIs that have passed
subu t3, t2, t3 //VIs left
bgtz t3, nonewchar
nop
notransend:
jal COMPLETESTRINGRESETFUNC //resetstring
nop
beq r0, r0, end
nop


//85 cmd
startdialogoptions:

//print string before dialog options

jal SETENVCOLORFUNC //set env color
sw t8, 0x18 (SP)
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
jal FLUSHSTRINGFUNC //flushstring
nop
lh a0, 0x6d04 (S7)
jal POPMATRIXFUNC //pop matrix
sh a0, 0x6d10 (S7)
jal SCALEMATRIXFUNC //push scale matrix
nop
jal FIXSTRINGTRANSLATIONFUNC //fix translation after scale
nop

//6d58 will store returned dialog option
//6d59 will store currently displaying dialog option
//6d5A will store total number of options
//6d5B will store hovered dialog option


//line break for first option to be under question dialog
lh a1, 0x6d12 (S7) //temp y pos
subiu a1, a1, 0xd
sh a1, 0x6d12 (S7)

//store total num of options
lw t8, 0x18 (SP)
lb t0, 0x1 (T8) //number of options
sb t0, 0x1c (SP)
jal GETLABELS
li at, 23 ;absi
jalr v1
or a0, r0, t0
sb v0, 0x6d5a (S7)

//check for dialog selection
lw t0, 0x6d5C (S7)
beq t0, r0, setmenuvaluesdialog
nop
jal GETLABELS
li at, 0 //gcontrollers
lh t9, 0x12 (V1) //button pressed
andi t9, t9, 0x8000
beq t9, r0, setmenuvaluesdialog
lbu t0, 0x6d5b (S7)
sb t0, 0x6d58 (S7)
sh r0, 0x6d5A (S7)
sb r0, 0x6d59 (S7)
lw t9, 0x6d5C (S7)
sw r0, 0x6d5c (S7)
sw r0, 0x6df0 (S7)
sw r0, 0x6d54 (S7)
sw r0, 0x6dfc (S7)
sw s6, 0x6d00 (S7)
beq r0, r0, printnone
sw t9, 0x6df8 (S7) //*string



setmenuvaluesdialog:
//update end of string ptr
sb r0, 0x6d59 (S7)
lw t1, 0x6df0 (S7)
lw t0, 0x6d14 (S7)
subu t0, t0, t1
sw t0, 0x6d14 (S7)




//get control stick input for menu selection
lb at, 0x1c (SP)
bgtz at, vertdiag
li a0, 0x1 //vert
li a0, 0x2 //horiz
vertdiag:
ori a1, s7, 0x6d5B //*menuvalue
li a2, 0 //min
jal GETLABELS
li at, 1//menu scrolls
jalr v1
lbu a3, 0x6d5a (S7) //max value

//draw arrow if option 0 is selected

lb t0, 0x6d5b (S7)
bne t0, r0, nosetarrowdialog
li t1, 0x53
li at, 0xff
sb at, 0x7001 (S7)
jal SETENVCOLORFUNC //set env color
sb t1, 0x7000 (S7)
lh a0, 0x6d04 (S7)
lh a1, 0x6d12 (S7) //temp y pos
jal transitionprint //print string
ori a2, s7, 0x7000
jal FLUSHSTRINGFUNC //flushstring
nop
jal POPMATRIXFUNC //pop matrix
nop
jal SCALEMATRIXFUNC //push scale matrix
nop
jal FIXSTRINGTRANSLATIONFUNC //fix translation after scale
nop

nosetarrowdialog:

//set x pos to be init pos for line break
lh a0, 0x6d04 (S7)
sh a0, 0x6d10 (S7)
li t0, 0x8
jal XPOSRESETFUNC //update x pos of string
sw t0, 0x6d54 (S7)
sw r0, 0x6dF0 (S7)
lw at, 0x18 (SP) //*temp string
addiu at, at, 0x2
beq r0, r0, parse
sw at, 0x6d0c (S7) //*temp string 



//86 cmd
displayoncorrectdialogreturn:
lbu t1, 0x1 (T8)
lbu t0, 0x6d58 (S7)
lw t2, 0x6d0c (S7)
addiu t2, t2, 0x2
beq t1, t0, parse
sw t2, 0x6d0c (S7)

notcorrectoption:
addiu t8, t8, 0x1
lbu t0, 0x0 (T8)
li at, 0x95
beql at, t0, brackets86
lbu t4, 0x1 (T8)
li t2, 0x87
beql t0, t2, generaltextdisplay
addiu t7, t8, 1
li t3, 0x86
bne t0, t3, notcorrectoption
or t7, t8, r0
generaltextdisplay:
jal SETENVCOLORFUNC //set env color
sw t7, 0x18 (SP)
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
jal FLUSHSTRINGFUNC //flushstring
nop
jal POPMATRIXFUNC //pop matrix
nop
jal SCALEMATRIXFUNC //push scale matrix
nop
jal FIXSTRINGTRANSLATIONFUNC //fix translation after scale
nop
lw at, 0x6d14 (S7)
lw t1, 0x6df0 (S7) //current pos in string
subu at, at, t1
jal XPOSRESETFUNC //update x pos of string
sw at, 0x6d14 (S7)
lw at, 0x18 (SP)
sw r0, 0x6df0 (S7)
beq r0, r0, parse
sw at, 0x6d0c (S7)

//95 cmd
brackets86:
//t4 is key
li at, 0x96
addiu t8, t8, 1
b86loop:
lbu t0, 0x0 (T8)
bne t0, at, b86loop
addiu t8, t8, 1
lbu t5, 0x0 (T8)
bnel t5, t4, b86loop
addiu t8, t8, 1
b notcorrectoption
nop


//a1 cmd
//a0 cmd
callasmonce:
lb t9, 0x9 (T8)
bltz t9, callasmonceExt
sb t7, 0x20 (SP)
jal U32LOADFUNC //get a0 arg
sw t8, 0x18 (SP)
or a2, r0, a0
or a1, r0, t8 //ptr string pos
jal U32LOADFUNC //get asm add
addiu t8, t8, 4
or t9, r0, a0 //function address
jalr t9
or a0, r0, a2 //user defined arg
lw t8, 0x18 (SP)
lbu t7, 0x9 (T8)
sll t7, t7, 2
addu at, s7, t7
or v1, v0, v1
sw v0, 0x6d90 (AT)
lbu t7, 0x20 (SP)
li at, 0xa1
beq at, t7, advancedbyten
or a0, r0, t8
jal ADDTOBUFFERFUNC
li a1, 10
advancedbyten:
lw t0, 0x6d0c (S7)
addiu t0, t0, 10
b parse
sw t0, 0x6d0c (S7)

callasmonceExt:
jal U32LOADFUNC //get a0 arg
sw t8, 0x18 (SP)
sw a0, 0x1c (sp)
jal U32LOADFUNC //get asm addr
addiu t8, t8, 4
or t3, r0, a0
jal U32LOADFUNC //get a1 arg
addiu t8, t8, 5
or a1, a0, a0
jal U32LOADFUNC //get a2 arg
addiu t8, t8, 4
or a2, a0, a0
jal U32LOADFUNC //get a3 arg
addiu t8, t8, 4
or a3, r0, a0
jal U32LOADFUNC //get f12 arg
addiu t8, t8, 4
mtc1 a0, f12
jal U32LOADFUNC //get f14
addiu t8, t8, 4
mtc1 a0, f14
jalr t3
lw a0, 0x1c (sp)
lw t8, 0x18 (SP)
lb t7, 0x9 (T8)
sll t7, t7, 2
addu at, s7, t7
sw v0, 0x6e80 (AT)
lbu t7, 0x20 (SP)
li at, 0xa1
beq at, t7, advancedbythirty
or a0, r0, t8
jal ADDTOBUFFERFUNC
li a1, 10
advancedbythirty:
lw t0, 0x6d0c (S7)
addiu t0, t0, 30
b parse
sw t0, 0x6d0c (S7)


negfuncreturns:
addu at, t1, s7
b regfuncreturns
lw t0, 0x6e80 (AT) //returned value

//a2 cmd
displayonmatchingreturn:
jal U32LOADFUNC
addiu t8, t8, 1
lb t1, 0x0 (T8)
bltz t1, negfuncreturns
sll t1, t1, 2
addu at, t1, s7
lw t0, 0x6d90 (AT) //returned value
regfuncreturns:
lw t2, 0x6d0c (S7)
addiu t8, t8, 4
sw t8, 0x18 (SP)
addiu t2, t2, 0x6
beq A0, t0, parse
sw t2, 0x6d0c (S7)

notcorrectreturn:
addiu t8, t8, 0x1
lbu t0, 0x0 (T8)
li at, 0x95
beql at, t0, bracketsa2
lbu t4, 0x1 (T8)
li t2, 0x87
beql t0, t2, generaltextdisplay
addiu t7, t8, 1
li t3, 0xA2
bne t0, t3, notcorrectreturn
or t7, t8, r0
lw t6, 0x18 (SP)
subu t5, t7, t6
lw t1, 0x6d0c (S7)
addu t1, t1, t5
subiu t1, t1, 1
b displayonmatchingreturn
sw t1, 0x6d0c (S7)

//95 cmd
bracketsa2:
//t4 is key
li at, 0x96
addiu t8, t8, 1
ba2loop:
lbu t0, 0x0 (T8)
bne t0, at, ba2loop
addiu t8, t8, 1
lbu t5, 0x0 (T8)
bnel t5, t4, ba2loop
addiu t8, t8, 1
b notcorrectreturn
nop

//a5 cmd
SetMarioAnim:
jal GETLABELS
li at, 17//gmario
or a0, r0, v1
jal GETLABELS
li at, 19//set mario anim
jalr v1
lbu a1, 0x1 (t8)
b advancedbytwo
nop

//a6 cmd
SetMarioAction:
jal U32LOADFUNC
nop
jal GETLABELS
li at, 17//gmario
sw a0, 0xc (V1)
b advancedbyfive
lw at, 0x6d0c (S7)

//a7 cmd
AddBhvtoArray:
jal U32LOADFUNC
sw t8, 0x18 (sp)
li at, 0x13002ec0 ;mario bhv
beq a0, at, addmario
nop
jal GETLABELS
li at, 18//find ObjbyBhv
jalr v1
nop
addingtoarray:
lw t8, 0x18 (sp)
lbu t1, 0x5 (t8) ;obj index
sll t1, t1, 2
addu at, t1, s7
or v0, v1, v0
sw v0, 0x6e80 (at)
lw at, 0x6d0c (S7)
addiu at, at, 6
b parse
sw at, 0x6d0c (S7)

addmario:
jal GETLABELS
li at, 17//gmario
b addingtoarray
addiu v0, v1, 0x3c-0xa0


//a8 cmd
rotatetowardsobj:
lbu t0, 0x1 (t8) ;obj index
sll t0, t0, 2
addu at, t0, s7
lw v0, 0x6e80 (at);source

lbu t1, 0x2 (t8) ;obj index
sll t1, t1, 2
addu at, t1, s7
lw v1, 0x6e80 (at);target

lwc1 f2, 0xa0 (V0)
lwc1 f4, 0xa0 (V1)

lwc1 f16, 0xa8 (V0)
lwc1 f18, 0xa8 (V1)

sub.s f12, f18, f16
//f14 is x
//f12 is z
//returns angle as v0
sub.s f14, f4, f2
jal GETLABELS
li at, 20//atan2s
jalr v1
sw V0, 0x1c (SP)
lw t0, 0x1c (SP)
b advancedbythree
sw v0, 0xd4 (T0)


//93 cmd
skipuntilkey:
lbu t4, 0x1 (T8) //t4 is key
or t5, t4, r0 //t5 is key
addiu t8, t8, 2
lw t2, 0x6d0c (S7)
addiu t2, t2, 0x2
sw t2, 0x6d0c (S7)
li at, 0
notcorrectkey:
addiu t8, t8, 0x1
lbu t0, 0x0 (T8)
li t2, 0x94
bne t0, t2, notcorrectkey
addiu at, at, 1
lbu t4, 0x1 (T8) //t4 is key
lw t2, 0x6d0c (S7)
bne t4, t5, notcorrectkey
//correct key
addu t2, t2, at
b parse
sw t2, 0x6d0c (S7)


//97 cmd
setreturnptr:
lbu t4, 0x1 (T8)
sll t5, t4, 2
addiu t5, t5, 0x6db0
addiu t8, t8, 2
b advancedbytwo
sw t8, 0x0 (S7)


//98 cmd
gotoreturnptr:
lbu t4, 0x1 (T8)
sll t5, t4, 2
addiu t5, t5, 0x6db0
lw t8, 0x0 (S7)
beq t8, r0, advancedbytwo
nop
sw t8, 0x6df8 (S7)
sw r0, 0x6df0 (S7)
lh t1, 0x6d6c (S7) //original spd
bgezl t1, noresetVIs
sh t1, 0x6d08 (S7)
beq r0, r0, printnone
sw r0, 0x6dfc (S7) //end string pos



//88 cmd
shakescreen:
lb at, 0x6d60 (s7)
ori at, at, 1
sb at, 0x6d60 (s7)
advancedbyone:
lw t0, 0x6d0c (S7)
addiu t0, t0, 0x1
beq r0, r0, parse
sw t0, 0x6d0c (S7)

//89 cmd
disablescreenshake:
lb at, 0x6d60 (s7)
li t9, -2
and at, at, t9
beq r0, r0, advancedbyone
sb r0, 0x6d60 (s7)




//43 cmd
shortuserstring:
jal SETENVCOLORFUNC //set env color
sw t8, 0x18 (SP)
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
jal PREPSTRING //flushstring
nop
lw t1, 0x6df0 (S7) //current pos in string
lw t2, 0x6d14 (S7)
subu t2, t2, t1
sw t2, 0x6d14 (S7)
sw r0, 0x6df0 (S7)
sw r0, 0x6d54 (S7)
lw t8, 0x18 (SP)
lw at, 0x6d0c (S7) //temp string
addu at, at, t1
addiu at, at, 0x2
sw at, 0x6d1c (S7) //*string return loc
ori t1, s7, 0x6f00
lbu t9, 0x1 (T8)
li at, 0x10
multu t9, at
mflo at
addu t1, t1, at
beq r0, r0, parse
sw t1, 0x6d0c (S7)


//41 cmd
setsfx:
lw at, 0x6d0c (S7) //temp string
addiu at, at, 0x3
sw at, 0x6d0c (S7)

lw t9, 0x6df0 (S7) //current pos in string
lw t6, 0x6d14 (S7) //end pos in string
subu t7, t6, t9
bne t7, r0, parse
nop
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
nop
beq r0, r0, parse
sh t4, 0x6d18 (S7)



//40 cmd
setspeed:
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
nop
sh t4, 0x6d08 (S7)
advancedbythree:
lw t9, 0x6d0c (S7) //temp string
addiu t9, t9, 0x3
beq r0, r0, parse
sw t9, 0x6d0c (S7)


//FE cmd (not really a cmd but I made it one)
linebreak:
jal SETENVCOLORFUNC //set env color
sw t8, 0x18 (SP)
li t0, 0x9eff
lh at, 0x7000 (S7)
beq at, t0, noprintLB
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
lh a0, 0x6d04 (S7) //initial x cord
sh a0, 0x6d10 (S7) //temp x pos
lh a1, 0x6d12 (S7) //temp y pos
subiu a1, a1, 0xd
jal FLUSHSTRINGFUNC //flushstring
sh a1, 0x6d12 (S7)

//reset pos to zero so x pos doesn't get fuked, readjust string end pos to match
noprintLB:
lw at, 0x6df0 (S7) //current string pos
lw t1, 0x6d14 (S7) //temp string end pos
subu t1, t1, at
sw t1, 0x6d14 (S7)
sw r0, 0x6df0 (S7)
sw r0, 0x6d54 (S7)

lw t9, 0x6d0c (S7) //temp string pos
addu t9, t9, at
addiu t9, t9, 0x1

//reapply scaling and fix translation

jal POPMATRIXFUNC //pop matrix
sw t9, 0x6d0c (S7)
jal SCALEMATRIXFUNC //push scale matrix
nop
jal FIXSTRINGTRANSLATIONFUNC //fix translation after scale
nop

beq r0, r0, parse
nop



//70 cmd
AutoBoxEnd:
sw s6, 0x6d00 (S7)
lbu t2, 0x6dd4 (S7) //length
beqz t2, notrans
lw t1, 0x6dd0 (S7)
beqzl t1, nonewchar
sw s6, 0x6dd0 (S7)
subu t3, s6, t1 //VIs that have passed
subu t3, t2, t3 //VIs left
bgtz t3, nonewchar

notrans:
lbu at, 0x6d0a (S7)
andi at, at, 0x7f
sb at, 0x6d0a (S7)
lw at, 0x6df0 (S7) //temp string pos
sw r0, 0x6dd0 (S7)
lbu t2, 0x6de4 (S7)
beqz t2, nostarttrans
nop
sw s6, 0x6de0 (S7)
nostarttrans:
sw r0, 0x6df0 (S7)
lw t9, 0x6d0c (S7) //temp *string
addu t9, t9, at
addiu t9, t9, 0x1
sw t9, 0x6df8 (S7) //*string
li t7, 0xff
sb t7, 0x7000 (S7)
lh t1, 0x6d6c (S7) //original spd
bgezl t1, noresetVIs
sh t1, 0x6d08 (S7)
noresetVIs:
beq r0, r0, printnone
sw r0, 0x6dfc (S7) //end string pos



//71 cmd
ABoxEnd:
lbu at, 0x6d0a (S7)
andi t0, at, 0x80
sw s6, 0x6d00 (S7)
ori at, at, 0x80
beq t0, r0, nonewchar
sb at, 0x6d0a (S7)
lw at, 0x6dd0 (S7)
bne at, r0, AutoBoxEnd
nop
jal GETLABELS
li at, 0 //gcontrollers
lh t9, 0x12 (V1) //button pressed
andi t9, t9, 0x8000 //a press
andi t1, s6, 0x20
beq r0, t1, noarrowforend71
li t7, 0xff
li t7, 0x51
noarrowforend71:
li t6, 0xff
lw at, 0x6df0 (S7) //temp string pos
addu AT, S7, at
sb t6, 0x7001 (AT)
beq t9, r0, nonewchar
sb t7, 0x7000 (AT)
beq r0, r0, AutoBoxEnd
nop

//enable transition cmd
enabletransition:
jal U32LOADFUNC
nop
sw t6, 0x6dd4 (S7)//end transition shit
b advancedbyfive
lw at, 0x6d0c (S7)

//setup begin transition
Begintransition:
jal U32LOADFUNC
nop
sw t6, 0x6de4 (S7)//start transition shit
b advancedbyfive
lw at, 0x6d0c (S7)


DotransitionlinterpolateSS:
sh t1, 0x20 (SP)
sh t2, 0x2A (SP)
sh t3, 0x2C (SP)
b Dotransitionlinterpolate
sh t4, 0x2E (SP)


//44 cmd
setscissor:
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
sw t8, 0x18 (SP)
or t1, r0, t4 //xi
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
addiu t8, t8, 0x2
or t2, r0, t4 //xf
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
addiu t8, t8, 0x2
li t0, 240
subu t3, t0, t4 //yi
jal U16LOADFUNC //load unaligned half, t8 in, t4 out
addiu t8, t8, 0x2
subu t4, t0, t4

lh t7, 0x28 (SP)
li at, 0xaa
beq at, t7, DotransitionlinterpolateSS

FinishScissor:
li t0, 0x00fff000
sll t1, t1, 14
and t1, t1, t0
lui t9, 0xed00
addu t9, t1, t9//ed xx x
sll t4, t4, 2
andi t4, t4, 0x0fff
addu t9, t9, t4
//t9 is upper
sll t2, t2, 14
and t2, t2, t0
sll t3, t3, 2
andi t3, t3, 0x0fff
addu t8, t2, t3
//t8 is lower
jal GETLABELS
li at, 10 //DisplayListHead
lw v0, 0x0 (V1)
sw t9, 0x0 (V0)
sw t8, 0x4 (V0)
addiu v0, v0, 8
sw v0, 0x0 (V1)
lbu at, 0x6d60 (S7)
ori at, at, 2
sb at, 0x6d60 (S7)
lw t9, 0x6d0c (s7)
addiu t9, t9, 9
lh t7, 0x28 (SP)
li at, 0xaa
bne at, t7, noextraScis
nop
addiu t9, t9, 9
noextraScis:
b parse
sw t9, 0x6d0c (s7)

//a9 cmd
wobblytext:
lbu t0, 0x1 (T8)
beqz t0, endwobbly
lb at, 0x6d0b (s7)
sll t0, t0, 4
b advancedbytwo
sb t0, 0x6d0b (s7)

endwobbly:
b advancedbytwo
sb r0, 0x6d0b (s7)

//45 cmd
returnuserstring:
jal SETENVCOLORFUNC //set env color
sw t8, 0x18 (SP)
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
jal PREPSTRING //flushstring
nop

lw t1, 0x6df0 (S7) //current pos in string
lw t2, 0x6d14 (S7)
subu t2, t2, t1
sw t2, 0x6d14 (S7) //temp end pos
sw r0, 0x6df0 (S7)
sw r0, 0x6d54 (S7)
lw at, 0x6d1c (S7) //temp string
beq r0, r0, parse
sw at, 0x6d0c (S7) //*string return loc


//FF cmd (just stops display, does not remove string)
endofstring:

//draw previous string
jal SETENVCOLORFUNC //set env color
sw t8, 0x18 (SP)
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint //print string
ori a2, s7, 0x7000
jal FLUSHSTRINGFUNC //flushstring
nop
jal POPMATRIXFUNC //pop matrix
nop
jal SCALEMATRIXFUNC //push scale matrix
nop
jal FIXSTRINGTRANSLATIONFUNC //fix translation after scale
nop

//update *end of string
lw t1, 0x6df0 (S7)
lw t0, 0x6d14 (S7)
subu t0, t0, t1
sw t0, 0x6d14 (S7)

//update *temp string 
lw v1, 0x6d0c (S7) //*temp string
addu v1, v1, t1
addiu v1, v1, 0x1
sw v1, 0x6d0c (S7) //*temp string 


//set x pos to be init pos for line break
lh t1, 0x6d04 (S7)
sh t1, 0x6d10 (S7)
li at, 8
jal XPOSRESETFUNC //update x pos of string
sw at, 0x6d54 (S7) //temp x pos

//line break y pos
lh a1, 0x6d12 (S7) //temp y pos
subiu a1, a1, 0xd
sh a1, 0x6d12 (S7)


//6d58 will store returned dialog option
//6d59 will store currently displaying dialog option
//6d5A will store total number of options
//6d5B will store hovered dialog option

//end string after all options are drawn
lbu t0, 0x6d59 (S7)
lbu t1, 0x6d5A (S7)
BEQL t1, t0, printnone
sw v1, 0x6d5C (S7)
addiu t3, t0, 1
//draw arrow on current hovered line
sb t3, 0x6d59 (S7)
lb t2, 0x6d5b (S7)
bne t2, t3, noarrowenddialog
li t1, 0x53
li at, 0xff
sb at, 0x7001 (S7)


jal SETENVCOLORFUNC //set env color
sb t1, 0x7000 (S7)
lh a0, 0x6d04 (S7)
sh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7) //temp y pos
jal transitionprint //print string
ori a2, s7, 0x7000
jal FLUSHSTRINGFUNC //flushstring
li t9, 0x8
jal XPOSRESETFUNC //update x pos of string
sw t9, 0x6d54 (S7)
jal POPMATRIXFUNC //pop matrix
nop
jal SCALEMATRIXFUNC //push scale matrix
nop
jal FIXSTRINGTRANSLATIONFUNC //fix translation after scale
nop

noarrowenddialog:
lw t1, 0x6d0c (S7)
sw r0, 0x6d54 (S7)
beq r0, r0, parse
sw r0, 0x6df0 (S7)




//the following is just a character not a cmd
normalchar:
lw t9, 0x6df0 (S7) //current pos in string
lw t8, 0x6d14 (S7) //end pos in string
subu t7, t8, t9
bne t7, r0, advancedchar

//here I check if I should advanced the end pos of the string, which should happen when a new chars time is here

lw at, 0x6d00 (S7) //VI of last character
lh t7, 0x6d08 (S7) //VIs per character
bgez t7, VIperChar
addu t8, t7, at //check if new char should be drawn
;negative numbers will act as Chars per VI
jal GETLABELS
li at, 23 ;absi
jalr v1
or a0, t7, r0
sllv t1, at, v0
sllv t0, s6, v0
addu t1, t1, s1
addiu s1, s1, 1
subu t0, t0, t1
bgtz t0, drawchar
sw at, 0x1c (SP)
;in this case, I've drawn all the characters needed this VI
b nonewchar
sw s6, 0x6d00 (S7)
VIperChar:
sw t8, 0x1c (SP)
subu t8, s6, t8
//compares VI of last char drawn + wait time to current VI.
bltz t8, nonewchar

drawchar:
//here is a check to play a sfx
lh a0, 0x6d18 (S7) //sfx pointer
beq a0, r0, checkblip
sll a0, a0, 16
jal GETLABELS
li at, 16//sound args
or a1,r0,v1
jal GETLABELS
li at, 15//play sfx
jalr v1//play sfx
ori a0, a0, 0x0081

checkblip:
lb a0, 0x6d1a (S7) //sfx check
beq a0, r0, drawnewchar
lui a0, 0x16 //ice (placeholder)
jal GETLABELS
li at, 16//sound args
or a1,r0,v1
jal GETLABELS
li at, 15//play sfx
jalr v1//play sfx
ori a0, a0, 0xff81




drawnewchar:
lw t8, 0x6dfc (S7) //end pos in string
addiu t8, t8, 0x1
sw t8, 0x6dfc (S7)
lw t8, 0x6d14 (S7) //temp end pos in string
addiu t8, t8, 0x1
sw t8, 0x6d14 (S7)
lw t0, 0x1C (SP)
sw t0, 0x6d00 (S7) //make last VI + spd the last VI of char
advancedchar:
lw t9, 0x6df0 (S7) //current pos in string
addu t0, s7, t9
lw t8, 0x6d0c (S7)
addu t8, t9, t8
lbu t7, 0x0 (T8) //char in string to write

//80331370 is char kerning table
jal GETLABELS
li at, 11//char kerning
addu t6, v1, t7
lbu t5, 0x0 (T6)
lw t4, 0x6d54 (S7)
addu t4, t4, t5
sw t4, 0x6d54 (S7)

sb t7, 0x7000 (T0)
li t6, 0xff
sb t6, 0x7001 (T0)
addiu t9, t9, 0x1
//check for wobbly text
lbu at, 0x6d0b (S7)
andi t0, at, 0xf0
beq t0, r0, parse
sw t9, 0x6df0 (S7) //advanced position in string
//if wobbly text, print
addiu at, at, 7
andi at, at, 0x0f
or at, at, t0
jal SETENVCOLORFUNC //set env color
sb at, 0x6d0b (S7)
lbu at, 0x6d0b (S7)
andi t2, at, 0xf0 //wobble spd
srl t2, t2, 4
sll t1, s6, 1
andi at, at, 0xf
addu at, t1, at
multu t2, at
mflo at
mtc1 at, f12
cvt.s.w f12,f12
li.s f2, 0.025
jal GETLABELS
li at, 13//sinf
jalr v1 //sinf
mul.s f12, f12, f2
li.s f2, 3.0
mul.s f0, f0, f2
cvt.w.s f0, f0
lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
mfc1 at, f0
subiu at, at, 3
addu a1, at, a1
jal transitionprint
ori a2, s7, 0x7000
jal FLUSHSTRINGFUNC
nop
jal XPOSRESETFUNC //update x pos of string
nop
lw t8, 0x6d14 (S7) //end pos in string
subiu t8, t8, 1
sw t8, 0x6d14 (S7)
b advancedbyone
sw r0, 0x6df0 (S7)

//The string has gone as far as it needs to go
nonewchar:

//print whatever has not been printed already


lb at, 0x6d61 (S7)
bne at, r0, disableplaintext
sb r0, 0x6d61 (S7)


jal SETENVCOLORFUNC //set env color
nop

lh a0, 0x6d10 (S7)
lh a1, 0x6d12 (S7)
jal transitionprint
ori a2, s7, 0x7000

printnone:
lb at, 0x6d60 (S7)
andi at, at, 1
beq at, r0, checkscissor
nop
jal 0x802A50FC //shake screen
li a0, 0x1


checkscissor:
lb at, 0x6d60 (S7)
andi at, at, 2
beq at, r0, noscreenshake
nop
jal GETLABELS
li at, 10 //DisplayListHead
lw v0, 0x0 (V1)
lui t9, 0xed00
ori t9, t9, 0x20
sw t9, 0x0 (V0)
lui t8, 0x0050
ori t8, t8, 0x03a0
sw t8, 0x4 (V0)
addiu v0, v0, 8
sw v0, 0x0 (V1)

noscreenshake:
//end a string
jal INSERTCHARENDDLFUNC //end string
sh r0, 0x6d62 (S7)
sw v0, 0x6d64 (S7) //end ptr
jal FLUSHSTRINGFUNC //flushstring
sw r0, 0x6df0 (S7)

end:
lw s1, 0x38 (SP)
lw s2, 0x50 (SP)
lw s3, 0x4c (SP)
lw s4, 0x48 (SP)
lw s5, 0x44 (SP)
lw s6, 0x40 (SP)
lw s7, 0x3c (SP)
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x58


KEYBOARDSTARTLOC:
.incbin KEYBOARD_FILE

//.skip filesize("keyboard.bin")

.align


transitionprint:
//new print, replaces set env and print in prints
//takes a2 *string
//a1 y pos
//a0 x pos

//in struct
//1 byte length
//1 byte target alpha
//1 byte direction
//1 byte spd
//length of zero doubles as
//bool check

//1 word initial VI timer

addiu sp, sp, -0x28
sw ra, 0x14 (sp)

sw a2, 0x1c (sp) //*string
sh a0, 0x20 (sp)
sh a1, 0x22 (sp)
or t9, r0, s7
lw t1, 0x6dd0 (S7) //end transition timer
bnel t1, r0, EndTransition
sw s6, 0x6d00 (S7)
lw t2, 0x6de0 (S7) //start trans timer
beq t2, r0, Callgenericprint
nop
addiu t9, s7, 0x10
or t1, t2, r0
EndTransition:
lbu a2, 0x6dd4 (t9) //transition length
beq a2, r0, Callgenericprint
subu a3, s6, t1 //VIs since transition start
slt at, a3, a2
beqzl at, ZeroTimer
or a3, r0, a2
ZeroTimer:
sb a3, 0x26 (sp)


lbu a1, 0x6d3b (S7)//Currenvalpha
sb a1, 0x25 (sp)
jal LINEARINTERPOLATE
lbu a0, 0x6dd5 (t9)//Targetenvalpha


jal SETENVCOLORFUNC
sb v0, 0x6d3b (S7)//Currenvalpha
lb t7, 0x25 (sp)
sb t7, 0x6d3b (S7)//Currenvalpha

//now linear interpolate
//position
//first byte is angle, second is spd
//a0 is intended x, a1 y
lbu t1, 0x6dd6 (t9)//angle
sll a0, t1, 8
jal sin_u16
sw a0, 0x4 (sp)
swc1 f0, 0x8 (sp)
jal cos_u16
lw a0, 0x4 (sp)
lbu t0, 0x6dd7 (t9)//spd
lb t3, 0x26 (sp) //VIs since start
multu t0, t3
mflo t4 //total units

lwc1 f10, 0x8 (sp)
mtc1 t4, f2
cvt.s.w f2, f2
mul.s f4, f2, f10 //y change
mul.s f6, f2, f0 //x change
cvt.w.s f4, f4
mfc1 t4, f4
cvt.w.s f2, f6
mfc1 t2, f2

lh a0, 0x20 (sp)
lh a1, 0x22 (sp)
addu a0, a0, t2
addu a1, a1, t4
andi a0, a0, 0xff
andi a1, a1, 0xff
Callgenericprint:
jal GETLABELS
li at, 12//print generic
jalr v1 //printfunction
lw a2, 0x1c (sp)

lw ra, 0x14 (sp)
jr ra
addiu sp, sp, 0x28


/* sin_u16
Take the sine of an angle stored as a short
args:
	A0 - [short] the angle
returns:
	F0 - [float] the sine of the angle
*/
sin_u16:
ANDI A0, A0, 0xFFF0
SRL A0, A0, 0x2
LUI AT, 0x8038
ADDU A0, AT, A0
JR RA
L.S F0, 0x6000 (A0)

/* cos_u16
Take the cosine of an angle stored as a short
args:
	A0 - [short] the angle
returns:
	F0 - [float] the sine of the angle
*/
cos_u16:
ANDI A0, A0, 0xFFF0
SRL A0, A0, 0x2
LUI AT, 0x8038
ADDU A0, AT, A0
JR RA
L.S F0, 0x7000 (A0)




//load unaligned half, t8 holds address, returns t4
U16LOADFUNC:
Lbu t9, 0x1 (T8) //params of cmd
Lbu t4, 0x2 (T8) //params of cmd
sll t9, t9, 8
jr ra
or t4, t9, t4 //xi


//update x pos of string
XPOSRESETFUNC:
lw t2, 0x6d54 (S7) //kerning increase
lwc1 f4, 0x6d30 (S7) //x scale
mtc1 t2, f2
cvt.s.w f2, f2
mul.s f2, f2, f4 //kern*xscale
cvt.w.s f2, f2
mfc1 t2, f2
lh a0, 0x6d10 (S7)
addu a0, a0, t2
sw r0, 0x6d54 (S7)
jr ra
sh a0, 0x6d10 (S7)



//unaligned load, t8 in, a0 out

U32LOADFUNC:
andi t9, t8, 0x3 //unaligned address mod 4
addiu t9, t9, 0x1
sll t9, t9, 3
nor t7, r0, t9
addiu t7, t7, 0x21
subiu at, r0, 0x1
srlv t1, at, t7
sllv at, at, t9
LWR t7, 0x4 (T8)
and t7, t7, t1
LWL t6, 0x1 (T8)
and t6, t6, at
jr ra
or a0, t7, t6


//pop matrix
POPMATRIXFUNC:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)
jal GETLABELS
li at, 10//DisplayListHead
lw v0, 0x0 (V1)
li t6, 0xBD380002
sw t6, 0x0 (V0)
li t6, 1
sw t6, 0x4 (V0)
addiu v0, v0, 0x8
sw v0, 0x0 (V1)

lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18



//scale matrix
SCALEMATRIXFUNC:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)

lw a1, 0x6d30 (S7)
lw a2, 0x6d34 (S7)
lui a3, 0x3f80 //1 = scale
jal GETLABELS//uses V1 and AT
li at, 3//scale
jalr v1 //scale matrix
ori a0, r0, 0x1 //push matrix

lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18


//fix string translate position
//fix positions for scale using this formula
//t=x/s-x=x(1/s-1)
FIXSTRINGTRANSLATIONFUNC:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)
lwc1 f4, 0x6d30 (S7) //x scale
lwc1 f6, 0x6d34 (S7) //y scale
lui at, 0x3f80
mtc1 at, f20
lh a0, 0x6d10 (S7)
mtc1 a0, f0
lh t2, 0x6d12 (S7) //temp y
cvt.s.w f0, f0 //temp x
mtc1 t2, f2
div.s f4, f20, f4 //1/Sx
cvt.s.w f2, f2 //temp y
div.s f6, f20, f6 //1/Sy
sub.s f6, f6, f20 //1/Sy-1
sub.s f4, f4, f20 //1/Sx-1
mul.s f4, f4, f0 //X(1/Sx-1)
mul.s f6, f6, f2 //y(1/Sy-1)
mfc1 a1, f4 //x translate
mfc1 a2, f6 //y translate
or a3, r0, r0
jal GETLABELS//uses V1 and AT
li at, 4
jalr v1 //translate matrix
ori a0, r0, 0x2 //multiply
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18



//insert char start ptr

INSERTCHARSTARTDLFUNC:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)
jal GETLABELS
li at, 2//ortho
jalr v1//ortho matrix
nop
jal GETLABELS
li at, 10//DisplayListHead
lw v0, 0x0 (v1)
lui t6, 0x0600
sw t6, 0x0 (V0)
jal GETLABELS
li at, 7//ia8text begin
sw v1, 0x4 (V0)
jal GETLABELS
li at, 10//DisplayListHead
addiu v0, v0, 0x8
sw v0, 0x0 (V1)
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18



//insert char end ptr

INSERTCHARENDDLFUNC:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)

jal GETLABELS
li at, 10//DisplayListHead
lw v0, 0x0 (V1)
lui t6, 0x0600
sw t6, 0x0 (V0)
jal GETLABELS
li at, 8//ia8text end
sw v1, 0x4 (V0)
jal GETLABELS
li at, 10//DisplayListHead
addiu v0, v0, 0x8
sw v0, 0x0 (V1)
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18


//flush string

FLUSHSTRINGFUNC:
lui at, 0x807f
flushnext:
lw t1, 0x7000 (AT)
beq t1, r0, endflush
sw r0, 0x7000 (AT)
beq r0, r0, flushnext
addiu at, at, 0x4
endflush:
li t1, 0xff
li t2, 0x9e
lui at, 0x807f
sb t2, 0x7000 (AT)
jr ra
sb t1, 0x7001 (AT)


//SET ENV COLOR
SETENVCOLORFUNC:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)
lw v0, 0x6de8 (S7)
lw a0, 0x6d38 (S7)
beq a0, v0, Savehead
nop
jal GETLABELS
li at, 10//DisplayListHead
lw v0, 0x0 (V1)
sw a0, 0x6de8 (S7)
lui t6, 0xFB00
sw t6, 0x0 (V0)
sw a0, 0x4 (V0)
addiu v0, v0, 0x8
sw v0, 0x0 (V1)
Savehead:
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18


//completely reset string

COMPLETESTRINGRESETFUNC:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)
lui at, 0x807f
lh a1, 0x6d20 (AT) //original music
lh t1, 0x6d22 (AT) //switched to music
beq a1, t1, endresetstring
nop
jal GETLABELS
li at, 14//play sequence
jalr v1 //set music
or a0, r0, r0

endresetstring:
jal CLEANUPBUFFERFUNC //cleanup buffer
lui s7, 0x807f
jal CLEANUPVIBUFFER
nop
jal FLUSHSTRINGFUNC //flush string
nop
jal INSERTCHARENDDLFUNC //end string
nop
addiu at, s7, 0x6d00
addiu t9, at, 0x100
clear1:
sw r0, 0x0 (AT)
bne t9, at, clear1
addiu at, at, 0x4

lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18



//signed unaligned half load

S16LOADFUNC:
lb t3, 0x1 (T8)
lbu t4, 0x2 (T8)
sll t3, t3, 8
jr ra
or a0, t3, t4 //SIGNED value

LINEARINTERPOLATE:
//a0 is target
//a1 is current
//a2 is max value length
//a3 is VIs passed

//linear interpolate s16

mtc1 a2, f4
mtc1 a3, f2
cvt.s.w f4, f4
cvt.s.w f2, f2
div.s f6, f2, f4 //fractional completion

subu a0, a0, a1
mtc1 a0, f8
cvt.s.w f8, f8
mul.s f8, f6, f8
mtc1 a1, f10
cvt.s.w f10, f10
add.s f8, f8, f10 //(target-current)*percent+current
cvt.w.s f8, f8
jr ra
mfc1 v0, f8// interpolated value



ADDVIBUFFER:
//a0 - location
lw at, 0x6dd8 (S7) //pos of buffer
addu a1, at, s7
sw a0, 0x6600 (A1)
addiu at, at, 4
jr ra
sw at, 0x6dd8 (S7)


CLEANUPVIBUFFER:
lw at, 0x6dd8 (S7) //pos of buffer
beqz at, VIbuffclean
addu t1, at, s7
lw t2, 0x65fc (T1)
VIbuffloop:
sb r0, 0x0 (T2)
sb r0, 0x1 (T2)
sb r0, 0x2 (T2)
sb r0, 0x3 (T2)
subiu t1, t1, 4
slt t3, s7, t1
beqz t3, VIbuffclean
lw t2, 0x65fc (T1)
b VIbuffloop
nop
VIbuffclean:
jr ra
sw r0, 0x6dd8 (S7)

//ADDTOBUFFERFUNC
//a0 = location start
//a1 = num of bytes to add to buffer

ADDTOBUFFERFUNC:
li t7, 0x9d
lw at, 0x6d68 (S7) //original string start
subu t0, a0, at //offset
lw t1, 0x6ddc (S7) //buffer ptr
addu t1, t1, s7
sb t0, 0x6001 (T1)
srl t0, t0, 8
sb t0, 0x6000 (T1)
sb a1, 0x6002 (T1) //length
addiu t1, t1, 1
or t9, r0, r0
bufferaddloop:
lb t2, 0x0 (A0)
beq t9, a1, endbufferadd
addiu t1, t1, 1
sb t2, 0x6001 (T1)
sb t7, 0x0 (A0)
addiu a0, a0, 1
beq r0, r0, bufferaddloop
addiu t9, t9, 1

endbufferadd:
lw t1, 0x6ddc (S7) //buffer ptr
addiu a1, a1, 3
addu t1, a1, t1
jr ra
sw t1, 0x6ddc (S7)



//CLEANUPBUFFER
CLEANUPBUFFERFUNC:
or t3, r0, s7
lw at, 0x6d68 (T3) //string start
checknextaddress:
lbu t1, 0x6000 (T3)
lbu t2, 0x6001 (T3)
sll t1, t1, 8
addu t1, t1, t2
lbu t4, 0x6002 (T3) //length
beq t4, r0, bufferfullclean
addu t2, t1, at //ptr to load string from
sb r0, 0x6000 (T3)
sb r0, 0x6001 (T3)
sb r0, 0x6002 (T3)
addiu t3, t3, 3
removepaddingloop:
subiu t4, t4, 1
lbu t9, 0x6000 (T3)
sb t9, 0x0 (T2)
sb r0, 0x6000 (T3)
addiu t3, t3, 1
beqz t4, checknextaddress
addiu t2, t2, 1
b removepaddingloop
nop

bufferfullclean:
jr ra
sw r0, 0x6ddc (S7)


//PREP STRING
PREPSTRING:
addiu sp, sp, -0x18
sw ra, 0x14 (SP)
jal FLUSHSTRINGFUNC //flushstring
nop
jal XPOSRESETFUNC //update x pos of string
nop
jal POPMATRIXFUNC //pop matrix
nop
jal SCALEMATRIXFUNC //push scale matrix
nop
jal FIXSTRINGTRANSLATIONFUNC //fix translation after scale
nop
lw ra, 0x14 (SP)
jr ra
addiu sp, sp, 0x18


//Get Labels
GETLABELS:
li v1, SEG_TEST
sll at, at, 2
addu v1, v1, at
jr ra
lw v1, 0x00 (V1) //changes depending on segments.h