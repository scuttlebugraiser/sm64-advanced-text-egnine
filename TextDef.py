import re
import math
from bitstring import *
from ctypes import *
Ascii={}
for x in range(10):
	Ascii[str(x)]=hex(x)
def char_range(c1,c2):
	for c in range(ord(c1),ord(c2)+1):
		yield chr(c)
for x in char_range('A','Z'):
	Ascii[x]=hex(ord(x)-55)
for x in char_range('a','z'):
	Ascii[x]=hex(ord(x)-61)
Ascii.update({
'[up]':hex(0x50),
'[down]':hex(0x51),
'[left]':hex(0x52),
'[right]':hex(0x53),
'[A]':hex(0x54),
'[B]':hex(0x55),
'[C]':hex(0x56),
'[Z]':hex(0x57),
'[R]':hex(0x58),
',':hex(0x6f),
'/':hex(0xd0),
'[the]':hex(0xd1),
'[you]':hex(0xd2),
' ':hex(0x9e),
'-':hex(0x9f),
'$':hex(0xf9),
'(':hex(0xe1),
')':hex(0xe3),
'+':hex(0xe4),
'&':hex(0xe5),
':':hex(0xe6),
'!':hex(0xf2),
'”':hex(0xf5),
'?':hex(0xf4),
'“':hex(0xf6),
'~':hex(0xf7),
'[x]':hex(0xfb),
'[·]':hex(0xfc),
'Hollow Star':hex(0xfd),
'FUll Star':hex(0xfa),
'\n':hex(0xfe),
'.':hex(0x3f),
"'":hex(0x3e),
'[end]':hex(0xff)
})
#color 1 is BG, color 2 is FG
#colors should be hexrgb string
colors={
hex(0x40):['#103040','#FFFFFF'],
hex(0x41):['#2000FF','#FF6020'],
hex(0x42):['green','white'],
hex(0x43):['red','#0000BF'],
hex(0x44):['yellow','green'],
hex(0x45):['white','black'],
hex(0x46):['green','yellow'],
hex(0x47):['dark red','light blue'],
hex(0x48):['#90F5FF','red'],
hex(0x49):['#D7E388','#21691f'],
hex(0x4a):['#17E3f8','#21691f'],
hex(0x4b):['#D723D8','#11191f'],
hex(0x4c):['#ffc0c0','magenta'],
hex(0x4d):['#E6B7E8','black'],
hex(0x4e):['green','#E6B7E8'],
hex(0x4f):['purple','white'],
hex(0x70):['#106030','orange'],
hex(0x71):['#202020','pink'],
hex(0x72):['#B5AC0B','white'],
hex(0x73):['#3050f0','#90ffff'],
hex(0x74):['#701090','light yellow'],
hex(0x75):['#10c060','white'],
hex(0x76):['orange','blue'],
hex(0x77):['blue','orange'],
hex(0x78):['#b15600','white'],
hex(0x79):['light blue','magenta'],
hex(0x7a):['pink','green'],
hex(0x7b):['dark green','light pink'],
hex(0x7c):['#8C0A0A','white'],
hex(0x7d):['yellow','black'],
hex(0x7e):['red','yellow'],
hex(0x7f):['yellow','red'],
hex(0x80):['#10e0a0','#2060e0'],
hex(0x81):['#2060e0','#10e0a0'],
hex(0x82):['light grey','dark red'],
hex(0x84):['green','light yellow'],
hex(0x85):['red','light yellow'],
hex(0x86):['#c030c0','#b0b0b0'],
hex(0x87):['grey','light blue'],
hex(0x88):['#30006B','#f010f0'],
hex(0x89):['brown','light green'],
hex(0x8a):['light green','brown'],
hex(0x8b):['brown','pink'],
hex(0x8c):['#FFa0FF','brown'],
hex(0x8d):['brown','light yellow'],
hex(0x8e):['orange','green'],
hex(0x8f):['green','orange'],
hex(0x90):['#c0c010','#2020d0'],
hex(0x91):['#206050','#c06090'],
hex(0x92):['#501030','#30a0c0'],
hex(0x93):['#408010','black'],
hex(0x94):['grey','black'],
hex(0x95):['#c0c010','#2020d0'],
hex(0x96):['#808080','#FFFFa0'],
hex(0x97):['#40d030','#3040D0'],
hex(0x98):['#6070e0','black'],
hex(0x99):['#2090e0','white'],
hex(0x9A):['#f07010','#2060e0'],
hex(0x9B):['#107010','#20e0e0'],
hex(0x9C):['#a07010','#2010e0'],
hex(0xA0):['#6020c0','white'],
hex(0xA1):['#750B65','#D15E00'],
hex(0xA3):['#D15E00','#750B65'],
hex(0xA2):['pink','grey'],
hex(0xA4):['grey','pink'],
hex(0xa5):['dark green','light pink'],
hex(0xa6):['#8C0A0A','white'],
hex(0xa7):['yellow','black'],
hex(0xa8):['grey','white'],
hex(0xa9):['yellow','black'],
hex(0xAA):['yellow','red']
}
#cmds format!!
#key=cmd value
#value is tuple with this format:
#mod 1-descriptions
#mod 2-params
Cmds={
hex(0x40):['Set text speed',(2,'VIs/char')],
hex(0x41):['Play SFX',(2,'SFX arg')],
hex(0x42):['Set Text Color',(3,'RGB',1,'Alpha')],
hex(0x43):['Insert Short User String',(1,'String Num')],
hex(0x44):['Set Scissor',(2,'x start',2,'x end',2,'y start',2,'y end')],
hex(0x46):['Enable Rainbow Text',(1,'Alpha')],
hex(0x47):['Change Origin',(2,'x pos',2,'y pos')],
hex(0x48):['Jump String',(4,'Segmented Address')],
hex(0x49):['Translate Text',(2,'x change',2,'y change')],
hex(0x4a):['Translate Text Absolute',(2,'x pos',2,'y pos')],
hex(0x4b):['Translate Text Moving(Not fully supported)',()],
hex(0x4c):['Enable A to Increase Speed',(2,'VIs')],
hex(0x4d):['Disable A to Increase Speed',()],
hex(0x4e):['Keep User Input on Re-Talk',()],
hex(0x4f):['Create User Input',(1,'String Num')],
hex(0x70):['Auto Goto Next Box',()],
hex(0x71):['Goto Next Box on A press',()],
hex(0x72):['Remove text for inputted time',(2,'VIs')],
hex(0x73):['Do Btn Check',(2,"Button")],
hex(0x74):['Pause for Inputted Time',(2, 'VIs')],
hex(0x75):['End Btn check',()],
hex(0x76):['Play Small Blip Each Char',()],
hex(0x77):['Disable Small Blip',()],
hex(0x78):['Continue Playing Music After String End',()],
hex(0x79):['Play Music Track',(1,'Music Track #')],
hex(0x7a):['Clear Cmd Buffer',()],
hex(0x7b):['End Display on A Press',()],
hex(0x7c):['Auto End Display After VIs',(2,'VIs')],
hex(0x7d):["Display Textured BG mosaic",(2,'x start',2,'x end',
2,'y start',2,'y end',4,'Texture',1,"x tiles",1,"y tiles")],
hex(0x7e):['Define Moving Textured BG Box(Not fully supported)',()],
hex(0x7f):['Create Shaded BG Box',(2,'x start',2,'x end',
2,'y start',2,'y end',3,'RGBA',1,'Alpha')],
hex(0x80):['Define Textured BG Box ',(2,'x start',2,'x end',
2,'y start',2,'y end',4,'Texture')],
hex(0x81):['Define Moving Shaded BG Box(Not fully supported)',()],
hex(0x82):['Enable Cutscene',(1,'Cutscene Index')],
hex(0x84):['Scale Text',(4,'X Scale',4,'Y Scale')],
hex(0x85):['Enable Dialog Options',(1,'Num Options')],
hex(0x86):['Display on Matching Dialog Option',(1,'Dialog Option')],
hex(0x87):['Display General Text',()],
hex(0x88):['Enable Screen Shake',()],
hex(0x89):['Disable Screen Shake',()],
hex(0x8a):['Set Cam Location',(2,'x pos',2,'y pos',2,'z pos')],
hex(0x8b):['Set Cam Focus',(2,'x pos',2,'y pos',2,'z pos')],
hex(0x8c):['Set Cam Location Speed',(2,'VIs',2,'x pos',2,'y pos',2,'z pos')],
hex(0x8d):['Set Cam Focus Speed',(2,'VIs',2,'x pos',2,'y pos',2,'z pos')],
hex(0x8e):['Lock Camera On Obj',(4,'Ptr to Object')],
hex(0x8f):['Trigger Warp',(2,'Warp Delay',1,'Warp ID')],
hex(0x90):['Set Animation',(1,'Obj Index',1,'Anim Index')],
hex(0x91):['Path to object Point',(1,'Padding',1,'Object ID',2,'x pos',2,'y pos',2,'z pos',2,'Speed')],
hex(0x92):['Drop Object to Ground',(1,'Obj Index')],
hex(0x93):['Skip Until Key',(1, 'key')],
hex(0x94):['End Skip Until Key',(1, 'key')],
hex(0x95):['Begin Dialog Option Mask',(1, 'key')],
hex(0x96):['End Dialog Option Mask',(1, 'key')],
hex(0x97):['Setup Return Pointer',(1, 'index')],
hex(0x98):['Goto Return Pointer',(1, 'index')],
hex(0x99):['Enable Plaintext backdrop shadow',()],
hex(0x9b):['Setup End Transition',(1,'Length',1,"Target Alpha",1,"Direction",1,'Speed')],
hex(0x9c):['Setup Start Transition',(1,'Length',1,"Target Alpha",1,"Direction",1,'Speed')],
hex(0x9A):['Disable Plaintext backdrop shadow',()],
hex(0xA0):['Call Function Once',(4,'A0 Argument',4,'Function',1,'Index',4,'A1 Argument',4,'A2 Argument',4,'A3 Argument',4,'F12 Argument',4,'F14 Argument')],
hex(0xA1):['Call Function Every Frame',(4,'A0 Argument',4,'Function',1,'Index',4,'A1 Argument',4,'A2 Argument',4,'A3 Argument',4,'F12 Argument',4,'F14 Argument')],
hex(0xA2):['Display on Matching Function Return',(1,'Index',4,'Return Value')],
hex(0xA3):['Set Camera Pos Relative to Focus',(2,'x pos',2,'y pos',2,'z pos')],
hex(0xA4):['Toggle Timestop',(1,'Toggle')],
hex(0xA5):['Set Mario Animation',(1,'Anim ID')],
hex(0xA6):['Set Mario Action',(4,'Action ID')],
hex(0xA7):['Add Behavior to Obj Arr',(4,'Behavior ID',1,'Obj Index')],
hex(0xA8):['Rotate Towards Object',(1,'Source Obj Index',1,'Target Obj Index')],
hex(0xA9):['Wobbly Text',(1,'Wobbling Speed')],
hex(0xAA):['Set BG box Transition',(2,'x start',2,'x end',2,'y start',2,'y end')],
hex(0xAB):['Clear VI Buffer',()],
}
def numput(value,numby,map):
	e = int(re.sub("[^0-9]","",value))
	limit = int(math.pow(int(2),int(numby)*4))-1
	if e>limit:
		e=limit
	return hex(int(e))
def textput(value,numby,map):
	e=value.strip()
	if not hasattr(map,'test'):
		e = re.sub("[^0-9a-fA-F]","",value)
		if len(e)>numby:
			e=e[0:numby]
	return e

def snumput(value,numby,map):
	if value.startswith("-"):
		neg=-1
	else:
		neg=1
	e = int(re.sub("[^0-9]","",value))
	limit = int(math.pow(int(2), int(numby) * 4)/2)-1
	if e > limit:
		e = limit
	sign = (1<<(numby*4))
	if neg==-1:
		e = e-sign
	return hex(e)

def fextput(value,numby,map):
	e = re.sub("[^0-9a-fA-F.]", "", value)
	f1 = BitArray(float=float(e),length=32)
	return f1
def colput(value,numby,map):
	return value.replace("#","")


def numout(value,numby,map):
	return int(value,16)

def textout(value,numby,map):
	return value

def snumout(value,numby,map):
	x = BitStream('hex:%d = %s' % (numby,value))
	print(x)
	value = x.unpack('int:%d'%numby)
	return value

def fextout(value,numby,map):
	i = int(value, 16)  # convert from hex to a Python int
	cp = pointer(c_int(i))  # make this into a c integer
	fp = cast(cp, POINTER(c_float))  # cast the int pointer to a float pointer
	return fp.contents.value  # dereference the pointer, get the float


ent={
hex(0x40):[snumput],
hex(0x41):[textput],
hex(0x49):[snumput,snumput],
hex(0x48):[textput],
hex(0x4C):[snumput],
hex(0x85):[snumput],
hex(0xA0):[textput,textput,snumput,textput,textput,textput,textput,textput],
hex(0xA1):[textput,textput,snumput,textput,textput,textput,textput,textput],
hex(0x8e):[textput],
hex(0xA2):[snumput,snumput],
hex(0xa6):[textput],
hex(0xa7):[textput],
hex(0x73):[textput],
hex(0x84):[fextput,fextput],
hex(0x7d):[numput,numput,numput,numput,textput],
hex(0x7f):[numput,numput,numput,numput,colput],
hex(0x80):[numput,numput,numput,numput,textput],
hex(0x42):[colput,numput],
hex(0x8a):[snumput,snumput,snumput],
hex(0xa3):[snumput,snumput,snumput],
hex(0x8b):[snumput,snumput,snumput],
hex(0x8c):[numput,snumput,snumput,snumput],
hex(0x8d):[numput,snumput,snumput,snumput],
hex(0x91):[numput,numput,snumput,snumput,snumput,snumput]
}

revent={
hex(0x40):[snumout],
hex(0x85):[snumout],
hex(0x41):[textout],
hex(0x49):[snumout,snumout],
hex(0x48):[textout],
hex(0x4C):[snumout],
hex(0xA0):[textout,textout,snumout,textout,textout,textout,textout,textout],
hex(0xA1):[textout,textout,snumout,textout,textout,textout,textout,textout],
hex(0x8e):[textout],
hex(0xA2):[snumout,snumout],
hex(0xa6):[textout],
hex(0xa7):[textout],
hex(0x73):[textout],
hex(0x84):[fextout,fextout],
hex(0x7d):[numout,numout,numout,numout,textout],
hex(0x7f):[numout,numout,numout,numout,textout],
hex(0x80):[numout,numout,numout,numout,textout],
hex(0x42):[textout,numout],
hex(0x8a):[snumout,snumout,snumout],
hex(0xa3):[snumout,snumout,snumout],
hex(0x8b):[snumout,snumout,snumout],
hex(0x8c):[numout,snumout,snumout,snumout],
hex(0x8d):[numout,snumout,snumout,snumout],
hex(0x91):[numout,numout,snumout,snumout,snumout,snumout]
}

notes={
hex(0x40):'Speed each new character will appear on screen. There are approx two VI per frame. Use a negative number for characters per VI',
hex(0x41):'sfx arguments for the playsound function. Only the upper value is taken, e.g. 2 hex bytes',
hex(0x42):'Color is RGB, alpha is a value ranging from 0 to 255',
hex(0x43):'insert an index from 0 to 15',
hex(0x44):'a scissor is a region where things are displayed. Outisde of the area, nothing is displayed. Y coordinates are slightly offset from text.',
hex(0x45):'you should not see this',
hex(0x46):'alpha ranges from 0 to 255',
hex(0x47):'screen coordinates start at the bottom left and go up and right',
hex(0x48):'Input a segmented address of the string you want to jump to. To input a ram address, start with 00. Does not make a new box',
hex(0x49):'screen coordinates start at the bottom left and go up and right. Translation is offset from current position.',
hex(0x4a):'screen coordinates start at the bottom left and go up and right. Translation is in absolute screen coordinates.',
hex(0x4b):'Following the normal translation offset cmd inputs, input keyframe lengths and positions for movement. Will require manual input. See readme for more details.',
hex(0x4c):'In order to use this, you must declare an 0x40 set text speed cmd in this box before this. You will have to declare this cmd in each new box aswell. Use a negative number for characters per VI',
hex(0x4d):'disables all speed increase',
hex(0x4e):'Use this cmd before an 0x4F in order for the keyboard to appear again once you initiate dialog',
hex(0x4f):'indexes range from 0 to 15',
hex(0x70):'boxes will not preserve any cmd except for origin, blip flags and text spd',
hex(0x71):'boxes will not preserve any cmd except for origin, blip flags and text spd',
hex(0x72):'all text will be removed for the declared amount of VIs',
hex(0x73):'Uses the same button codes as gameshark button activators',
hex(0x74):'will stop the next character from being drawn until the alloted time is finished',
hex(0x75):"Place after a button check to end conditional",
hex(0x76):'plays a small sfx for each new character thats drawn',
hex(0x77):'disables blip',
hex(0x78):'If you have already used a 0x79 play music cmd, use this to keep playing the track when the string ends. USE AFTER 0x79',
hex(0x79):'Play a sequence in layer 1.',
hex(0x7a):'Clears the buffer, so that all cmds that execute only once can be re-executed. Should only be used with jump string',
hex(0x7b):'Completely end the dialog/string',
hex(0x7c):'Completely end the dialog/string',
hex(0x7d):'coordinates start from bottom left, so does the texture tiles. Tiles must be next to each other in memory.',
hex(0x7e):[],
hex(0x7f):'coordinates start from the bottom left and go towards the top right. Alpha ranges from 0 to 255',
hex(0x80):'A textured BG box. The texture is broken as of this update but this may be fixed later.',
hex(0x81):'A moving shaded BG box. Following the normal shaded BG box inputs is a set of keyframes and lengths. Will require manual encoding for now.',
hex(0x82):'Use cutscene index 0 to get normal camera, index 1 to get a locked camera. Any index from 134 to 181 to enable an in game cutscene',
hex(0x83):'Displays nothing. Use this if you want to fill the space with characters using asm later',
hex(0x84):'Scales the text. Input a decimal number. X position is accounted for, but Y position is not. Text is scaled from the bottom right as the origin, meaning a y scale of 2 will take up 2 lines of space. Use line breaks to account for this.',
hex(0x85):'Number of dialog options to use. End each dialog option with an [end:FF] tag. This cmd is zero indexed so there will always be one option. Use a negative number for a horizontal selector. You must manually position these, subsequent options will still appear below the first.',
hex(0x86):'Display the following string only when the selected option matches the index. Use 0x87 to display default text. If no match is found, this cmd will skip all characters until the next 0x86 or 0x87',
hex(0x87):'Display a default string after a dialog option. Will stop any 0x86 cmd from searching for a matching return and resume normal text engine operation',
hex(0x88):'In game function for screen shaking. May only work in certain courses',
hex(0x89):'Disable screen shake',
hex(0x8a):'Set absolute camera position. Each position is a signed half. Use an object editor to find positions.',
hex(0x8b):'Sets absolute camera focus. Each position is a signed half. Use an object editor to find positions.',
hex(0x8c):'Sweeps the camera linearly from the position its starts at at a certain speed. Only one sweep can go on at a time. If a second sweep is in the text before the first finishes, it will not execute until the first is done',
hex(0x8d):'Sweeps the camera lienarly from the position its starts at at a certain speed. Only one sweep can go on at a time. If a second sweep is in the text before the first finishes, it will not execute until the first is done',
hex(0x8e):'Loads the pointer from the RAM location and takes that as the object. e.g. 80361158 will load mario object.',
hex(0x8f):'Warp Delay is in number of frames. Warp ID is in decimal.',
hex(0x90):'Object needs to have a defined animation ptr and valid ID.',
hex(0x91):'KEEP padding as ZERO. Positions are signed integers. Speed is an unsigned integer=units per frame, set mario action before pathing him',
hex(0x93):'Skips everything until the matching end key is found',
hex(0x94):'Ends the skip cmd, does nothing if not currently skipping',
hex(0x95):'Ignores all cmds and text only if a wrong dialog option return is found. Does nothing if a matching return is already found or if not searching. Requires a end mask cmd or will loop forever.',
hex(0x96):'Ends mask for dialog options. Does nothing if not already masking a dialog option',
hex(0x97):'Stores a pointer to the current cmd+2. Indices can be anywhere from 0 to 7',
hex(0x98):'Goes to the pointer stored by the setup cmd. Indices can be anywhere from 0 to 7. Will do nothing if not setup. Makes a new text box',
hex(0x99):"only certain cmds work in plaintext mode, these will basically be the cmds that don't require me to segment the string. Plaintext will auto end if such a cmd is encountered will use the currently defined color as foreground, background will always be dark grey.",
hex(0x9A):'Ends plaintext. Does nothing if plaintext is not currently enabled.',
hex(0x9B):'Enables a transition played at the end of a text boxes. Angle is from 0 to FF with 0 being positive x. Spd is pixels moved per frame. Target alpha is transparency of text at end of transition. Use negative length for transition at start of box.',
hex(0x9B):'Enables a transition played at the start of a text boxes. Angle is from 0 to FF with 0 being positive x. Spd is pixels moved per frame. Target alpha is transparency of text at end of transition. Use negative length for transition at start of box.',
hex(0x9A):'Disables any transition between text boxes',
hex(0xA0):'A0 is a word argument, A1 is a pointer to current string position. Index refers to the array of return values. Index can be anywhere from 0 to 3. Use negative index for extended args',
hex(0xA1):'A0 is a word argument, A1 is a pointer to current string position. Index refers to the array of return values. Index can be anywhere from 0 to 3. Use negative index for extended args',
hex(0xA2):'Displays if the index matches the return value. Index for an 0xA0 cmd is 0 to 3, index for an 0xA1 cmd is 4 to 8. Return Value is a word',
hex(0xA3):'Position is relative to the focus position',
hex(0xA4):'1 to enable, 0 to disable',
hex(0xA5):'See Helper Notes for List of animations, you have to set the action to specific things to have it not overwritten',
hex(0xA6):'See Helper text for list of actions. Set to 0 to kill any inputs. Set to idle to restart mario, use look forward action for dialog',
hex(0xA7):'If there are mutliple objects with the same behavior it will get the closest one.',
hex(0xA8):'The source rotates towards the target object, you cannot rotate mario',
hex(0xA9):'>1 to enable, 0 to disable. Higher numbers equals faster shaking. Only goes up to 15 max.',
hex(0xAA):'Use right before a BG box to get a transition on it. Setup a transition first.'
}

Kernings=[
	7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  6,  6,  6,  6,  6,  6,
    6,  6,  5,  6,  6,  5,  8,  8,  6,  6,  6,  6,  6,  5,  6,  6,
    8,  7,  6,  6,  6,  5,  5,  6,  5,  5,  6,  5,  4,  5,  5,  3,
    7,  5,  5,  5,  6,  5,  5,  5,  5,  5,  7,  7,  5,  5,  4,  4,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    8,  8,  8,  8,  7,  7,  6,  7,  7,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  4,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  5,  6,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    7,  5, 10,  5,  9,  8,  4,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  5,  7,  7,  6,  6,  8,  0,  8, 10,  6,  4, 10,  0,  0
]