from tkinter import *
from tkinter.filedialog import *
from tkinter.messagebox import *
import sys
from os import listdir
from os.path import isfile, join


class File():
    def newFile(self):
        self.filename = "Untitled"
        self.text.delete(0.0, END)

    def saveFile(self):
        try:
            t = self.text.get(0.0, 'end-1c')
            f = open(self.filename, 'w')
            f.write(t)
            f.close()
        except:
            self.saveAs()

    def saveAs(self):
        f = asksaveasfile(mode='w', defaultextension='.txt')
        t = self.text.get(0.0, 'end-1c')
        try:
            f.write(t.rstrip())
            self.filename = f.name
            self.root.title("Text Engine Encoder - %s" %self.filename)
        except:
            showerror(title="Oops!", message="Unable to save file...")

    def openFile(self):
        f = askopenfile(mode='r')
        self.filename = f.name
        t = f.read()
        self.text.delete(0.0, END)
        self.text.insert(0.0, t)
        name = f.name.split('/')
        self.root.title("Text Engine Encoder - %s" % name[-1])

    def openMap(self):
        f = askopenfile(mode='r')
        name = f.name.split('/')
        self.map.config(text="Map = %s \n" % name[-1])
        self.map.test = True
        self.mapf = f.read()

    def quit(self):
        entry = askyesno(title="Quit", message="Are you sure you want to quit?")
        if entry == True:
            self.root.destroy()

    def UpdateDirectoriesC(self):
        entry = askyesno(title="Quit", message="Save before updating dir? Changes will be lost.")
        if entry == True:
            self.saveAs()
        dir = askdirectory()
        files = [f for f in listdir(dir) if isfile(join(dir,f))]
        files = [f for f in files if f.split('.')[-1] == 'txt']
        for f in files:
            q = open(dir+'/'+f,'r')
            self.filename = dir+'/'+f
            t = q.read()
            self.text.delete(0.0, END)
            self.text.insert(0.0, t)
            self.Cout()

    def UpdateDirectoriesBin(self):
        entry = askyesno(title="Quit", message="Save before updating dir? Changes will be lost.")
        if entry == True:
            self.saveAs()
        dir = askdirectory()
        files = [f for f in listdir(dir) if isfile(join(dir,f))]
        files = [f for f in files if f.split('.')[-1] == 'txt']
        for f in files:
            q = open(dir+'/'+f,'r')
            self.filename = dir+'/'+f
            t = q.read()
            self.text.delete(0.0, END)
            self.text.insert(0.0, t)
            self.Hout()

    def __init__(self, text, root,map,COutput,HexOut):
        self.filename = None
        self.text = text
        self.root = root
        self.map = map
        self.Cout = COutput
        self.Hout = HexOut


def main(root, text, menubar,map,COutput,HexOut):
    filemenu = Menu(menubar)
    objFile = File(text, root,map,COutput,HexOut)
    filemenu.add_command(label="New", command=objFile.newFile)
    filemenu.add_command(label="Open", command=objFile.openFile)
    filemenu.add_command(label="Load Map", command=objFile.openMap)
    filemenu.add_command(label="Save", command=objFile.saveFile)
    filemenu.add_command(label="Save As...", command=objFile.saveAs)
    filemenu.add_command(label="Update Directory C", command=objFile.UpdateDirectoriesC)
    filemenu.add_command(label="Update Directory Bin", command=objFile.UpdateDirectoriesBin)
    filemenu.add_separator()
    filemenu.add_command(label="Quit", command=objFile.quit)
    menubar.add_cascade(label="File", menu=filemenu)
    root.config(menu=menubar)
    return objFile

if __name__ == "__main__":
    print("Please run 'main.py'")
